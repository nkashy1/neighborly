CC = gcc

TOP = src
METRICS = src/metrics
STRUCTURES = src/structures
COMMON = src/structures/common

TESTTOP = test
TESTMETRICS = test/metrics
TESTSTRUCTURES = test/structures
TESTCOMMON = test/structures/common

DEPS = $(TOP)/neighborly_types.h $(TOP)/naive.h $(TOP)/nn_kd_tree.h $(METRICS)/distance.h $(STRUCTURES)/data_set.h $(STRUCTURES)/kd_tree.h $(STRUCTURES)/results.h $(COMMON)/csv.h $(COMMON)/median.h $(COMMON)/numerical_heap.h $(COMMON)/results_heap.h $(COMMON)/spread.h

TESTDEPS = $(TESTTOP)/test_naive.h $(TESTTOP)/test_nn_kd_tree.h $(TESTMETRICS)/test_distance.h $(TESTSTRUCTURES)/test_data_set.h $(TESTSTRUCTURES)/test_kd_tree.h $(TESTSTRUCTURES)/test_results.h $(TESTCOMMON)/test_csv.h $(TESTCOMMON)/test_median.h $(TESTCOMMON)/test_numerical_heap.h $(TESTCOMMON)/test_results_heap.h $(TESTCOMMON)/test_spread.h

_OBJS = naive.o nn_kd_tree.o distance.o data_set.o kd_tree.o results.o csv.o median.o numerical_heap.o results_heap.o spread.o
OBJS = $(patsubst %, obj/%, $(_OBJS))

_TESTOBJS = test_naive.o test_nn_kd_tree.o test_distance.o test_data_set.o test_kd_tree.o test_results.o test_csv.o test_median.o test_numerical_heap.o test_results_heap.o test_spread.o
TESTOBJS = $(patsubst %, obj/%, $(_TESTOBJS))

obj/naive.o: $(TOP)/naive.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/nn_kd_tree.o: $(TOP)/nn_kd_tree.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/distance.o: $(METRICS)/distance.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/data_set.o: $(STRUCTURES)/data_set.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/kd_tree.o: $(STRUCTURES)/kd_tree.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/results.o: $(STRUCTURES)/results.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/csv.o: $(COMMON)/csv.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/median.o: $(COMMON)/median.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/numerical_heap.o: $(COMMON)/numerical_heap.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/results_heap.o: $(COMMON)/results_heap.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/spread.o: $(COMMON)/spread.c $(DEPS)
	$(CC) -Wall -c -o $@ $< -lm


obj/test_naive.o: $(TESTTOP)/test_naive.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_nn_kd_tree.o: $(TESTTOP)/test_nn_kd_tree.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_distance.o: $(TESTMETRICS)/test_distance.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_data_set.o: $(TESTSTRUCTURES)/test_data_set.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_kd_tree.o: $(TESTSTRUCTURES)/test_kd_tree.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_results.o: $(TESTSTRUCTURES)/test_results.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_csv.o: $(TESTCOMMON)/test_csv.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_median.o: $(TESTCOMMON)/test_median.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_numerical_heap.o: $(TESTCOMMON)/test_numerical_heap.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_results_heap.o: $(TESTCOMMON)/test_results_heap.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm

obj/test_spread.o: $(TESTCOMMON)/test_spread.c $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -c -o $@ $< -lm


test: $(TESTOBJS) $(OBJS) $(DEPS) $(TESTDEPS)
	$(CC) -Wall -g -pg -o bin/test.out test/unit_tests.c $(TESTOBJS) $(OBJS) -lm


shared: $(OBJS) $(DEPS)
	$(CC) -shared -o libneighborly.so $(OBJS) -lm


python: shared
	$(CC) -L. -o bin/python.out python/neighborlymodule.c -lneighborly -lm -lpython2.7


.PHONY: clean
clean:
	rm -f obj/*.o
