# neighborly

**Speedy nearest neighbor calculations.**


### Purpose
`neighborly` performs distance calculations between points in metric spaces. Suppose we are working in a metric space `(X, d)`, where `X` denotes the underlying point set and `d` denotes the metric on `X` --- if `x` and `y` are two points in `X`, then `d(x,y)` is the distance between them.

`neighborly` is built to answer the following questions:

1. *Streaming nearest neighbors problem*
Consider a list of points `list[0], list[1], ..., list[N-1]` in `X`. Given a new point `x` in `X`, for which `j` is `list[j]` closest to `x`? What is the distance between this `list[j]` and `x`?

2. *All nearest neighbors problem*
Consider a list of points `list[0], list[1], ..., list[N-1]` in `X`. For each `j`, for which index `k` not equal to `j` is `list[k]` closest to `list[j]`? What is the distance from each point `list[j]` to its corresponding closest point `list[k]`?

The functionality of `neighborly` extends to many variations of these two fundamental (and fundamentally related) questions.


### Status
Development is currently [on hold](https://bitbucket.org/nkashy1/neighborly/src/master/doc/iter.md?at=master).


### Getting started

[Compiling neighborly](https://bitbucket.org/nkashy1/neighborly/src/master/doc/compile.md?at=master)

[Developer Documentation](https://bitbucket.org/nkashy1/neighborly/src/master/doc/dev.md?at=master)

[User Documentation](https://bitbucket.org/nkashy1/neighborly/src/master/doc/user.md?at=master)


### License

`neighborly` is distributed under the [MIT License](https://bitbucket.org/nkashy1/neighborly/src/master/LICENSE.md?at=master).