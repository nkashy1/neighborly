
##### Compiling with scons

Install [SCons](http://www.scons.org/) and do this command to compile `src/` and `test/`:

```
$ scons
```

or with debug option (i.e. `-g` instead of `-O2`):

```
$ scons debug=1
```

Cleanup is also easy:

```
$ scons -c
```

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master)