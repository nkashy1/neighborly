## neighborly Developer Documentation

### Project overview
There are four directions in which `neighborly` is being developed:

1. **Numerics** --- This is the mathematical core of `neighborly`. It involves the development and implementation of nearest neighbor algorithms. The numerical libraries that make up this component are written in C. The focus is on **speed**.

2. **Interface** --- The development of this component determines how `neighborly` users connect to its numerical core. There are two kinds of users we have in mind -- developers, who want to integrate `neighborly` into their own software, and data analysts, who want to insert `neighborly` into their existing work flow. Not only do we have to provide direct interfaces for both kinds of users, but we also have to interface with their preferred mathematical/statistical software, their programming language of choice, and their favourite database platform. The key design principle is **flexibility**.

3. **Documentation** --- `neighborly` documentation is not and will never be a mere afterthought to our code. If we treated it as an afterthought, we would not be able to take advantage of the beneficial feedback loop that exists between documentation and development. Moreover, our documentation will likely be the first substantial contact our users have with our software. It needs to be of high quality in order to maximize their engagement. Our emphasis in documentation will always be on **clarity**.

4. **Testing** --- None of us are perfect. Bugs *will* creep into our code. By thoroughly testing the code that we push into the repository, we decrease the chances of an "unknown unknown" dragging the `neighborly` name through the mud. There are two aspects to focus on -- writing exhaustive unit tests, and improvement of the testing framework. Through all of this, our watchword is **rigor**.

The following pages house information and discussions about each of these components:  
1. [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)  
2. [Interface Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/inthub.md?at=master)  
3. [Documentation Guide](https://bitbucket.org/nkashy1/neighborly/src/master/doc/docguide.md?at=master)  

Information about unit tests may be found in the hub corresponding to their units.

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master)