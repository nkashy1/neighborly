## neighborly Documentation Guide


We want people to use `neighborly` because it is a great package. Our craftsmanship lies in our code. However, a new user is not going to interact directly with our code! Any potential user's first exposure to `neighborly` will be a test drive of sorts. And chances are that all they will have to guide them is the documentation. If we want to turn these potential users into definite users and subsequently into contributors, we *cannot* compromise on the quality of our documentation.

Always remember when writing documentation that we have two different users in mind for our package -- the developer, and the analyst. These two users require two completely different kinds of documentation. Your documentation will also guide contributors to `neighborly` in their development of the project. Your fellow contributors will require a third type of documentation. Each piece of documentation you submit should target *exactly* one of these three people.

Although the requirements for each type of documentation vary significantly, one thing that should be common across all three is an emphasis on examples. Every one of your contributions should be initiated with a particular set of use cases in mind. Your documentation should make those use cases explicit and provide examples of how one may realize those use cases with your code.

Finally, as a matter of convention, all documentation meant for `neighborly` users should be linked to from the [User Documentation](https://bitbucket.org/nkashy1/neighborly/src/master/doc/user.md?at=master) page. Documentation meant for contributors should be linked to from the relevant hub page.


- - -

**Interesting References:**  
1. ["Teach, Don't Tell"](http://stevelosh.com/blog/2013/09/teach-dont-tell/) by Steve Losh.  
2. ["Writing Great Documentation"](http://jacobian.org/writing/great-documentation/) by Jacob Kaplan-Moss.

- - - 

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Developer Documentation](https://bitbucket.org/nkashy1/neighborly/src/master/doc/dev.md?at=master)