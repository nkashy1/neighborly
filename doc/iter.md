### neighborly: Iterations

**Iteration 1:** [*functional*] k-d tree algorithms  
Start date: August 27, 2014  
Completed: October 5, 2014  

**Iteration 2:** [*dev*] Python interface, data_set loader  
Start date: October 6, 2014
Completed: October 13, 2014

- - -


##### Categories

+ *functional* --- *functional* iterations involve the implementation of existing algorithms for the solution of nearest neighbor problems, or the improvement of our existing implementations.

+ *dev* --- *dev* iterations involve development of the developer interface to the core `neighborly` library. This interface is for people who would like to use `neighborly` in their own programs, but without having to muck about with our low-level source code.

+ *user* --- *user* iterations involve development of the user interface to `neighborly`. This interface is for non-programmers who want the functionality of `neighborly` but would rather it didn't come with a learning curve.

+ *doc* --- *doc* iterations involve either the creation of new documentation or making updates to existing documentation.

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master)