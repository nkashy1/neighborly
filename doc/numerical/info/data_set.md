### data_set: Information

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/data_set.h?at=master), [Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/data_set.c?at=master), 
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/data_set.md?at=master)

**Description:** These files contain a definition of the `data_set` struct, which is designed to hold raw vector data accepted from the user. They also contain utility functions for the manipulation of such data.

**Notes:**  
1. The `struct data_set` is defined as `data_set` in [neighborly_types.h](https://bitbucket.org/nkashy1/neighborly/src/master/src/neighborly_types.h?at=master).


##### The data_set structure  
Each `data_set` contains three fields:  
1. `int embedding_dimension`, the dimension of the real vector space in which the data is embedded.  
2. `int num_data_points`, the number of data points in that `data_set`.  
3. `numerical_type *data`, the data points themselves. This is allocated as an array of length `embedding_dimension*num_data_points`. Each point is stored in `embedding_dimension` contiguous entries of the array.  


##### Utility functions  
Below is a list of the utility functions for the `data_set` structure. For more information, please refer to the comments in the [header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/data_set.h?at=master)  
1. `data_set_create`  
2. `data_set_destroy`  
3. `extract_data`  
4. `data_set_swap`  
5. `data_set_print`  

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)