### kd_tree: Information

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.h?at=master), [Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.c?at=master), [Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/kd_tree.md?at=master)

**Description:** `kd_tree.h` and `kd_tree.c` contain the tools needed to build k-d trees as specified in [1]

  
This is done with the `kd_vertex` and `kd_tree` structs, defined in [kd_tree.h](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.h?at=master).


##### The kd_vertex structure  
Each `kd_vertex` is comprised of the following four fields:  
1. `int disc_key`, the discriminating key used to be used in traversing the k-d tree at that vertex.  
2. `NUMERICAL_TYPE* record`, the actual vector stored at that vertex. (Note: The number of components in the vector is stored in the kd_tree, not at the vertex level.)  
3. `NUMERICAL_TYPE extremes[2]`, records the extremal values below and above the value of the current record at its discriminating key. The extremes are over the descendants of this kd_vertex.  
4. `struct kd_vertex* children[2]`, pointers to the low (0) and high (1) children of the current kd_vertex. If either child does not exist, the corresponding pointer will be `NULL`.


##### kd_vertex utilities
A list of the utility functions for `kd_vertex`. For detailed descriptions, please refer to the comments in [kd_tree.h](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.h?at=master):  
1. `kd_vertex_create`  
2. `kd_vertex_destroy`


##### The kd_tree structure  
Each `kd_tree` is comprised of the following four fields:  
1. `int dimension`, the number of keys for the records to the stored in the kd_tree.  
2. `int num_vertices`, the number of vertices in the kd_tree.  
3. `int depth`, the depth of the kd_tree. That is, the maximal length of a geodesic path emanating from the root of the kd_tree.  
4. `kd_vertex *root`, a pointer to the root vertex of the kd_tree.


##### kd_tree utilities
A list of the utility functions for `kd_vertex`. For detailed descriptions, please refer to the comments in [kd_tree.h](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.h?at=master):  
1. `kd_tree_create`  
2. `kd_tree_construct`  
3. `kd_tree_destroy`  
4. `kd_tree_insert`  
5. `kd_tree_enumerate`  
6. `kd_tree_construct_fancy`


- - -

##### References  
1. Jon Louis Bentley, *Multidimensional Binary Search Trees Used for Associative Searching*, Communications of the ACM, Volume 18 (9), September 1975.

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)