### naive: Information

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/naive.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/naive.c?at=master),
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/naive.md?at=master)

**Description:** `naive.h` and `naive.c` contain functions which implement naive solutions to the two fundamental nearest neighbor problems. These functions are intended to be used internally only, for benchmarking and verification purposes.

##### naive_nearest_neighbor_distance  
This function returns the `L_2`-distance from a given point to its nearest *non-identical* neighbor within a given data set. It doesn't return any further information about the neighboring point, simply the distance.

##### naive_all_nearest_neighbor_distances  
This function returns an array of the `L_2`-distances from each of the points in a given data set to their nearest non-identical neighbors. It relies upon the computation of the distance between every pair of points in the data set (with the cost mitigated somewhat by the introduction of the `distance_matrix` structure). This represents the worst we could do, *algorithmically* speaking, for the all nearest neighbor distance problem. This means that, even using a decent implementation of this algorithm, the performance is not acceptable.

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)