### neighborly_types: Information

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/neighborly_types.h?at=master), [Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/neighborly_types.md?at=master)

**Motivating problem:** C does not support templates. But we would like a user to be able to specify the level of floating point precision used in their calculations. We don't want to have to write different versions of our functions depending on whether the user wants to use floats or doubles, and such a solution precludes the possibility of dealing user-defined numerical types.


**Solution:** `neighborly_types.h` allows the user to specify what types they would like to use at compile time. This has extended beyond how they do floating point arithmetic and now also includes the structures they would like to use to store their data.

The `#define`s and `typedef`s in `neighborly_types.h` simulate some of the functionality of C++ templates, although it does require recompilation if they change their minds. I would be very happy to implement a different solution if it gives us more template-like functionality.

This creates some interface-related work, as we need to specify the capabilities of the various types that can be used in place of the defaults in `neighborly_types.h`. See [Issue #10](https://bitbucket.org/nkashy1/neighborly/issue/10/neighborly_types-type-specifications).  

- - -

As of September 15, 2014 (actually earlier, but I didn't get around to documenting it till then), the statement about neighborly_types extending to the types of data structures used is no longer true. I had initially intended it to be that `neighborly` developers could use a generic heap struct in their code by simply declaring a typedef at the top of their .h or .c files. Well, the way I was doing things was very, very ill-conceived. There are ways of achieving the type of genericity that I was targetting in C, but I decided to skip it and just define two different types of heaps. This can be revisited if we find a pressing need for template-like ability in the future.

- Neeraj

- - -

As of September 23, 2014, I have changed from `typedef float numerical_type;` to `#define NUMERICAL_TYPE float` as it gets the job done more cleanly and is more consistent with the handling of the absolute value function by `#define ABS fabsf`.

- Neeraj

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)