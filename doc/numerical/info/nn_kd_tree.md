### nn_kd_tree: Information

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/nn_kd_tree.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/nn_kd_tree.c?at=master),
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/nn_kd_tree.md?at=master)


**Description:** `nn_kd_tree.h` and `nn_kd_tree.c` contain functions which use k-dimensional trees to process streaming nearest neighbor queries, as well as solve the all nearest neighbor problem.


##### kd_tree_nearest_neighbors  
This function finds a given number of the nearest neighbors of a specified point within a "framing" data set. It also allows the user to specify an `L_p`-metric under which to operate.


##### kd_tree_all_nearest_neighbors  
This function performs an all nearest neighbors computation by applying kd_tree_nearest_neighbors sequentially to all the records in a given `kd_tree`. This implementation uses no further refinements.

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)