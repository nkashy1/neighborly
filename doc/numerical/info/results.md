### results: Information

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.c?at=master),
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/results.md?at=master)


**Description:** Data structures for holding the results of nearest neighbor queries, along with their related utilities.


- - -

##### List of data structures

[results.h](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.h?at=master) contains the following struct definitions:  
1. `results_datum`  
2. `results`


- - -

##### List of functions

[results.h](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.h?at=master) contains the following function definitions:  
1. `results_datum_create`  
2. `results_datum_destroy`  
3. `results_create`  
4. `results_destroy`  
5. `results_print`  
6. `results_lt`  
7. `results_gt`  


- - -

##### struct results_datum and struct results

Each point resulting from a nearest neighbor query is stored, along with its associated distance, in a `results_datum` struct. The fields are as follows:  
1. `NUMERICAL_TYPE *data_point`, the aforementioned point  
2. `NUMERICAL_TYPE distance`, its distance from the query point

A `results` struct is at its heart an array of `results_datum` structs, but contains additionally the length of this array. Its fields are:  
1. `results_datum *data`, an array of the nearest neighbors from 


- - -

##### Notes

1. September 25, 2014: The `results_lt` and `results_gt` functions are out of place here, and I think they should be moved (along with all the other order functions) to some common "order" library. -nkashy1

2. September 26, 2014: I think that I should also include fields for the query point and for the embedding dimension in the results struct. Things are just so much CLEANER that way. -nkashy1

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)