### data_set: Usage

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/data_set.h?at=master), [Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/data_set.c?at=master), [Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/data_set.md?at=master)


##### Example 1: Creating and manipulating a data_set   
The [example source](https://bitbucket.org/nkashy1/neighborly/src/master/examples/data_set/example1_data_set.c?at=master) shows how to create and manipulate `data_set`s in 7 simple steps. The comments within `main()` are a friendly guide to the process.

- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)