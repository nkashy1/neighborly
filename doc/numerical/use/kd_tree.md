### kd_tree: Usage

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.h?at=master), [Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.c?at=master), [Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/kd_tree.md?at=master)


##### Example 1: Constructing a kd_tree from a data_set  
[Example source](https://bitbucket.org/nkashy1/neighborly/src/master/examples/kd_tree/example1.c?at=master)  

In this example, we will begin with a `data_set` consisting of the points  
```
(0,0), (1,0), (-1, 1), (-1, -1), (2, -1), (3, 5), (2,2),
```
and show how one may use it to construct a `kd_tree`. Let us assume that the data_set is presented to us by means of the pointer  
```
data_set *sample_data;
```

We will build a `kd_tree` using this data_set using the `kd_tree_construct_fancy` function. This function dynamically allocates the memory for the desired `kd_tree` and returns a pointer to that memory. Therefore, we must create a `kd_tree *` in order to refer to the `kd_tree` constructed from `sample_data`:  
```
kd_tree *treeification;
treeification = kd_tree_construct_fancy(sample_data);
```

The way `kd_tree_construct_fancy` works is that it chooses as its discriminating key at a given vertex the height of that vertex in the tree modulo the number of keys, which is Bentley's original insertion procedure. In order to keep the `kd_tree` as balanced as possible, though, `kd_tree_construct_fancy` doesn't simply insert the `data_set` record by record. Instead, for each vertex, it chooses from the candidate data points the one of median key value.

As partial verification of `kd_tree_construct_fancy`, we can perform a breadth-first traversal of the `kd_tree` pointed to by `treeification`. This can be achieved using the `kd_tree_enumerate` function, which returns the list of visited vertices as a `kd_vertex **`, where each entry is a pointer to the relevant `kd_vertex` in the input `kd_tree`.  
```
kd_vertex **enumeration;
enumeration = kd_tree_enumerate(treeification);
```

We print out the records visited in this breadth-first traversal using the following code:  
```
	printf("\nBreadth-first traversal of the k-d tree\n");
    int i;
    for (i = 0; i < treeification->num_vertices; i++) {
        printf("Vertex %d: (%f, %f)\n", i, enumeration[i]->record[0], enumeration[i]->record[1]);
    }
```

Compiling the [example source](https://bitbucket.org/nkashy1/neighborly/src/master/examples/kd_tree/example1.c?at=master) and running the binary yields the following output:  
```
Breadth-first traversal of the k-d tree
Vertex 0: (1.000000, 0.000000)
Vertex 1: (0.000000, 0.000000)
Vertex 2: (2.000000, 2.000000)
Vertex 3: (-1.000000, -1.000000)
Vertex 4: (-1.000000, 1.000000)
Vertex 5: (2.000000, -1.000000)
Vertex 6: (3.000000, 5.000000)
```

Let us manually check that this is what the breadth-first traversal should look like. Being at height 0, the root of `treeification` will have `0` as its discriminating key, i.e. the `x`-coordinate. The median `x`-coordinate in the `sample_data` is `1`, coming from the point `(1,0)`, which was indeed chosen to be the root of `treeification`. The following data points are less than `(1,0)` in `x`-coordinate:  
```
(0,0), (-1,1), (-1,-1).
```
The children of the root will have `1` as their discriminating keys. Therefore, the low child of the root should be the point among these with median `y`-coordinate, i.e. `(0,0)`, and this is indeed `Vertex 1` in the traversal. Similarly, `Vertex 2` in the traversal is `(2,2)`, the point amongst those with `x`-coordinates greater than or equal to `1` of median `y`-coordinate. After this, the median criterion is no longer applied as there are too few data points left to insert. Therefore, the breadth-first traversal of `treeification` is exactly as it should be!

Once we have finished up, it is important to free the memory allocated by our program. The `enumeration` of the vertices should be deallocated manually with  
```
free(enumeration);
```

The `kd_tree` itself should be deallocated using the `kd_tree_destroy` utility, which also frees up the memory allocated to each of the `kd_vertex` structs comprising the `kd_tree`. This is simple done with  
```
kd_tree_destroy(treeification);
```

And that's all there is to k-d trees in `neighborly`!


- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)