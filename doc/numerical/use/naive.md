### naive: Usage

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/naive.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/naive.c?at=master),
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/naive.md?at=master)

##### Example 1: Using naive_nearest_neighbor_distance  
Suppose that we are given a `data_set` and a point as  
```
data_set *sample_data;
NUMERICAL_TYPE *sample_point;
```
It is important here that `sample_point` be an array of length `sample_data->embedding_dimension`. Also note that we will merely be passing a reference to a `data_set` rather than the `data_set` itself.

`naive_nearest_neighbor_distance` may be used to find the distance from `sample_point` to its nearest non-identical neighbor amidst the points in `sample_data`. This can be calculated as  
```
NUMERICAL_TYPE nearest_neighbor_distance;
nearest_neighbor_distance = naive_nearest_neighbor_distance(sample_data, sample_point);
```

The `test_naive_nearest_neighbor_distance()` function in [test_naive.c](https://bitbucket.org/nkashy1/neighborly/src/master/test/test_naive.c?at=master) shows this function in action.


##### Example 2: Using naive_all_nearest_neighbor_distances  
The standard use case for the `naive_all_nearest_neighbor_distances()` function is that we have a `data_set` as  
```
data_set *sample_data;
```
for which we would like to solve the all nearest neighbors problem. `naive_all_nearest_neighbor_distances()` determines the distance from each of the points in the data set to its nearest *non-identical* neighbor. It doesn't operate directly on the `data_set`, but rather on its associated `distance_matrix`, which is basically an encoding of the pairwise distances between points in the `data_set`. The `distance_matrix` implementation may be found in [distance.h](https://bitbucket.org/nkashy1/neighborly/src/master/src/metric/distance.h?at=master), and can be here as follows:  
```
#include "[path to neighborly]/src/metric/distance.h"
.
.
.
distance_matrix *pairwise_distances;
pairwise_distances = distance_matrix_create(sample_data);
```
With the `distance_matrix` created, we can get the non-zero nearest neighbor distances with  
```
NUMERICAL_TYPE *nearest_neighbor_distances;
nearest_neighbor_distances = naive_all_nearest_neighbor_distances(pairwise_distances);
```

The `nearest_neighbor_distances` array has length equal to `sample_data->num_data_points`.

Once we are finished with all our work, we should remember to deallocate `nearest_neighbor_distances` with  
```
free(nearest_neighbor_distances);
```

and `pairwise_distances` with  
```
distance_matrix_destroy(pairwise_distances);
```

For a practical demonstration, please refer to the `test_naive_all_nearest_neighbor_distances()` function in [test_naive.c](https://bitbucket.org/nkashy1/neighborly/src/master/test/test_naive.c?at=master).


- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)