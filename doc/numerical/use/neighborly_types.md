### neighborly_types: Usage

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/neighborly_types.h?at=master), [Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/neighborly_types.md?at=master)

##### Use Case: use a different floating point type

By default, `neighborly` uses the `float` type in its calculations. However, you may want to increase precision by using `double` or you may instead prefer to use your own `custom_type` for floating point arithmetic (this would require you to overload the arithmetical operators, and you would probably have to compile the `neighborly` C code using a C++ compiler).

In each of those situations, you will want override the following lines of `neighborly_types.h`:  
```
#define NUMERICAL_TYPE float
#define ABS fabsf
```

`NUMERICAL_TYPE` is used in all the `neighborly` libraries to signify the default type for floating point numbers. `ABS` is used to specify the handle for the absolute value function corresponding to that type.

If you want to use double precision floating point numbers, the above definitions should be replaced by:  
```
#define NUMERICAL_TYPE double
#define ABS fabs
```

If you would like to use `custom_type`, assuming that it and a corresponding absolute value function `custom_abs` are defined in the file `custom_type_definition.h`, simply override the previous definitions with  
```
#include "custom_type_definition.h"  
#define NUMERICAL_TYPE custom_type
#define ABS custom_abs
```


##### Use Case: use a different structure for storing and manipulating your data

Right now, the `neighborly` library uses the `data_set` struct and its associated utilities as an interface to the raw data. The binding of `struct data_set` to `data_set` is performed in `neighborly_types.h` and there is nothing stopping you from using your own data set structure, say `custom_data_set`. All you would have to do is construct utilities which function in the same manner as the data set utilities defined in [data_set.h](https://bitbucket.org/nkashy1/neighborly/src/master/lib/storage/data_set.h?at=master).

Suppose that you want to use `struct custom_data_set` which is defined along with its utility functions in `custom_data_set_definition.h`. Then you should replace the inclusion  
```
#include "storage/data_set.h"
```  
with  
```
#include "custom_data_set_definition.h"` 
```
and replace the definition  
```
typedef struct data_set data_set;
```  
with  
```
typedef struct custom_data_set data_set;
```


- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)