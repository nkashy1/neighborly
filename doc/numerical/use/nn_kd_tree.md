### nn_kd_tree: Usage

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/nn_kd_tree.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/nn_kd_tree.c?at=master),
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/nn_kd_tree.md?at=master)


- - -

##### Example 1: a streaming nearest neighbor query  
[Example Source](https://bitbucket.org/nkashy1/neighborly/src/master/examples/nn_kd_tree/example1.c?at=master),
[Example Output](https://bitbucket.org/nkashy1/neighborly/src/master/examples/nn_kd_tree/example1.output?at=master)

In this example, we will see how to use the `kd_tree_nearest_neighbors()` function to find the two closest neighbors to the point `(5,5)` in the framing data set  
```
(0,0), (1,0), (-1, 1), (-1, -1), (2, -1), (3, 5), (2,2).
```

These points were previously used in [kd_tree Usage Example 1](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/kd_tree.md?at=master), where we saw how to construct a `kd_tree` out of the `data_set` consisting of these data. Once again, we assume that these data are given as  
```
data_set *sample_data;
```

In order to perform nearest neighbor queries upon this `data_set` using the `kd_tree_nearest_neighbors()` function, we must first construct a `kd_tree` as in [kd_tree Usage Example 1](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/kd_tree.md?at=master). We do this with the following code:  
```
kd_tree *frame_tree;
frame_tree = kd_tree_construct_fancy(sample_data);
```

We also assume that we are given a query point in `R^2` as  
```
NUMERICAL_TYPE query_point[2];
```

We begin, then, by defining the other parameters to be used in the nearest neighbor query. First, we must specify the number of neighbors we are searching for with  
```
int num_neighbors;
num_neighbors = 2;
```

Next, we must specify the metric to be used in the search. This is done using  
```
NUMERICAL_TYPE p;
p = 2.0;
```

We are now ready to make the query! We will use the `results` struct to store the results of the query.  
```
results *neighbors;
neighbors = kd_tree_nearest_neighbors(frame_tree, query_point, num_neighbors, p);
```

Full specifications for the `results` struct may be found on the [results information page](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/results.md?at=master). The structure is very easy to navigate, however, and we give a brief overview here by taking upon ourselves the task of printing the results of the nearest neighbor query to the screen. This is done with the following code:  
```
printf("\nThe %d nearest neighbors of (%f, %f) within the given data set are:\n\n", num_neighbors, query_point[0], query_point[1]);
int i;
for (i = 0; i < num_neighbors; i++) {
    printf("%d. (%f, %f) at distance %f\n", i, neighbors->data[i].data_point[0], neighbors->data[i].data_point[1], neighbors->data[i].distance);
}
```

Finally, once we are finished with all this work, we should free up all the memory allocated as a result of our code:  
```
results_destroy(neighbors);
kd_tree_destroy(frame_tree);
data_set_destroy(sample_data);
```

This is what is done in the [code](https://bitbucket.org/nkashy1/neighborly/src/master/examples/nn_kd_tree/example1.c?at=master) for this example, which yields the following [output](https://bitbucket.org/nkashy1/neighborly/src/master/examples/nn_kd_tree/example1.output?at=master):  
```
The 2 nearest neighbors of (5.000000, 5.000000) within the given data set are:
0. (2.000000, 2.000000) at distance 4.242640
1. (3.000000, 5.000000) at distance 2.000000

```


- - -

##### Example 2: an all nearest neighbors query
[Example Source](https://bitbucket.org/nkashy1/neighborly/src/master/examples/nn_kd_tree/example2.c?at=master),
[Example Output](https://bitbucket.org/nkashy1/neighborly/src/master/examples/nn_kd_tree/example2.output?at=master)

In this example, we perform an all nearest neighbors query using `kd_tree_all_nearest_neighbors()` on the same data set
```
(0,0), (1,0), (-1, 1), (-1, -1), (2, -1), (3, 5), (2,2),
```
that we used in the previous example.

The initialization of `frame_tree` and `p` is performed as in Example 1. In this case, however, instead of the `results *` `neighbors`, we store the results of the query in
```
results **all_neighbors;
```
This is an array of length `frame_tree->num_vertices` which stores the results of the nearest neighbor queries for each of the records in `frame_tree`.

One thing to note about the all nearest neighbors query is that each record will be listed in the results as its own closest neighbor at distance 0. This is because vertices are not removed from `frame_tree` before the query is made for the nearest neighbors of their records. Therefore, we should initialize the `num_neighbors` parameter accordingly. If we want to find the two nearest non-identical neighbors of each data point, we should set  
```
int num_neighbors;
num_neighbors = 3;
```

With these preparations in place, all we have to do is call `kd_tree_all_nearest_neighbors()`:  
```
all_neighbors = kd_tree_all_nearest_neighbors(frame_tree, num_neighbors, p);
```

In the [example code](https://bitbucket.org/nkashy1/neighborly/src/master/examples/nn_kd_tree/example2.c?at=master), we perform such an all nearest neighbor query, and print its results to the screen. Note that the order in which the results are returned in `all_neighbors` matches up with the order in which the breadth-first traversal of `frame_tree` visits its vertices, and this is demonstrated in the example code. However, it is not necessary to enumerate the vertices of `frame_tree` in order to match up results with records. After all, the point in each of the results in `all_neighbors` at distance 0 is the query point.


- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)