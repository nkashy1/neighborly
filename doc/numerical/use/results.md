### results: Usage

(Author: nkashy1)  
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.c?at=master),
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/results.md?at=master)


- - -

##### Processing results of nearest neighbor queries

Suppose you've just used `neighborly` to make a nearest neighbor query. Whichever function you used probably returned the results of the query to you in the form of
```
results *query_results;
```

Great! Now you would like to access the information in `query_results`, and this section will show you how to do so.

The first thing to understand is that the `results` struct at `query_results` is just a fancy wrapper around an array of `results_datum` structs which contain the information you *really* want to get at. The number of neighbors you specified in your query is stored in the `num_neighbors` field of `query_results` and can be accessed by `query_results->num_neighbors`. The actual results are stored in the `data` field of `query_results` as  
```
query_results->data[0], query_results->data[1], ... , query_results->data[query_results->num_neighbors]
```

Each `query_results->data[i]` contains two pieces of information --- the neighboring point itself, and its distance from the query point. The neighboring point is stored in the `data_point` field, which can be accessed as `query_results->data[i].data_point` and which is an array of length equal to the number of coordinates in the query point. The distance between the neighboring point and the query point is stored in the `distance` field, which may be accessed with `query_results->data[i].distance`.  

The `results_print()` function in [results.c](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.c?at=master) is a demonstrates how the results of a nearest neighbor query can be manipulated in practice.


- - -

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Numerical Hub](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numhub.md?at=master)