## neighborly Numerical Hub

**Format** --- "component_name: Header, Source, document_1, document_2, ..."  
component_name could be the name of a file or even the name of a function or struct in the neighborly library.  
The documents link to the appropriate documentation files in ./doc.


### Source components

- neighborly_types: 
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/neighborly_types.h?at=master), 
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/neighborly_types.md?at=master), 
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/neighborly_types.md?at=master)

- data_set: 
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/data_set.h?at=master), 
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/data_set.c?at=master), 
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/data_set.md?at=master), 
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/data_set.md?at=master)

- kd_tree: 
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.h?at=master), 
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/kd_tree.c?at=master), 
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/kd_tree.md?at=master), 
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/kd_tree.md?at=master)

- naive: 
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/naive.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/naive.c?at=master),
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/naive.md?at=master),
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/naive.md?at=master)

- nn_kd_tree:
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/nn_kd_tree.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/nn_kd_tree.c?at=master),
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/nn_kd_tree.md?at=master),
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/nn_kd_tree.md?at=master)

- results:
[Header](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.h?at=master),
[Source](https://bitbucket.org/nkashy1/neighborly/src/master/src/structures/results.c?at=master),
[Information](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/info/results.md?at=master),
[Usage](https://bitbucket.org/nkashy1/neighborly/src/master/doc/numerical/use/results.md?at=master)


- - - 

[Home](https://bitbucket.org/nkashy1/neighborly/src/master/README.md?at=master) - [Developer Documentation](https://bitbucket.org/nkashy1/neighborly/src/master/doc/dev.md?at=master)
