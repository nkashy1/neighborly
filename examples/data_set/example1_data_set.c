
/*****************************************************************************
** Created on: September 15, 2014
**
** Modified:
**
******************************************************************************/

/* Load types */
#include "../../src/neighborly_types.h"

/* Example library */
#include "../../src/structures/data_set.h"

/* Standard libraries */
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */




int main() {
    // In this example, we will work with a data set consisting of three points in R^2. The points are (0, 0), (1, 1), and (2, 2).
    // We will run through the usage of each of the utility functions written for data_set to date.


    // Step 1: Use data_set_create to allocate the memory required to hold this information. Let us call the pointer to our data_set 'sample'.
    data_set *sample;
    sample = data_set_create(2, 3);


    // Step 2: Now that the space has been allocated for our data_set, we need to fill it up.
    // Note that the i^th vector in the data set is stored at sample->data[2*i] and sample->data[2*i+1] since the data points are 2-dimensional real vectors.
    int i;
    for (i = 0; i < 3; i++) {
        sample->data[2*i] = i;
        sample->data[2*i + 1] = i;
    }


    // Step 3: data_set_print allows us to visually check that our 'sample' data_set was created properly.
    data_set_print(sample);


    // Step 4: We can retrieve a copy of each individual vector in the data_set using the extract_data function. Let us extract the second vector, (1, 1), and store it as 'data_point'.
    // Two things to note:
    // 1. The vectors in the data_set are zero-indexed, so that the second vector is indexed by 1.
    // 2. extract_data returns a legitimate COPY of the vector being extracted, not just a reference to it. This means that modifications to the output of extract_data are NOT reflected in the data_set, and vice versa.
    NUMERICAL_TYPE *data_point;
    data_point = extract_data(sample, 1);


    // Step 5: Let us swap the positions of the vectors (0, 0) and (1, 1) in the data_set using data_set_swap.
    data_set_swap(sample, 0, 1);


    // Step 6: Let us check and see if the vector in position 1 (i.e. the second vector) in the data_set is the same as the one we extracted previously.
    // The answer, of course, should be "NO!" since we extracted (1, 1) before swapping (1, 1) with (0, 0).
    NUMERICAL_TYPE *new_data_point;
    new_data_point = extract_data(sample, 1);
    printf("\nThe data_point and new_data_point are ");
    for (i = 0; i < 2; i++) {
        if (data_point[i] != new_data_point[i]) {
            printf("NOT ");
            break;
        }
    }
    printf("the same!\n");


    // Step 7: Once you are done with a data_set created using data_set_create, it is good practice to deallocate the memory. We should also deallocate the memory which stores all the points extracted from the data_set, but ONLY when we are finished with them. After all, they may outlive the data_set in their usefulness.
    data_set_destroy(sample);
    free(data_point);
    free(new_data_point);

    return 0;
}
