
/*****************************************************************************
** Created on: September 23, 2014
**
** Modified:
**
******************************************************************************/

/* Load types */
#include "../../src/neighborly_types.h"

/* Example library */
#include "../../src/structures/kd_tree.h"

/* Standard libraries */
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */
#include "../../src/structures/data_set.h"



int main() {
    // Step 0: Prepare the data_set
    data_set *sample_data;
    sample_data = data_set_create(2, 7);

    // Add (0,0) to the data_set
    sample_data->data[0] = 0;
    sample_data->data[1] = 0;

    // Add (1,0) to the data_set
    sample_data->data[2] = 1;
    sample_data->data[3] = 0;

    // Add (-1,1) to the data_set
    sample_data->data[4] = -1;
    sample_data->data[5] = 1;

    // Add (-1,-1) to the data_set
    sample_data->data[6] = -1;
    sample_data->data[7] = -1;

    // Add (2,-1) to the data_set
    sample_data->data[8] = 2;
    sample_data->data[9] = -1;

    // Add (3,5) to the data_set
    sample_data->data[10] = 3;
    sample_data->data[11] = 5;

    // Add (2,2) to the data_set
    sample_data->data[12] = 2;
    sample_data->data[13] = 2;


    // Step 1: Now that the data_set is ready, let us use it to construct a kd_tree.
    // We will use the kd_tree_construct_fancy function, which cycles through the discriminating keys as it goes down the tree and chooses the admissible record with median key value for insertion at each step.
    kd_tree *treeification;
    treeification = kd_tree_construct_fancy(sample_data);


    // Step 2: Let us perform a breadth-first traversal of the kd_tree, printing the records at each vertex in the order that we visit them.
    // We achieve this with the function kd_tree_enumerate.
    kd_vertex **enumeration;
    enumeration = kd_tree_enumerate(treeification);

    printf("\nBreadth-first traversal of the k-d tree\n");
    int i;
    for (i = 0; i < treeification->num_vertices; i++) {
        printf("Vertex %d: (%f, %f)\n", i, enumeration[i]->record[0], enumeration[i]->record[1]);
    }


    // Step 3: Clean up after ourselves
    free(enumeration);
    kd_tree_destroy(treeification);
    data_set_destroy(sample_data);

    return 0;
}
