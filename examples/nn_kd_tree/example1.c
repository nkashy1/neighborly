
/*****************************************************************************
** Created on: September 24, 2014
**
** Modified:
**
******************************************************************************/

/* Load types */
#include "../../src/neighborly_types.h"

/* Example library */
#include "../../src/nn_kd_tree.h"

/* Standard libraries */
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */
#include "../../src/structures/data_set.h"
#include "../../src/structures/kd_tree.h"
#include "../../src/structures/results.h"


int main() {

    // Step 0: Introducing the major players in our game
    data_set *sample_data;
    NUMERICAL_TYPE query_point[2];
    int num_neighbors;
    NUMERICAL_TYPE p;
    kd_tree *frame_tree;
    results *neighbors;


    // Step 1: Prepare the k-d tree
    sample_data = data_set_create(2, 7);
    sample_data->data[0] = 0;
    sample_data->data[1] = 0;
    sample_data->data[2] = 1;
    sample_data->data[3] = 0;
    sample_data->data[4] = -1;
    sample_data->data[5] = 1;
    sample_data->data[6] = -1;
    sample_data->data[7] = -1;
    sample_data->data[8] = 2;
    sample_data->data[9] = -1;
    sample_data->data[10] = 3;
    sample_data->data[11] = 5;
    sample_data->data[12] = 2;
    sample_data->data[13] = 2;

    frame_tree = kd_tree_construct_fancy(sample_data);


    // Step 2: Accept the query point
    query_point[0] = 5;
    query_point[1] = 5;


    // Step 3: Determine the number of neighbors to be found
    num_neighbors = 2;


    // Step 4: Determine the L_p metric under which to operate
    p = 2.0;


    // Step 5: Find those neighbors
    neighbors = kd_tree_nearest_neighbors(frame_tree, query_point, num_neighbors, p);


    // Step 6: Print the results of the query to the screen
    printf("\nThe %d nearest neighbors of (%f, %f) within the given data set are:\n", num_neighbors, query_point[0], query_point[1]);
    int i;
    for (i = 0; i < num_neighbors; i++) {
        printf("%d. (%f, %f) at distance %f\n", i, neighbors->data[i].data_point[0], neighbors->data[i].data_point[1], neighbors->data[i].distance);
    }


    // Step 7: Free up allocated memory!
    results_destroy(neighbors);
    kd_tree_destroy(frame_tree);
    data_set_destroy(sample_data);


    return 0;
}
