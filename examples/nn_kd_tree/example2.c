
/*****************************************************************************
** Created on: September 25, 2014
**
** Modified:
**
******************************************************************************/

/* Load types */
#include "../../src/neighborly_types.h"

/* Example library */
#include "../../src/nn_kd_tree.h"

/* Standard libraries */
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */
#include "../../src/structures/data_set.h"
#include "../../src/structures/kd_tree.h"
#include "../../src/structures/results.h"


int main() {

    // Step 0: Introducing the major players in our game
    data_set *sample_data;
    int num_neighbors;
    NUMERICAL_TYPE p;
    kd_tree *frame_tree;
    kd_vertex **vertices;
    results **all_neighbors;


    // Step 1: Prepare the k-d tree
    sample_data = data_set_create(2, 7);
    sample_data->data[0] = 0;
    sample_data->data[1] = 0;
    sample_data->data[2] = 1;
    sample_data->data[3] = 0;
    sample_data->data[4] = -1;
    sample_data->data[5] = 1;
    sample_data->data[6] = -1;
    sample_data->data[7] = -1;
    sample_data->data[8] = 2;
    sample_data->data[9] = -1;
    sample_data->data[10] = 3;
    sample_data->data[11] = 5;
    sample_data->data[12] = 2;
    sample_data->data[13] = 2;

    frame_tree = kd_tree_construct_fancy(sample_data);


    // Step 2: Determine the number of neighbors to be found
    num_neighbors = 3;


    // Step 3: Determine the L_p metric under which to operate
    p = 2.0;


    // Step 4: Find those neighbors
    all_neighbors = kd_tree_all_nearest_neighbors(frame_tree, num_neighbors, p);


    // Step 5: Print the results of the query to the screen
    vertices = kd_tree_enumerate(frame_tree);
    int i, j;
    for (i = 0; i < frame_tree->num_vertices; i++) {
        printf("\nThe %d nearest neighbors of (%f, %f) within the given data set are:\n", num_neighbors, vertices[i]->record[0], vertices[i]->record[1]);
        for (j = 0; j < num_neighbors; j++) {
            printf("%d. (%f, %f) at distance %f\n", i, all_neighbors[i]->data[j].data_point[0], all_neighbors[i]->data[j].data_point[1], all_neighbors[i]->data[j].distance);
        }
    }


    // Step 7: Free up allocated memory!
    free(vertices);

    for (i = 0; i < frame_tree->num_vertices; i++) {
        results_destroy(all_neighbors[i]);
    }

    free(all_neighbors);

    kd_tree_destroy(frame_tree);

    data_set_destroy(sample_data);


    return 0;
}
