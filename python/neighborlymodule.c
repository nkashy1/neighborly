
/*************************************************************************************************

 I used the following sources as references during the initial design of this interface:
 1. https://docs.python.org/2/extending/index.html
 2. https://github.com/felipec/libmtag-python

 These sources were very helpful.

 - nkashy1

 *************************************************************************************************/

/* External libraries */
#include <python2.7/Python.h>

/* Standard libraries */
// python2.7/Python.h includes stdio.h, stdlib.h, string.h, and error.h

/* neighborly headers*/
#include "../src/neighborly_types.h"
#include "../src/naive.h"
#include "../src/nn_kd_tree.h"
#include "../src/metrics/distance.h"
#include "../src/structures/common/csv.h"
#include "../src/structures/common/median.h"
#include "../src/structures/common/numerical_heap.h"
#include "../src/structures/common/results_heap.h"
#include "../src/structures/common/spread.h"
#include "../src/structures/data_set.h"
#include "../src/structures/kd_tree.h"
#include "../src/structures/results.h"


// A utility function which converts a results struct into a PyObject*

PyObject* neighborly_BuildResults(results *info, int dimension) {
    PyObject *data = NULL, *datum = NULL, *point = NULL;

    data = PyTuple_New(info->num_neighbors);

    int i, j;
    for (i = 0; i < info->num_neighbors; i++) {
        point = PyTuple_New(dimension);
        for (j = 0; j < dimension; j++) {
            PyTuple_SetItem(point, j, Py_BuildValue("f", info->data[i].data_point[j]));
        }

        datum = PyDict_New();
        PyDict_SetItem(datum, Py_BuildValue("s", "data_point"), point);
        PyDict_SetItem(datum, Py_BuildValue("s", "distance"), Py_BuildValue("f", info->data[i].distance));

        PyTuple_SetItem(data, i, datum);
    }

    return data;
}


// Python interface to data_set struct

typedef struct {
    PyObject_HEAD
    data_set *c_data_set;
} DataSet;


static PyTypeObject DataSetType = {
    PyObject_HEAD_INIT(NULL)
};


static void DataSet_dealloc(DataSet *self) {
    if (self->c_data_set) {
        data_set_destroy(self->c_data_set);
    }

    self->ob_type->tp_free((PyObject*) self);
}


// The __init__ function for DataSet will take a single argument -- a PyObject* which implements the sequence protocol such that its elements are alsoe PyObject* which implement the sequence protocol.
// I refer to this object as matrix in the function body.
static int DataSet_init(DataSet *self, PyObject *args, PyObject *kw) {
    PyObject *matrix = NULL, *point = NULL, *value = NULL;

    int dimension, num_points, i, j;

    char *kwlist[] = {"matrix", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kw, "|O", kwlist, &matrix)) {
        return -1;
    }

    if (matrix) {
        num_points = PySequence_Length(matrix);
        if (num_points <= 0) {
            PyErr_SetString(PyExc_ValueError, "Data matrix must be a sequence of positive length.");
            goto error;
        }

        point = PySequence_GetItem(matrix, 0);
        dimension = PySequence_Length(point);
        Py_DECREF(point);
        if (dimension <= 0) {
            PyErr_SetString(PyExc_ValueError, "Data points must be a sequence of positive length.");
            goto error;
        }

        self->c_data_set = data_set_create(dimension, num_points);

        for (i = 0; i < num_points; i++) {
            point = PySequence_GetItem(matrix, i);
            for (j = 0; j < dimension; j++) {
                value = PySequence_GetItem(point, j);
                self->c_data_set->data[i*dimension + j] = (NUMERICAL_TYPE) PyFloat_AsDouble(value);
                Py_DECREF(value);
            }
            Py_DECREF(point);
        }

        return 0;

        error:
        return -1;
    } else {
        self->c_data_set = NULL;
        return 0;
    }
}


static PyObject* DataSet_extract_data(DataSet* self, PyObject *args) {

    if (!self->c_data_set) {
        PyErr_SetString(PyExc_AttributeError, "The DataSet is empty.");
        return NULL;
    }

    PyObject *point;
    point = PyTuple_New(self->c_data_set->embedding_dimension);

    NUMERICAL_TYPE *c_point;
    int i;

    PyArg_ParseTuple(args, "i", &i);

    c_point = extract_data(self->c_data_set, i);

    for (i = 0; i < self->c_data_set->embedding_dimension; i++) {
        PyTuple_SET_ITEM(point, i, Py_BuildValue("f", c_point[i]));
    }

    return point;
}


static PyObject* DataSet_display(DataSet *self) {
    if (!self->c_data_set) {
        PyErr_SetString(PyExc_AttributeError, "The DataSet is empty.");
        return NULL;
    }

    data_set_print(self->c_data_set);

    return Py_BuildValue("i", 0);
}


static Py_ssize_t DataSet_len(DataSet *self) {
    if (!self->c_data_set) {
        PyErr_SetString(PyExc_AttributeError, "The DataSet is empty.");
        return NULL;
    }

    return self->c_data_set->num_data_points;
}


static PyObject* DataSet_item(DataSet *self, Py_ssize_t i) {

    if (!self->c_data_set) {
        PyErr_SetString(PyExc_AttributeError, "The DataSet is empty.");
        return NULL;
    }

    PyObject *value;
    value = NULL;

    if (0 <= i && i < self->c_data_set->num_data_points) {
        PyObject *args;
        args = Py_BuildValue("(i)", i);
        value = DataSet_extract_data(self, args);
        Py_DECREF(args);
    } else {
        PyErr_SetString(PyExc_IndexError, "DataSet index out of bounds.");
    }

    return value;
}


static PyMethodDef DataSet_methods[] = {
    {"extract_data", (PyCFunction) DataSet_extract_data, METH_VARARGS, "Extract a specified point from the DataSet."},
    {"display", (PyCFunction) DataSet_display, METH_NOARGS, "Print the contents of the DataSet."}
};


static PySequenceMethods DataSet_seq = {
    DataSet_len,                // sq_length
    0,                          // sq_concat
    0,                          // sq_repeat
    DataSet_item,               // sq_item
};



// Python interface to the kd_tree struct

typedef struct {
    PyObject_HEAD
    kd_tree *c_kd_tree;
} KDTree;


static PyTypeObject KDTreeType = {
    PyObject_HEAD_INIT(NULL)
};


static void KDTree_dealloc(KDTree *self) {
    kd_tree_destroy(self->c_kd_tree);
    self->ob_type->tp_free((PyObject*) self);
}


static int KDTree_init(KDTree *self, PyObject *args, PyObject *kw) {
    DataSet *data = NULL;

    char *kwlist[] = {"data", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kw, "|O", kwlist, &data)) {
        return -1;
    }

    if (data) {
        self->c_kd_tree = kd_tree_construct_fancy(data->c_data_set);
    } else {
        self->c_kd_tree = NULL;
    }

    return 0;
}


static PyObject* KDTree_call(KDTree *self, PyObject *args, PyObject *kw) {
    PyObject *output = NULL;

    PyObject *point = NULL, *value = NULL;
    int num_neighbors = 1;
    NUMERICAL_TYPE p = 2.0;

    int i;

    char *kwlist[] = {"num_neighbors", "p", "point", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kw, "|ifO", kwlist, &num_neighbors, &p, &point)) {
        return -1;
    }

    if (point) {
        results *c_output;
        NUMERICAL_TYPE c_point[self->c_kd_tree->dimension];
        for (i = 0; i < self->c_kd_tree->dimension; i++) {
            value = PySequence_GetItem(point, i);
            c_point[i] = PyFloat_AsDouble(value);
        }

        c_output = nn_streaming_kd_tree(self->c_kd_tree, c_point, num_neighbors, p);
        output = neighborly_BuildResults(c_output, self->c_kd_tree->dimension);
    } else {
        if (num_neighbors == 1) {
            num_neighbors++;
        }

        results **c_output;
        NUMERICAL_TYPE temp[2] = {0,0};
        c_output = nn_all_kd_tree(self->c_kd_tree, num_neighbors, p);

        output = PyTuple_New(self->c_kd_tree->num_vertices);
        for (i = 0; i < self->c_kd_tree->num_vertices; i++) {
            PyTuple_SetItem(output, i, neighborly_BuildResults(c_output[i], self->c_kd_tree->dimension));
        }
    }

    return output;
}


static PyMethodDef* KDTree_methods = NULL;



// Module initialization

PyMethodDef module_methods[] = {
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initneighborly() {
    PyObject *m;
    m = Py_InitModule("neighborly", module_methods);

    DataSetType.tp_new = PyType_GenericNew;
    DataSetType.tp_init = (initproc) DataSet_init;
    DataSetType.tp_name = "neighborly.DataSet";
    DataSetType.tp_basicsize = sizeof(DataSet);
    DataSetType.tp_dealloc = (destructor) DataSet_dealloc;
    DataSetType.tp_flags = Py_TPFLAGS_DEFAULT;
    DataSetType.tp_methods = DataSet_methods;
    DataSetType.tp_as_sequence = &DataSet_seq;

    if (PyType_Ready(&DataSetType) < 0) {
        return;
    }

    Py_INCREF(&DataSetType);
    PyModule_AddObject(m, "DataSet", (PyObject*) &DataSetType);


    KDTreeType.tp_new = PyType_GenericNew;
    KDTreeType.tp_init = (initproc) KDTree_init;
    KDTreeType.tp_name = "neighborly.KDTree";
    KDTreeType.tp_basicsize = sizeof(KDTree);
    KDTreeType.tp_dealloc = (destructor) KDTree_dealloc;
    KDTreeType.tp_flags = Py_TPFLAGS_DEFAULT;
    KDTreeType.tp_methods = KDTree_methods;
    KDTreeType.tp_call = (ternaryfunc) KDTree_call;

    if (PyType_Ready(&KDTreeType) < 0) {
        return;
    }

    Py_INCREF(&KDTreeType);
    PyModule_AddObject(m, "KDTree", (PyObject*) &KDTreeType);
}
