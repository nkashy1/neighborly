
from distutils.core import setup
from distutils.core import Extension

files_top = ["../src/naive.c", "../src/nn_kd_tree.c"]
files_metrics = ["../src/metrics/distance.c"]
files_common = ["../src/structures/common/csv.c", "../src/structures/common/median.c", "../src/structures/common/numerical_heap.c", "../src/structures/common/results_heap.c", "../src/structures/common/spread.c"]
files_structures = ["../src/structures/data_set.c", "../src/structures/kd_tree.c", "../src/structures/results.c"]

module1 = Extension(name = "neighborly",
                    sources = ["neighborlymodule.c"] + files_top + files_metrics + files_common + files_structures,
                    include_dirs = ["/usr/include", "/usr/local/include"],
                    library_dirs = ["/usr/lib", "/usr/local/lib"],
                    libraries = ["m", "python2.7"],
                    extra_compile_args = ["-O3"],
                    extra_link_args = ["-O3"]
                   )


setup(name = "neighborly",
      version = "0.2",
      description = "Speedy nearest neighbor calculations!",
      author = "Neeraj Kashyap",
      author_email = "nkashy1@gmail.com",
      url = "https://bitbucket.org/nkashy1/neighborly",
      ext_modules = [module1]
      )
