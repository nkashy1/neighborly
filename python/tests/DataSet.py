import neighborly
import numpy as np

matrix = np.array([np.array([1.0, 2.0]), np.array([3.0, 4.0])])

D = neighborly.DataSet(matrix)

print "Testing the extract_data method:"
print D.extract_data(0)
print D.extract_data(1)

print "Testing the print method"
D.display()

print "The number of data points in D is"
print len(D)

print "The first item in D is"
print D[0]

for p in D:
	print p
