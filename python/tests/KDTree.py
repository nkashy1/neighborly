import neighborly
import numpy as np

matrix = np.array([np.array([1.0, 2.0]), np.array([3.0, 4.0])])

D = neighborly.DataSet(matrix)
T = neighborly.KDTree(D)

results = T()

for result in results:
    print result
