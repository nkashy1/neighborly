# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/neeraj/.spyder2/.temp.py
"""

import neighborly as nb
import numpy as np
import scipy.spatial as sp
import time

A = 10000*np.random.rand(100000, 2)
point = list(1000*np.random.rand(1, 2))[0]

D = nb.DataSet(A)

start = time.time()
T = nb.KDTree(D)
end = time.time()

contime = end - start


start = time.time()
results = T(1, 2, point)[0]
end = time.time()

runtime = end - start

print "Module: neighborly"
print "Query point:", point
print "Construction time:", contime
print "Query time:", runtime
print "Nearest neighbor:", results['data_point']
print "Distance:", results['distance']


start = time.time()
S = sp.cKDTree(A)
end = time.time()

contime = end - start


start = time.time()
(dist, neigh) = S.query(point)
end = time.time()

runtime = end - start

print "\nModule: scipy"
print "Query point:", point
print "Construction time:", contime
print "Query time:", runtime
print "Nearest neighbor:", S.data[neigh]
print "Distance:", dist
