
/* Header file */
#include "distance.h"

/* Standard Libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* Custom Libraries */
#include "../structures/data_set.h"

NUMERICAL_TYPE distance_L_p(int n, NUMERICAL_TYPE* P, NUMERICAL_TYPE* Q, NUMERICAL_TYPE p) {
    NUMERICAL_TYPE distance = 0;
    int i; // Looper

    for(i = 0; i < n ; i++){
        distance += pow(ABS(P[i] - Q[i]), p);
    }
    distance = pow(distance, 1/p);

    return distance;
}



NUMERICAL_TYPE distance_L_p_power(int n, NUMERICAL_TYPE* P, NUMERICAL_TYPE* Q, NUMERICAL_TYPE p) {
    NUMERICAL_TYPE distance = 0;
    int i; // Looper

    for(i = 0; i < n ; i++){
        distance += pow(ABS(P[i] - Q[i]), p);
    }

    return distance;
}



NUMERICAL_TYPE distance_sup(int n, NUMERICAL_TYPE* P, NUMERICAL_TYPE* Q) {
    NUMERICAL_TYPE distance = 0, component;
    int i;

    for (i = 0; i < n; i++){
        component = ABS(P[i] - Q[i]);
        if (distance < component) {
            distance = component;
        }
    }

    return distance;
}



int distance_matrix_index_compression(int num_data_points, int i, int j){
    int row_index, col_index;
    if (i < j) {
        row_index = i;
        col_index = j;
    } else {
        row_index = j;
        col_index = i;
    }

    int row_offset = (row_index)*num_data_points - ((row_index + 1)*row_index)/2;

    return row_offset + col_index - row_index - 1;
}



void distance_matrix_destroy(distance_matrix* obsolete_distance_matrix) {
    free(obsolete_distance_matrix->distances);
    free(obsolete_distance_matrix);
}



distance_matrix* distance_matrix_create(data_set const *D) {
    int n = D->embedding_dimension, N = D->num_data_points;
    int num_compressed_indices = (N*(N-1))/2;
    int compressed_index;
    NUMERICAL_TYPE *P, *Q;

    int i, j;

    distance_matrix* pairwise_distances = malloc(sizeof(distance_matrix));
    assert(pairwise_distances != NULL);

    pairwise_distances->num_points = N;

    pairwise_distances->distances = malloc(sizeof(NUMERICAL_TYPE)*num_compressed_indices);
    assert(pairwise_distances->distances != NULL);

    for (i = 0; i < N; i++) {
        for (j = i + 1; j < N; j++) {
            compressed_index = distance_matrix_index_compression(N, i, j);
            P = extract_data(D, i);
            Q = extract_data(D, j);
            pairwise_distances->distances[compressed_index] = distance_L_p(n, P, Q, 2.0);
        }
    }

    return pairwise_distances;
}



NUMERICAL_TYPE extract_distance(distance_matrix const *pairwise_distances, int i, int j) {
    int num_data_points = pairwise_distances->num_points;
    int compressed_index = distance_matrix_index_compression(num_data_points, i, j);

    return pairwise_distances->distances[compressed_index];
}
