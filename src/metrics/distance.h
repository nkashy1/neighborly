
#include "../neighborly_types.h"

#ifndef DISTANCE_H
#define DISTANCE_H



/********************************************************
** distance_matrix
** Definition of structure and utility functions.
*********************************************************/

// The distance_matrix structure is meant to hold a compressed form of the matrix of pairwise distances between points in a given data set.
// As the aforementioned matrix is symmetric and is 0 along its diagonal, distance_matrix simply holds the entries of the matrix where the column index exceeds the row index.
// Associated methods:  (1) distance_matrix_index_compression
//                      (2) create_distance_matrix
//                      (3) extract_distance
//                      (4) distance_matrix_destroy

struct distance_matrix{
    int num_points;
    NUMERICAL_TYPE* distances;
};



// distance_matrix_index_compression:
//
// Description: Takes a pair of data point indices and returns the index at which the distance between the data points is stored in the 'distances' array of the distance_matrix struct.
//
// Inputs:  (1) int num_data_points, the size of the underlying data set.
//          (2) int i, the index of the first data point.
//          (3) int j, the index of the second data point.
//
// Output:  The index at which the corresponding distance data would be stored in the 'distances' field of a distance_matrix.
//
// Notes:
//  (1) It is assumed that i and j are distinct. The distance between any point and itself is 0, and this universal information is NOT stored in a distance_matrix.
//  (2) All indices begin at 0.

int distance_matrix_index_compression(int num_data_points, int i, int j);



// 'distance_matrix_destroy':
//
// Description: Destroys a dynamically allocated distance matrix.
//
// Inputs:  (1) distance_matrix* obsolete_distance_matrix
//
// Output:

void distance_matrix_destroy(distance_matrix* obsolete_distance_matrix);



// 'distance_matrix_create':
//
// Inputs:  (1) data_set D
//
// Output:  A pointer to the corresponding distance_matrix.
//
// Notes:
//  (1) The output points to a dynamically allocated distance_matrix. Remember to manually deallocate it when finished with it!

distance_matrix* distance_matrix_create(data_set const *D);



// 'extract_distance':
//
// Description: Takes indices for a pair of data points and extracts the distance between those points in the given distance_matrix.
//
// Inputs:  (1) distance_matrix* pairwise_distances, a pointer to the relevant distance_matrix.
//          (2) int i, the index of the first data point.
//          (3) int j, the index of the second data point.
//
// Output:  The distance between data point i and data point j.

NUMERICAL_TYPE extract_distance(distance_matrix const *pairwise_distances, int i, int j);



/********************************************************
** distance functions
*********************************************************/

// 'distance_L_p':
//
// Inputs:  (1) A dimension 'n' of the Euclidean space one is working in.
//          (2) A point 'P' in R^'n'.
//          (3) A point 'Q' in R^'n'.
//          (4) A NUMERICAL_TYPE 'p' specifying the metric to be used on R^'n'.
//
// Outputs: (1) A NUMERICAL_TYPE which quantifies the distance between 'P' and 'Q' according to the given 'metric'.
//
// Notes:
//  (1) The metric used by the function corresponds to the p-norm on R^'n'.
//  (2) The sup-norm is handled with the distance_sup function so as to eliminate the overhead of a conditional.

NUMERICAL_TYPE distance_L_p(int n, NUMERICAL_TYPE* P, NUMERICAL_TYPE* Q, NUMERICAL_TYPE p);



// 'distance_sup':
//
// Inputs:  (1) A dimension 'n' of the Euclidean space one is working in.
//          (2) A point 'P' in R^'n'.
//          (3) A point 'Q' in R^'n'.
//
// Outputs: (1) A NUMERICAL_TYPE which quantifies the L_{\infty}-distance between 'P' and 'Q'.
//
// Notes:
//

NUMERICAL_TYPE distance_sup(int n, NUMERICAL_TYPE* P, NUMERICAL_TYPE* Q);



// 'distance_L_p_power':
// Computes the p^th power of the L_p distance between two points.
//
// Inputs:  (1) A dimension 'n' of the Euclidean space one is working in.
//          (2) A point 'P' in R^'n'.
//          (3) A point 'Q' in R^'n'.
//          (4) A NUMERICAL_TYPE 'p' specifying the L_p-metric to be used on R^'n'.
//
// Outputs: (1) A NUMERICAL_TYPE which quantifies the p^th power of the distance between 'P' and 'Q' according to the given 'metric'.
//
// Notes:
//  (1) This function allows saves us the overhead of computing the p^th root if we don't have to.

NUMERICAL_TYPE distance_L_p_power(int n, NUMERICAL_TYPE* P, NUMERICAL_TYPE* Q, NUMERICAL_TYPE p);



#endif // DISTANCE_H
