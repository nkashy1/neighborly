
/* Header file */
#include "naive.h"

/* Standard Libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom Libraries */
#include "structures/data_set.h"
#include "metrics/distance.h"



NUMERICAL_TYPE naive_nearest_neighbor_distance(data_set const *D, NUMERICAL_TYPE* P){
    int i;
    int n = D->embedding_dimension, N = D->num_data_points;
    NUMERICAL_TYPE* Q = extract_data(D, 0);
    NUMERICAL_TYPE nearest_neighbor_distance = distance_L_p(n, P, Q, 2), new_distance;

    for (i = 1; i < N; i++){
        Q = extract_data(D, i);
        new_distance = distance_L_p(n, P, Q, 2.0);
        if (0 < new_distance && new_distance < nearest_neighbor_distance) {
            nearest_neighbor_distance = new_distance;
        }
    }

    return nearest_neighbor_distance;
}



NUMERICAL_TYPE* naive_all_nearest_neighbor_distances(distance_matrix const *pairwise_distances) {
    int i, j;

    int num_data_points = pairwise_distances->num_points;

    NUMERICAL_TYPE *nearest_neighbor_distance_list = malloc(sizeof(NUMERICAL_TYPE)*num_data_points);

    NUMERICAL_TYPE current_distance, fresh_distance;

    for (i = 0; i < num_data_points; i++) {

        current_distance = 0;

        for (j = 0; j < num_data_points; j++) {
            if (j == i) {
                continue;
            } else {
                fresh_distance = extract_distance(pairwise_distances, i, j);
                if (current_distance == 0 || (0 < fresh_distance && fresh_distance < current_distance)) {
                    current_distance = fresh_distance;
                }
            }
        }

        nearest_neighbor_distance_list[i] = current_distance;
    }

    return nearest_neighbor_distance_list;
}

