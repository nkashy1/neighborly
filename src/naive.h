
#ifndef NAIVE_H
#define NAIVE_H


#include "neighborly_types.h"


// 'naive_nearest_neighbor_distance':
//
// Inputs:  (1) An integer 'n', representing the embedding dimension of the data.
//          (2) A point 'P' in R^'n'.
//          (3) An integer 'N', representing the number of data points from which to check the distance to 'P'.
//          (4) A list of 'N' points 'D' in R^'n', the data set against which to compare 'P'.
//
// Outputs: (1) The L_2-distance from 'P' to its nearest non-identical neighbor in the set 'D'.
//
// Notes:
//  (1) 'P' is an array of NUMERICAL_TYPEs of size 'n'.
//  (2) 'D' is an array of NUMERICAL_TYPEs of size 'n'*'N' and it is assumed that 'N' is at least 1.

NUMERICAL_TYPE naive_nearest_neighbor_distance(data_set const *D, NUMERICAL_TYPE* P);



// 'naive_all_nearest_neighbor_distances':
//
// Inputs:  (1) A pointer to a distance matrix which encodes the distances for the desired data set.
//
// Output: A NUMERICAL_TYPE array of nearest neighbor distances for the points in the data set.
//
// Notes:

NUMERICAL_TYPE* naive_all_nearest_neighbor_distances(distance_matrix const *pairwise_distances);


#endif // NAIVE_H
