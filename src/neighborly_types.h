


#ifndef NEIGHBORLY_TYPES_H
#define NEIGHBORLY_TYPES_H

#define NUMERICAL_TYPE float
#define ABS fabsf

typedef struct data_set data_set;

typedef struct distance_matrix distance_matrix;

// kd_tree types
typedef struct kd_vertex kd_vertex;
typedef struct kd_tree kd_tree;

// results types
typedef struct results_datum results_datum;
typedef struct results results;

// Heap and heap data types
typedef struct numerical_heap numerical_heap;
typedef struct results_heap results_heap;

// csv data types
typedef struct csv_info csv_info;

#endif // NEIGHBORLY_TYPES_H
