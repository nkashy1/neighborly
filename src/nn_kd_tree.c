
/* Header file */
#include "nn_kd_tree.h"

/* Standard libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */
#include "metrics/distance.h"
#include "structures/data_set.h"
#include "structures/kd_tree.h"
#include "structures/results.h"
#include "structures/common/results_heap.h"


results* nn_streaming_kd_tree(kd_tree *frame_tree, NUMERICAL_TYPE *point, int num_neighbors, NUMERICAL_TYPE p) {

    // This function performs its work in two stages -- the traversal stage and the examination stage.
    //
    // The traversal stage is initiated with a 'current' vertex. The function traverses the frame_tree starting at that point and does two things:
    // 1. It checks if each point it visits has a smaller distance from target 'point' than the current furthest admissible nearest neighbor. If so, it removes the now obsolete potential neighbor from the priority queue of potential neighbors, and inserts the current vertex in the traversal into the priority queue.
    // 2. It adds each vertex it visits during the traversal to the 'examine' array, which is iterated through in the examination stage.
    //
    // The function begins in the traversal stage with the root of the frame_tree as its 'current' vertex. Traversal continues until the function reaches a leaf in the frame_tree, in which case it switches to the examination phase.
    //
    // In the examination stage, the vertices in the 'examine' array are checked, starting from the end of the array and going towards its head, for whether or not it would be possible for a an admissible neighbor of 'point' to lie on the other side of the hyperplane used to split the data at that vertex.
    // If it is possible that an admissible neighbor of 'point' lies on the other side of that vertex, then the function sets the 'current' vertex to be the as yet unvisited child of the vertex which is currently being examined and it shifts back into the traversal phase.
    //
    // This procedure terminates as no vertex can be set as the 'current' vertex more than one time and not can any vertex be examined more than once.


    // 'potential_neighbors' is the priority queue which will holds the best guesses for the nearest neighbors of 'point' as the function traverses the 'frame_tree'.
    // When all the relevant nodes in 'frame_tree' have been examined, the point-distance pairs (which are stored as results_datum structs) in 'potential_neighbors' are transferred from the 'potential_neighbors' heap to the 'neighbors' array.
    // 'potential_neighbors' is a max heap and so the points in 'neighbors' are sorted in descending order of distances from 'point'.
    results_heap *potential_neighbors;
    results *neighbors;

    neighbors = results_create(num_neighbors);
    potential_neighbors = results_heap_create(NULL, 0, num_neighbors, results_gt);
    results_datum vertices[num_neighbors];
    potential_neighbors->vertices = vertices;    // VERY IMPORTANT TO ALLOCATE MEMORY TO THIS FIELD!


    // The work horses.
    kd_vertex *current;
    kd_vertex **examine;
    int to_examine = 1, branch, i;
    int traverse = 1;

    examine = malloc(sizeof(kd_vertex*)*(frame_tree->num_vertices));


    // 'populator' simply holds the results_datum structures that we initially populate 'potential_neighbors' with.
    results_datum populator;
    NUMERICAL_TYPE distance;


    // Initialization.
    current = frame_tree->root;
    examine[0] = current;


    while (to_examine > 0) {
        if (traverse == 1) {
            // REFACTOR: Why don't I just define distance up here? I am calculating it in either case of the following if statement...
            if (potential_neighbors->current_size < potential_neighbors->max_size) {
                populator.data_point = current->record;
                populator.distance = distance_L_p_power(frame_tree->dimension, current->record, point, p);
                results_heap_insert(populator, potential_neighbors);
            } else {
                distance = distance_L_p_power(frame_tree->dimension, point, current->record, p);
                if (distance < potential_neighbors->vertices[0].distance) {
                    potential_neighbors->vertices[0].data_point = current->record;
                    potential_neighbors->vertices[0].distance = distance;
                    results_heap_down(0, potential_neighbors);
                }
            }

            branch = (point[current->disc_key] > current->record[current->disc_key]);
            if (current->children[branch] == NULL) {
                traverse = 0;

                if (point[current->disc_key] == current->record[current->disc_key] && current->children[1 - branch] != NULL) {
                    traverse = 1;
                    branch = 1 - branch;
                    examine[to_examine] = current->children[branch];
                    current = current->children[branch];
                    to_examine++;
                }
            } else {
                examine[to_examine] = current->children[branch];
                current = current->children[branch];
                to_examine++;
            }
        } else {
            i = to_examine - 1;
            while (i >= 0) {
                distance = distance_L_p_power(1, point + examine[i]->disc_key, examine[i]->record + examine[i]->disc_key, p);

                if (distance < potential_neighbors->vertices[0].distance) {
                    branch = (point[examine[i]->disc_key] <= examine[i]->record[examine[i]->disc_key]);
                    if (examine[i]->children[branch] != NULL) {
                        current = examine[i]->children[branch];
                        traverse = 1;
                        examine[to_examine - 1] = current;
                        break;
                    }
                }

                i--;
                to_examine--;

            }
        }

    }

    // We are now done with 'examine'
    free(examine);

    // Allocate the neighbors before returning. This makes it easy to destroy all our allocated variables.
    branch = 0; // Using this as an index for results
    while (potential_neighbors->current_size > 0) {
        neighbors->data[branch] = potential_neighbors->vertices[0];
        neighbors->data[branch].distance = pow(neighbors->data[branch].distance, 1/p);
        branch++;
        results_heap_delete(0, potential_neighbors);
    }


    // FREE EVERYTHING
    //free(potential_neighbors->vertices);
    free(potential_neighbors);


    // Phew!
    return neighbors;
}



// NOTE: The following function is identical to nn_streaming_kd_tree except for the following changes:
//  (1) potential_neighbors is now an argument to the function and no longer has to be defined within it.
//  (2) num_neighbors is replaced by potential_neighbors->max_size.
//  (3) potential_neighbors is no longer destroyed at the end, which required some modifications to the production of the return value.

results* nn_streaming_kd_tree_with_prep(kd_tree *frame_tree, NUMERICAL_TYPE *point, results_heap *potential_neighbors, NUMERICAL_TYPE p){
    results *neighbors;

    neighbors = results_create(potential_neighbors->max_size);

    // The work horses.
    kd_vertex *current;
    kd_vertex **examine = malloc(sizeof(kd_vertex*)*(frame_tree->num_vertices));
    int to_examine = 1, branch, i;
    int traverse = 1;


    // 'populator' simply holds the results_datum structures that we initially populate 'potential_neighbors' with.
    results_datum populator;
    NUMERICAL_TYPE distance;


    // Initialization.
    current = frame_tree->root;
    examine[0] = current;


    while (to_examine > 0) {
        if (traverse == 1) {
            // REFACTOR: Why don't I just define distance up here? I am calculating it in either case of the following if statement...
            if (potential_neighbors->current_size < potential_neighbors->max_size) {
                populator.data_point = current->record;
                populator.distance = distance_L_p_power(frame_tree->dimension, current->record, point, p);
                results_heap_insert(populator, potential_neighbors);
            } else {
                distance = distance_L_p_power(frame_tree->dimension, point, current->record, p);
                if (distance < potential_neighbors->vertices[0].distance) {
                    potential_neighbors->vertices[0].data_point = current->record;
                    potential_neighbors->vertices[0].distance = distance;
                    results_heap_down(0, potential_neighbors);
                }
            }

            branch = (point[current->disc_key] > current->record[current->disc_key]);
            if (current->children[branch] == NULL) {
                traverse = 0;
                if (point[current->disc_key] == current->record[current->disc_key] && current->children[1 - branch] != NULL) {
                    traverse = 1;
                    branch = 1 - branch;
                    examine[to_examine] = current->children[branch];
                    current = current->children[branch];
                    to_examine++;
                }
            } else {
                examine[to_examine] = current->children[branch];
                current = current->children[branch];
                to_examine++;
            }
        } else {
            i = to_examine - 1;
            while (i >= 0) {
                distance = distance_L_p_power(1, point + examine[i]->disc_key, examine[i]->record + examine[i]->disc_key, p);

                if (distance < potential_neighbors->vertices[0].distance) {
                    branch = (point[examine[i]->disc_key] <= examine[i]->record[examine[i]->disc_key]);
                    if (examine[i]->children[branch] != NULL) {
                        current = examine[i]->children[branch];
                        traverse = 1;
                        examine[to_examine - 1] = current;
                        break;
                    }
                }

                i--;
                to_examine--;

            }
        }

    }

    // Allocate the neighbors before returning. This makes it easy to destroy all our allocated variables.
    results_heap *temp_heap;
    NUMERICAL_TYPE temp_record[frame_tree->dimension];

    temp_heap = results_heap_copy(potential_neighbors, frame_tree->dimension);
    branch = 0; // Using this as an index for results
    while (temp_heap->current_size > 0) {
        populator = temp_heap->vertices[0];
        populator.distance = pow(populator.distance, 1/p);
        for (i = 0; i < frame_tree->dimension; i++) {
            temp_record[i] = populator.data_point[i];
        }
        populator.data_point = temp_record;
        neighbors->data[branch] = populator;
        branch++;
        results_heap_delete(0, temp_heap);
    }


    // FREE EVERYTHING
    free(examine);
    free(temp_heap->vertices);
    free(temp_heap);


    // Phew!
    return neighbors;
}



results** nn_all_kd_tree(kd_tree *frame_tree, int num_neighbors, NUMERICAL_TYPE p) {
    kd_vertex **vertices;
    results **neighbors;
    int i;

    vertices = kd_tree_enumerate(frame_tree);
    neighbors = malloc(sizeof(results*)*frame_tree->num_vertices);

    for (i = 0; i < frame_tree->num_vertices; i++) {
        neighbors[i] = nn_streaming_kd_tree(frame_tree, vertices[i]->record, num_neighbors, p);
    }

    free(vertices);

    return neighbors;
}
