
#ifndef NN_KD_TREE_H
#define NN_KD_TREE_H

#include "neighborly_types.h"

/**************************************************************************
** Find the nearest neighbors of a point using the kd_tree-based algorithm of Friedman, Bentley, and Finkel.
** Reference: Jerome H. Friedman, Jon Louis Bentley, Raphael Ari Finkel, "An Algorithm for Finding Best Matches in Logarithmic Expected Time", ACM Transactions on Mathematical Software, Volume 3 (3), September 1977, Pages 209-226.
**
***************************************************************************/



// nn_streaming_kd_tree
// A kd_tree-based algorithm to solve the streaming nearest neighbors problem.
//
// Inputs:  (1) kd_tree *frame_tree, a kd_tree built out of the reference points for the streaming nearest neighbors problem
//          (2) NUMERICAL_TYPE *point, the point whose nearest neighbors we would like to find from among the reference points
//          (3) int num_neighbors, the number of nearest neighbors we would like to find
//          (4) NUMERICAL_TYPE p, specifies the L_p metric under which we would like to find the nearest neighbors
//
// Output:  results*, the results of the streaming nearest neighbor query.
//
// Notes:
//  (1) This function is a purely iterative implementation of the algorithm of Friedman, Bentley, and Finkel. The reason for this is that we do not know in advance how deep the frame_tree will be and the extent to which we will have to recursively examine branches of it. The recursive approach could lead to complaints from the stack.
//

results* nn_streaming_kd_tree(kd_tree *frame_tree, NUMERICAL_TYPE *point, int num_neighbors, NUMERICAL_TYPE p);



// nn_streaming_kd_tree_with_prep
// A kd_tree-based algorithm to solve the streaming nearest neighbors problem. This function accepts a pointer to a results_heap which can already contain information about the potential neighbors of the query point.
//
// Inputs:  (1) kd_tree *frame_tree, a kd_tree built out of the reference points for the streaming nearest neighbors problem
//          (2) NUMERICAL_TYPE *point, the point whose nearest neighbors we would like to find from among the reference points
//          (3) results_heap *potential_neighbors, a heap containing a list of potential neighbors of the query point and which is also responsible for specifying the number of nearest neighbors to find
//          (4) NUMERICAL_TYPE p, specifies the L_p metric under which we would like to find the nearest neighbors
//
// Output:  results*, the results of the streaming nearest neighbor query.
//
// Notes:
//  (1) This function is a purely iterative implementation of the algorithm of Friedman, Bentley, and Finkel. The reason for this is that we do not know in advance how deep the frame_tree will be and the extent to which we will have to recursively examine branches of it. The recursive approach could lead to complaints from the stack.
//

results* nn_streaming_kd_tree_with_prep(kd_tree *frame_tree, NUMERICAL_TYPE *point, results_heap *potential_neighbors, NUMERICAL_TYPE p);



// nn_all_kd_tree
// A naive kd_tree-based algorithm to solve the all nearest neighbors problem. This algorithm is naive because it simply applies the nn_streaming_kd_tree function individually with each point in the kd_tree as the target point.
//
// Inputs:  (1) kd_tree *frame_tree, a pointer to the kd_tree within which we would like to solve the all nearest neighbors problem
//          (2) int num_neighbors, the number of nearest neighbors we would like to find
//          (3) NUMERICAL_TYPE p, specifies the L_p metric under which we would like to find the nearest neighbors
//
// Output:  results**, an array of pointers to the nearest neighbor 'results' for each point in the frame_tree.
//
// Notes:
//

results** nn_all_kd_tree(kd_tree *frame_tree, int num_neighbors, NUMERICAL_TYPE p);

#endif // NN_KD_TREE_H
