

/* Header file */
#include "csv.h"

/* Standard libraries */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Custom libraries */


csv_info csv_loadinfo(char *filename, char delimiter) {
    csv_info info;
    info.filename = filename;
    info.delimiter = delimiter;
    info.num_fields = 0;
    info.num_rows = 0;

    FILE *ifp;
    ifp = fopen(filename, "r");
    if (ifp == NULL) {
        perror(filename);
    }
    assert(ifp != NULL);

    int cursor;
    cursor = fgetc(ifp);
    if (cursor != EOF) {
        info.num_fields = 1;

        while (cursor != '\n' && cursor != EOF) {
            if (cursor == delimiter) {
                info.num_fields++;
            }

            cursor = fgetc(ifp);
        }

        while (cursor != EOF) {
            if (cursor == '\n') {
                info.num_rows++;
            }

            cursor = fgetc(ifp);
        }
    }

    fclose(ifp);

    return info;
}



csv_info csv_createinfo(char* filename, char delimiter, int num_fields, int num_rows) {
    csv_info info;
    info.filename = filename;
    info.delimiter = delimiter;
    info.num_fields = num_fields;
    info.num_rows = num_rows;

    return info;
}
