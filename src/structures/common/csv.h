
#ifndef CSV_H
#define CSV_H


#include "../../neighborly_types.h"



/*******************************************************************************
 csv.h and csv.c contain utilities for reading from and writing to .csv files.
 *******************************************************************************/

// The csv_info struct holds
struct csv_info {
    char *filename;
    int num_fields;
    int num_rows;
    char delimiter;
};


// csv_loadinfo
// Returns a csv_info struct corresponding to a given csv file with a specified delimiter.
//
// Inputs:
//  (1) char *filename, the name of the csv file
//  (2) char delimiter, the delimiter used in the csv file to separate fields
//
// Outputs: csv_info, csv_info corresponding to 'filename'
//
// Notes:
//

csv_info csv_loadinfo(char *filename, char delimiter);



// csv_createinfo
// Returns a csv_info struct containing the information passed to it.
//
// Inputs:
//  (1) char *filename, the file name of the relevant csv file
//  (2) char delimiter, the field delimiter used in the file
//  (3) int num_fields, the number of fields per row of the file
//  (4) int num_rows, the number of rows in the file
//
// Outputs: csv_info, the csv_info structure containing all the input information
//
// Notes:
//

csv_info csv_createinfo(char* filename, char delimiter, int num_fields, int num_rows);


#endif // CSV_H
