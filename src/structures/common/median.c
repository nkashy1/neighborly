
/* Header file */
#include "median.h"

/* Standard libraries */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */
#include "../data_set.h"


void quick_select_old(data_set *samples, int axis) {
    if (samples->num_data_points > 2) {
        int examiner, saver, pivot, median_position;
        median_position = (samples->num_data_points + 1)/2 - 1;

        examiner = 0;
        saver = 0;
        pivot = samples->num_data_points - 1;

        // Is this kind of looping bad form?
        for(;;) {
            while (examiner < pivot) {
                if (extract_data(samples, examiner)[axis] < extract_data(samples, pivot)[axis]) {
                    data_set_swap(samples, examiner, saver);
                    saver++;
                }
                examiner++;
            }
            data_set_swap(samples, pivot, saver);

            if (saver > median_position) {
                pivot = saver - 1;
                examiner = 0;
                saver = 0;
            } else if (saver < median_position) {
                examiner = ++saver;
            } else {
                break;
            }
        }
    }
}



// The following implementation is based on the quickselect implementation of Nicolas Devillard: http://ndevilla.free.fr/median/median/index.html
// Devillard's implementation is based upon the one in "Numerical recipes in C", Second Edition, Cambridge University Press, 1992, Section 8.5, ISBN 0-521-43108-5
// "Numerical Recipes" points out that the performance is directly related to the number of conditionals within the inner loop. Although they are individually cheap, they add up in cost.

void quick_select(data_set *samples, int axis) {
    int low, mid, high, median, examine_low, examine_high, test;
    NUMERICAL_TYPE value, value_low, value_mid, value_high;

    low = 0;
    high = samples->num_data_points - 1;
    median = median_position(samples->num_data_points);

    for (;;) {
        test = high - low;
        if (test <= 0) {
            break;
        } else if (test == 1) {
            if (extract_data(samples, low)[axis] > extract_data(samples, high)[axis]) {
                data_set_swap(samples, low, high);
            }
            break;
        }

        mid = (low + high)/2;
        value_mid = extract_data(samples, mid)[axis];
        value_low = extract_data(samples, low)[axis];
        value_high = extract_data(samples, high)[axis];
        // The following if statements arrange the points at indices low, mid, and high so that low contains their median and mid contains the smallest point (everything evaluated along axis).
        if (value_mid > value_high) {
            data_set_swap(samples, mid, high);
            value = value_mid;
            value_mid = value_high;
            value_high = value;
        }
        if (value_low > value_high) {
            data_set_swap(samples, low, high);
            value = value_low;
            value_low = value_high;
            value_high = value;
        }
        if (value_mid > value_low) {
            data_set_swap(samples, mid, low);
            value = value_mid;
            value_mid = value_low;
            value_low = value;
        }

        // Place the lowest of the three (which is at position mid) right next to the median of the three, which is used as the pivot.
        data_set_swap(samples, mid, low + 1);

        // Start moving in on the middle position from top and bottom.
        examine_low = low + 1;
        examine_high = high;
        value = extract_data(samples, low)[axis];
        for (;;) {
            do examine_low++; while (extract_data(samples, examine_low)[axis] < value);
            do examine_high--; while (extract_data(samples, examine_high)[axis] > value);

            if (examine_high < examine_low) {
                break;
            }

            data_set_swap(samples, examine_low, examine_high);
        }

        // Put the pivot in its proper place.
        data_set_swap(samples, low, examine_high);

        // Shift focus to correct portion of array.
        if (examine_high <= median) {
            low = examine_low;
        }
        if (examine_high >= median) {
            high = examine_high - 1;
        }
    }
}



// For the definition of SEGMENT_SIZE, refer to median.h
// SEGMENT_SIZE can be changed by the user prior to compilation.
void median_of_medians(data_set *samples, int axis){
    if (samples->num_data_points < SEGMENT_SIZE) {
        return;
    }

    int num_segments, segment_median, i;
    data_set *segment, *medians;

    segment_median = median_position(SEGMENT_SIZE);

    segment = malloc(sizeof(data_set));
    segment->embedding_dimension = samples->embedding_dimension;
    segment->num_data_points = SEGMENT_SIZE;

    num_segments = samples->num_data_points/SEGMENT_SIZE;

    // The following loop calculates the median of each segment and places the corresponding median data points at the beginning of the samples data_set.
    // I don't think this causes any slow-down, but it bears probably some more thought.
    i = 0;
    while (i < num_segments) {
        //segment->data = samples->data;
        segment->data_pointers = samples->data_pointers + i*SEGMENT_SIZE;
        quick_select(segment, axis);
        data_set_swap(samples, i, i*SEGMENT_SIZE + segment_median);
        i++;
    }

    // Find the median of medians.
    medians = malloc(sizeof(data_set));
    medians->embedding_dimension = samples->embedding_dimension;
    medians->num_data_points = num_segments;
    medians->data_pointers = samples->data_pointers;
    quick_select(medians, axis);

    // Move the median of medians to the end of samples.
    data_set_swap(samples, samples->num_data_points - 1, (medians->num_data_points + 1)/2 - 1);

    // Do NOT use data_set_destroy here, since the data fields for segment and medians point directly into samples.
    free(segment);
    free(medians);
}



int median_position(int list_size) {
    return (list_size - 1)/2;
}
