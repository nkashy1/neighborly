
#ifndef MEDIAN_H
#define MEDIAN_H


#include "../../neighborly_types.h"

#define SEGMENT_SIZE 5


// quick_select
// A quick select implementation for data_set structures.
//
// Inputs:  (1) data_set *samples, the data set to which we would like to apply the quick select algorithm
//          (2) int axis, the axis along which we would like to find the median within the data_set
//
// Output:  None
//
// Notes:
//  (1) The selection is done in place. The data_set is rearranged so that the element in the median position is the one with the median entry along the given axis.
//  (2) This implementation automatically chooses the last element of the list as its pivot. Therefore, it can easily be used with any arbitrary pivot-selection mechanism -- simply swap the intended pivot with the final element before calling quick_select.
//

void quick_select(data_set *samples, int axis);



// median_of_medians
// Finds the median of medians along a specific dimension in a given data_set and places it at the end of the data_set.
// Reference: M. Blum, R.W. Floyd, V.R. Pratt, R.L. Rivest, R.E. Tarjan. Time bounds for selection. Journal of Computer and System Sciences 7 (4), August 1973. (Pages 448--461)
//
// Inputs:  (1) data_set* samples, a pointer to the data set in which we want to find a pivot for quick select
//          (2) int pivot, the index of the pivot for the selection
//          (3) int axis, the axis along which we would like to apply the algorithm
//
// Output:  None
//
// Notes:
//  (1) We divide the original data set into groups of 5 as is the convention. This is reflected in the SEGMENT_SIZE macro defined at the top of this page, and can be changed by the user.
//  (2) Everything is done in place. If the original order of the data matters to you, pass a pointer to a copy of the original data set.
//

void median_of_medians(data_set *samples, int axis);



// median_position
// Returns the median position in a 0-indexed list of a given size, i.e. the position which would contain the median if the list were ordered.
//
// Inputs:  (1) int list_size, the size of the 0-indexed list
//
// Outputs: int, the position which should contain the median when the list is ordered
//
// Notes:
//

int median_position(int list_size);


#endif // MEDIAN_H
