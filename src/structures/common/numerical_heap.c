
/* Header file */
#include "numerical_heap.h"

/* Standard Libraries */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom Libraries */
#include "../results.h"




void numerical_heap_down(int position, numerical_heap* target_heap) {
    int current_position, left_child, right_child, max_child;
    current_position = position;
    left_child = 2*current_position + 1;
    right_child = left_child + 1;

    NUMERICAL_TYPE temp_storage;

    while (left_child < target_heap->current_size) {
        if (right_child < target_heap->current_size) {
            if (target_heap->order(&target_heap->vertices[right_child], &target_heap->vertices[left_child]) == 1) {
                max_child = right_child;
            } else {
                max_child = left_child;
            }
        } else {
            max_child = left_child;
        }

        if (target_heap->order(&target_heap->vertices[max_child], &target_heap->vertices[current_position]) == 1) {
            temp_storage = target_heap->vertices[current_position];
            target_heap->vertices[current_position] = target_heap->vertices[max_child];
            target_heap->vertices[max_child] = temp_storage;

            current_position = max_child;
            left_child = 2*current_position + 1;
            right_child = left_child + 1;
        } else {
            break;
        }
    }
}



void numerical_heap_up(int position, numerical_heap* target_heap) {
    int current_position, parent;
    current_position = position;
    parent = (current_position - 1)/2;

    NUMERICAL_TYPE temp_storage;

    while (current_position > 0) {
        if (target_heap->order(&target_heap->vertices[current_position], &target_heap->vertices[parent]) == 1) {
            temp_storage = target_heap->vertices[current_position];
            target_heap->vertices[current_position] = target_heap->vertices[parent];
            target_heap->vertices[parent] = temp_storage;

            current_position = parent;
            parent = (current_position - 1)/2;
        } else {
            break;
        }
    }
}



void numerical_heap_insert(NUMERICAL_TYPE value, numerical_heap* target_heap) {
    printf("numerical_heap_insert -- target_heap %p: %d, %d\n", target_heap, target_heap->current_size, target_heap->max_size);
    assert(target_heap->current_size < target_heap->max_size);

    target_heap->vertices[target_heap->current_size] = value;
    numerical_heap_up(target_heap->current_size, target_heap);
    target_heap->current_size++;
}



void numerical_heap_delete(int position, numerical_heap *target_heap) {
    target_heap->current_size -= 1;
    target_heap->vertices[position] = target_heap->vertices[target_heap->current_size];

    numerical_heap_up(position, target_heap);
    numerical_heap_down(position, target_heap);
}



void numerical_heap_heapify(int root, numerical_heap *target_heap) {

    int last_non_leaf = (target_heap->current_size - 2)/2, vertex = root, height_counter = 0, i;

    while (vertex <= last_non_leaf) {
        vertex = 2*vertex + 2;
        height_counter++;
    }

    int max_num_non_leaf_descendants = 1;

    for (i = height_counter; i >= 0; i--) {
        max_num_non_leaf_descendants *= 2;
    }
    max_num_non_leaf_descendants--;

    int *non_leaf_descendants = malloc(sizeof(int)*max_num_non_leaf_descendants);
    non_leaf_descendants[0] = root;
    vertex = 2*root + 1;
    int current = 0, insert = 1, num_non_leaf_descendants = 0;

    while (vertex <= last_non_leaf) {
        non_leaf_descendants[insert] = vertex;
        num_non_leaf_descendants++;
        vertex++;
        insert++;
        if (vertex > last_non_leaf) {
            break;
        }
        non_leaf_descendants[insert] = vertex;
        num_non_leaf_descendants++;
        insert++;

        current++;
        vertex = 2*non_leaf_descendants[current] + 1;
    }


    // Let's finally do what we came here to do.

    for (i = num_non_leaf_descendants; i >= 0; i--) {
        numerical_heap_down(non_leaf_descendants[i], target_heap);
    }

    free(non_leaf_descendants);
}



numerical_heap* numerical_heap_create(NUMERICAL_TYPE* target_array, int target_size, int max_size, int (*order)(NUMERICAL_TYPE*, NUMERICAL_TYPE*)) {
    numerical_heap* fresh_heap = malloc(sizeof(numerical_heap));

    fresh_heap->max_size = max_size;
    fresh_heap->current_size = target_size;
    fresh_heap->order = order;
    fresh_heap->vertices = target_array;
    numerical_heap_heapify(0, fresh_heap);

    return fresh_heap;
}



int gt(NUMERICAL_TYPE *x, NUMERICAL_TYPE *y) {
    if (*x > *y) {
        return 1;
    } else {
        return 0;
    }
}



int ge(NUMERICAL_TYPE *x, NUMERICAL_TYPE *y) {
    if (*x >= *y) {
        return 1;
    } else {
        return 0;
    }
}



/*

** The following is an over-engineered version of numerical_heap_heapify which has less of a memory profile at the cost of longer running time.
** Our heaps are going to be quite small. It is kind of ridiculous that I did this.
** This version has NOT been tested.
** There is also probably a better way to accomplish what I was trying to do here.
** - Neeraj


void numerical_heap_heapify_less_memory(int root, numerical_heap *target_heap) {

    // We can use the fact that a binary heap is a complete binary tree to determine quickly the number of non-leaf descendants of the root.
    // If we follow the path of right-most children from the root, constantly checking whether we are beyond the non-leaf stage or not, then the spot directly after the transition takes place can be used to check for where the non-leaf is.

    int last_non_leaf = (target_heap->current_size - 2)/2, vertex = root, height_counter = 0, i;

    while (vertex <= last_non_leaf) {
        vertex = 2*vertex + 2;
        height_counter++;
    }

    int num_checks = 1;

    for (i = height_counter; i > 0; i--) {
        num_checks *= 2;
    }

    int last_non_leaf_descendant = -1, int num_non_leaf_descendants = 2*num_checks - 1;

    for (i = num_checks; i > 0; i--) {
        vertex--;

        if (vertex <= last_non_leaft) {
            last_non_leaf_descendant = vertex;
            break;
        }

        num_non_leaf_descendants--;
    }

    if (last_non_leaf_descendant < 0) {
        vertex = vertex + num_checks;
        last_non_leaf_descendant = (vertex - 1)/2;
        num_non_leaf_descendants = num_checks - 1;
    }


    // Now we can create the list of indices of non-leaf descendants of root.
    // The root is considered to be a descendant of itself.

    int *non_leaf_descendants = malloc(sizeof(int)*num_non_leaf_descendants);
    non_leaf_descendants[0] = root;

    int current = 0, insert = 1;
    while (insert < num_non_leaf_descendants) {
        non_leaf_descendants[insert] = 2*non_leaf_descendants[current] + 1;
        insert++;
        if (insert <= num_non_leaf_descendants) {
            non_leaf_descendants[insert] = 2*non_leaf_descendants[current] + 2;
            insert++;
        }
        current++;
    }


    // Now we can apply the numerical_heap_down function iteratively starting from the last non-leaf descendant of root to heapify the subtree.

    for (i = num_non_leaf_descendants - 1; i >= 0; i--) {
        numerical_heap_down(non_leaf_descendants[i], target_heap);
    }

}

*/
