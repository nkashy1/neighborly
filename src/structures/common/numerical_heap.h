
#ifndef NUMERICAL_HEAP_H
#define NUMERICAL_HEAP_H


#include "../../neighborly_types.h"


/************************************************************
** numerical_heap.h and numerical_heap.c contain an implementation of a
** binary heap for NUMERICAL_TYPE data.
*************************************************************/


/************************************************************
** numerical_heap
** Definition of the numerical_heap struct and its associated utility functions.
*************************************************************/

struct numerical_heap {
    int max_size;
    int current_size;
    NUMERICAL_TYPE* vertices;
    int (*order)(NUMERICAL_TYPE*, NUMERICAL_TYPE*);
};



// numerical_heap_create:
// Turns an array into a heap.
//
// Inputs:  (1) NUMERICAL_TYPE* target_array, the array to be heapified
//          (2) int target_size, the length of target_array
//          (3) int max_size, the largest that the heap will be allowed to get
//
// Output: A pointer to a numerical_heap which contains a heapified version of target_array
//
// Notes:
//  (1) In this default implementation, the heapification is carried out in place, so that the vertices pointer in the output is equal to the memory location target_array.
//

numerical_heap* numerical_heap_create(NUMERICAL_TYPE* target_array, int target_size, int max_size, int (*order)(NUMERICAL_TYPE*, NUMERICAL_TYPE*));



// numerical_heap_insert:
// Inserts a number into a given heap.
//
// Inputs:  (1) NUMERICAL_TYPE value, the number to be inserted
//          (2) numerical_heap* target_heap, the heap in which the insertion should take place
//
// Output:  None
//
// Notes:
//

void numerical_heap_insert(NUMERICAL_TYPE value, numerical_heap* target_heap);



// numerical_heap_delete:
// Removes a vertex from a given heap.
//
// Inputs:  (1) int position, the position of the vertex to be removed
//          (2) numerical_heap* target_heap, the heap from which the vertex will be deleted
//
// Output:  None
//
// Notes:
//

void numerical_heap_delete(int position, numerical_heap* target_heap);



// numerical_heap_heapify:
// Heapifies a subheap of a given heap.
//
// Inputs:  (1) int root, the position in the heap of the root of the subheap
//          (2) numerical_heap* target_heap, the heap in which the heapification is to take place
//
// Output:  None
//
// Notes:
//  (1) Not the most memory-efficient implementation of this function, but that shouldn't really be a concern in neighborly applications.
//  (2) If you do want something memory-efficient, you might be interested in the commented out code block at the bottom of static_heap.c containing the numerical_heap_heapify_less_memory function. This code has NOT been tested.
//

void numerical_heap_heapify(int root, numerical_heap* target_heap);



// numerical_heap_up:
// Moves the value at a given vertex of a heap up as far as possible towards the root of the heap.
//
// Inputs:  (1) int position, the position in the heap of the target vertex
//          (2) numerical_heap* target_heap, the heap in which the upshift is to take place
//
// Output:  None
//
// Notes:
//

void numerical_heap_up(int position, numerical_heap* target_heap);



// numerical_heap_down:
// Moves the value at a given vertex of a heap as far down as possible away from the root of the heap.
//
// Inputs:  (1) int position, the position in the heap of the target vertex
//          (2) numerical_heap* target_heap, the heap in which the downshift is to take place
//
// Output:  None
//
// Notes:
//

void numerical_heap_down(int position, numerical_heap* target_heap);



/*******************************************************
** Order functions
********************************************************/

int gt(NUMERICAL_TYPE* x, NUMERICAL_TYPE* y);

int ge(NUMERICAL_TYPE* x, NUMERICAL_TYPE* y);



#endif // NUMERICAL_HEAP_H
