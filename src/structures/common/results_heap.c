
/* Header file */
#include "results_heap.h"

/* Standard Libraries */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom Libraries */
#include "../results.h"




void results_heap_down(int position, results_heap* target_heap) {
    int current_position, left_child, right_child, max_child;
    current_position = position;
    left_child = 2*current_position + 1;
    right_child = left_child + 1;

    results_datum temp_storage;

    while (left_child < target_heap->current_size) {
        if (right_child < target_heap->current_size) {
            if (target_heap->order(&target_heap->vertices[right_child], &target_heap->vertices[left_child]) == 1) {
                max_child = right_child;
            } else {
                max_child = left_child;
            }
        } else {
            max_child = left_child;
        }

        if (target_heap->order(&target_heap->vertices[max_child], &target_heap->vertices[current_position]) == 1) {
            temp_storage = target_heap->vertices[current_position];
            target_heap->vertices[current_position] = target_heap->vertices[max_child];
            target_heap->vertices[max_child] = temp_storage;

            current_position = max_child;
            left_child = 2*current_position + 1;
            right_child = left_child + 1;
        } else {
            break;
        }
    }
}



void results_heap_up(int position, results_heap* target_heap) {
    int current_position, parent;
    current_position = position;
    parent = (current_position - 1)/2;

    results_datum temp_storage;

    while (current_position > 0) {
        if (target_heap->order(&target_heap->vertices[current_position], &target_heap->vertices[parent]) == 1) {
            temp_storage = target_heap->vertices[current_position];
            target_heap->vertices[current_position] = target_heap->vertices[parent];
            target_heap->vertices[parent] = temp_storage;

            current_position = parent;
            parent = (current_position - 1)/2;
        } else {
            break;
        }
    }
}



void results_heap_insert(results_datum value, results_heap* target_heap) {
    // DEBUGGING
    //printf("results_heap_insert -- target_heap %p: %d, %d\n", target_heap, target_heap->current_size, target_heap->max_size);

    assert(target_heap->current_size < target_heap->max_size);

    target_heap->vertices[target_heap->current_size] = value;
    results_heap_up(target_heap->current_size, target_heap);
    target_heap->current_size++;
}



void results_heap_delete(int position, results_heap *target_heap) {
    target_heap->current_size -= 1;
    target_heap->vertices[position] = target_heap->vertices[target_heap->current_size];

    results_heap_up(position, target_heap);
    results_heap_down(position, target_heap);
}



void results_heap_heapify(int root, results_heap *target_heap) {

    int last_non_leaf = (target_heap->current_size - 2)/2, vertex = root, height_counter = 0, i;

    while (vertex <= last_non_leaf) {
        vertex = 2*vertex + 2;
        height_counter++;
    }

    int max_num_non_leaf_descendants = 1;

    for (i = height_counter; i >= 0; i--) {
        max_num_non_leaf_descendants *= 2;
    }
    max_num_non_leaf_descendants--;

    int *non_leaf_descendants = malloc(sizeof(int)*max_num_non_leaf_descendants);
    non_leaf_descendants[0] = root;
    vertex = 2*root + 1;
    int current = 0, insert = 1, num_non_leaf_descendants = 0;

    while (vertex <= last_non_leaf) {
        non_leaf_descendants[insert] = vertex;
        num_non_leaf_descendants++;
        vertex++;
        insert++;
        if (vertex > last_non_leaf) {
            break;
        }
        non_leaf_descendants[insert] = vertex;
        num_non_leaf_descendants++;
        insert++;

        current++;
        vertex = 2*non_leaf_descendants[current] + 1;
    }


    // Let's finally do what we came here to do.

    for (i = num_non_leaf_descendants; i >= 0; i--) {
        results_heap_down(non_leaf_descendants[i], target_heap);
    }

    free(non_leaf_descendants);
}



results_heap* results_heap_create(results_datum* target_array, int target_size, int max_size, int (*order)(results_datum*, results_datum*)) {
    results_heap* fresh_heap = malloc(sizeof(results_heap));

    fresh_heap->max_size = max_size;
    fresh_heap->current_size = target_size;
    fresh_heap->order = order;
    fresh_heap->vertices = target_array;
    results_heap_heapify(0, fresh_heap);

    return fresh_heap;
}



results_heap* results_heap_copy(results_heap *target_heap, int dimension){
    results_heap *heap_copy;
    results_datum *vertices_copy, *temp_vertex;
    int i;

    heap_copy = malloc(sizeof(results_heap));
    heap_copy->current_size = target_heap->current_size;
    heap_copy->max_size = target_heap->max_size;
    heap_copy->order = target_heap->order;

    vertices_copy = malloc(sizeof(results_datum)*heap_copy->current_size);
    for (i = 0; i < heap_copy->current_size; i++) {
        temp_vertex = results_datum_copy(target_heap->vertices + i, dimension);
        vertices_copy[i] = *temp_vertex;
        results_datum_destroy(temp_vertex);
    }

    heap_copy->vertices = vertices_copy;

    return heap_copy;
}
