
#ifndef RESULTS_HEAP_H
#define RESULTS_HEAP_H


#include "../../neighborly_types.h"


/************************************************************
** results_heap.h and results_heap.c contain an implementation of a
** binary heap.
*************************************************************/


/************************************************************
** results_heap
** Definition of the results_heap struct and its associated utility functions.
*************************************************************/

struct results_heap {
    int max_size;
    int current_size;
    results_datum* vertices;
    int (*order)(results_datum*, results_datum*);
};



// results_heap_create:
// Turns an array into a heap.
//
// Inputs:  (1) results_datum* target_array, the array to be heapified
//          (2) int target_size, the length of target_array
//          (3) int max_size, the largest that the heap will be allowed to get
//
// Output: A pointer to a results_heap which contains a heapified version of target_array
//
// Notes:
//  (1) In this default implementation, the heapification is carried out in place, so that the vertices pointer in the output is equal to the memory location target_array.
//

results_heap* results_heap_create(results_datum* target_array, int target_size, int max_size, int (*order)(results_datum*, results_datum*));



// results_heap_insert:
// Inserts a number into a given heap.
//
// Inputs:  (1) results_datum value, the number to be inserted
//          (2) results_heap* target_heap, the heap in which the insertion should take place
//
// Output:  None
//
// Notes:
//

void results_heap_insert(results_datum value, results_heap* target_heap);



// results_heap_delete:
// Removes a vertex from a given heap.
//
// Inputs:  (1) int position, the position of the vertex to be removed
//          (2) results_heap* target_heap, the heap from which the vertex will be deleted
//
// Output:  None
//
// Notes:
//  (1) Insertion is performed by value, not by reference.
//

void results_heap_delete(int position, results_heap* target_heap);



// results_heap_heapify:
// Heapifies a subheap of a given heap.
//
// Inputs:  (1) int root, the position in the heap of the root of the subheap
//          (2) results_heap* target_heap, the heap in which the heapification is to take place
//
// Output:  None
//
// Notes:
//  (1) Not the most memory-efficient implementation of this function, but that shouldn't really be a concern in neighborly applications.
//  (2) If you do want something memory-efficient, you might be interested in the commented out code block at the bottom of static_heap.c containing the results_heap_heapify_less_memory function. This code has NOT been tested.
//

void results_heap_heapify(int root, results_heap* target_heap);



// results_heap_up:
// Moves the value at a given vertex of a heap up as far as possible towards the root of the heap.
//
// Inputs:  (1) int position, the position in the heap of the target vertex
//          (2) results_heap* target_heap, the heap in which the upshift is to take place
//
// Output:  None
//
// Notes:
//

void results_heap_up(int position, results_heap* target_heap);



// results_heap_down:
// Moves the value at a given vertex of a heap as far down as possible away from the root of the heap.
//
// Inputs:  (1) int position, the position in the heap of the target vertex
//          (2) results_heap* target_heap, the heap in which the downshift is to take place
//
// Output:  None
//
// Notes:
//

void results_heap_down(int position, results_heap* target_heap);



// results_heap_copy
// Makes a copy of a given results_heap.
//
// Inputs:  (1) results_heap *target_heap, the results_heap we would like to copy
//
// Outputs: results_heap *, a pointer to the copy of target_heap
//
// Notes:
//

results_heap* results_heap_copy(results_heap *target_heap, int dimension);


#endif // RESULTS_HEAP_H
