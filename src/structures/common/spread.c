
/* Header file */
#include "spread.h"

/* Standard libraries */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */
#include "../data_set.h"
#include "median.h"


NUMERICAL_TYPE madmed(data_set *samples, int axis) {
    if (samples->num_data_points < 1) {
        return -1;
    }

    NUMERICAL_TYPE median, deviation;
    int i;

    median_of_medians(samples, axis);
    quick_select(samples, axis);
    median = extract_data(samples, median_position(samples->num_data_points))[axis];

    deviation = 0;
    for (i = samples->num_data_points - 1; i >= 0; i--) {
        deviation += ABS(extract_data(samples, i)[axis] - median);
    }

    return deviation/((NUMERICAL_TYPE) samples->num_data_points);
}



int max_axis(data_set *samples, NUMERICAL_TYPE (*spread)(data_set*, int)) {
    NUMERICAL_TYPE max_spread, current_spread;
    int maximal_axis, i;

    max_spread = 0;
    for (i = samples->embedding_dimension - 1; i >= 0; i--) {
        current_spread = spread(samples, i);
        if (current_spread > max_spread) {
            max_spread = current_spread;
            maximal_axis = i;
        }
    }

    return maximal_axis;
}
