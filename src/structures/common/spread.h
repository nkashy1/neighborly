
#ifndef SPREAD_H
#define SPREAD_H

#include "../../neighborly_types.h"

// max_axis
// Returns the label of an axis along which the given data set has maximal spread, where the notion of spread is passed as an argument.
//
// Inputs:  (1) data_set *samples, a pointer to the data_set in which we would like to find the axis of maximal spread
//          (2) NUMERICAL_TYPE (*spread)(data_set*, int), a function which assigns a spread value to a given data_set along a given axis
//
// Ouput:   int, the label of the axis along which the given data_set has maximal spread
//
// Notes:
//  (1) If there are multiple axes exhibiting the maximal spread, max_axis returns the largest labelled such axis.
//  (2) max_axis makes no guarantees about preserving the order of your data_set. This behaviour depends on the spread function you use. See below.
//

int max_axis(data_set *samples, NUMERICAL_TYPE (*spread)(data_set*, int));



/**********************************************************
** Spread functions
***********************************************************/

// madmed
// Calculates the mean absolute deviation of a given data_set about a given axis from its median entry along that axis.
//
// Inputs:  (1) data_set *samples, a pointer to the target data_set
//          (2) int axis, the axis along which we would like to calculate the mean absolute deviation of samples
//
// Outputs: NUMERICAL_TYPE, the desired mean absolute deviation
//
// Notes:
//  (1) This function makes use of the in-place median-finding functions defined in median.h. If you want to preserve the order of your data points, make a copy of your data_set before passing it to this function or any function which uses it (like max_axis).
//

NUMERICAL_TYPE madmed(data_set *samples, int axis);


#endif // SPREAD_H
