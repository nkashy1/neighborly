
/* Header file */
#include "data_set.h"

/* Standard Libraries */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom Libraries */
#include "common/csv.h"




data_set* data_set_create(int embedding_dimension, int num_data_points) {
    data_set *fresh_data_set = malloc(sizeof(data_set));
    fresh_data_set->embedding_dimension = embedding_dimension;
    fresh_data_set->num_data_points = num_data_points;
    fresh_data_set->data = malloc(sizeof(NUMERICAL_TYPE)*embedding_dimension*num_data_points);
    fresh_data_set->data_pointers = malloc(sizeof(NUMERICAL_TYPE*)*num_data_points);

    int i;
    for (i = 0; i < num_data_points; i++) {
        fresh_data_set->data_pointers[i] = fresh_data_set->data + i*embedding_dimension;
    }

    return fresh_data_set;
}



data_set* data_set_create_bare(int embedding_dimension, int num_data_points) {
    data_set *fresh_data_set = malloc(sizeof(data_set));
    fresh_data_set->embedding_dimension = embedding_dimension;
    fresh_data_set->num_data_points = num_data_points;

    return fresh_data_set;
}



void data_set_destroy(data_set *obsolete_data_set) {
    free(obsolete_data_set->data);
    free(obsolete_data_set->data_pointers);
    free(obsolete_data_set);
}



NUMERICAL_TYPE* extract_data(data_set const *D, int k){
    return D->data_pointers[k];
}



void data_set_swap(data_set *D, int i, int j) {
    NUMERICAL_TYPE *temp_pointer;
    temp_pointer = D->data_pointers[i];
    D->data_pointers[i] = D->data_pointers[j];
    D->data_pointers[j] = temp_pointer;
}



void data_set_print(data_set *D) {
    int i, j;
    NUMERICAL_TYPE *data_point;

    for (i = 0; i < D->num_data_points; i++) {
        printf("(");
        data_point = extract_data(D,i);
        for (j = 0; j < D->embedding_dimension - 1; j++) {
            printf("%f, ", data_point[j]);
        }
        printf("%f)\n", data_point[j]);
    }
}



void data_set_reset_pointers(data_set *D) {
    int i;
    for (i = 0; i < D->num_data_points; i++) {
        D->data_pointers[i] = D->data + i*D->embedding_dimension;
    }
}



data_set* data_set_loadcsv(csv_info info) {
    data_set *target;
    target = data_set_create(info.num_fields, info.num_rows);

    FILE *ifp;
    ifp = fopen(info.filename, "r");

    int i, j;

    NUMERICAL_TYPE temp;

    for (i = 0; i < info.num_rows; i++) {
        for (j = 0; j < info.num_fields - 1; j++) {
            if (fscanf(ifp, "%f,", &temp) == 1) {
                target->data[i*target->embedding_dimension + j] = temp;
            }
        }
        if (fscanf(ifp, "%f\n", &temp) == 1) {
            target->data[(i+1)*target->embedding_dimension - 1] = temp;
        }
    }

    fclose(ifp);

    return target;
}
