
#ifndef DATA_SET_H
#define DATA_SET_H

#include "../neighborly_types.h"


/********************************************************
** data_set
** Definition of structure and utility functions.
*********************************************************/

struct data_set {
    int embedding_dimension;
    int num_data_points;
    NUMERICAL_TYPE* data;
    NUMERICAL_TYPE** data_pointers;
};



// 'data_set_create':
// Creates an empty data set structure of a given embedding dimension and a given size.
//
// Inputs:  (1) An integer 'embedding_dimension'.
//          (2) An integer 'num_data_points'.
//
// Output:  A pointer to an empty data set with memory dynamically allocated to both the data set itself as well to its data array.
//
// Notes:

data_set* data_set_create(int embedding_dimension, int num_data_points);



// 'data_set_create_bare':
// Creates an empty data set structure of a given embedding dimension and a given size. In this version, no memory is allocated to the data and data_pointers fields.
//
// Inputs:  (1) An integer 'embedding_dimension'.
//          (2) An integer 'num_data_points'.
//
// Output:  A pointer to an empty data set with memory dynamically allocated to both the data set itself as well to its data array.
//
// Notes:
//  (1) data_sets created in this manner should simply be freed when they are no longer needed.
//

data_set* data_set_create_bare(int embedding_dimension, int num_data_points);



// 'data_set_destroy':
// Frees up dynamically allocated data_sets in memory.
//
// Inputs:  (1) A dynamically allocated data_set* 'obsolete_data_set'.
//
// Ouput:
//
// Notes:

void data_set_destroy(data_set *obsolete_data_set);



// 'extract_data':
// Takes an array representing a data set and extracts a point in a specified position.
//
// Inputs:  (1) An integer 'n', representing the embedding dimension of the data set.
//          (2) The data set 'D', encoded as specified in the 'naive_nearest_neighbor' description.
//          (3) The position 'k' of the point one wants to extract. 'k' starts at 0.
//
// Outputs: (1) A pointer to the 'k'^{th} data point in 'D'.
//
// Notes:
//

NUMERICAL_TYPE* extract_data(data_set const *D, int k);



// data_set_swap
// Swaps the positions of two points in a data_set.
//
// Inputs:  (1) data_set *D, a pointer to the data_set in which the points are to be swapped
//          (2) int i, the position within the data_set of the first point
//          (3) int j, the position within the data_set of the second point
//
// Output:  None
//
// Notes:
//  (1) The swapping is done in place.
//

void data_set_swap(data_set *D, int i, int j);



// data_set_print
// Prints the vectors in the given data_set.
//
// Inputs:  (1) data_set *D, a pointer to the data set to be displayed
//
// Outputs: None
//
// Notes:
//

void data_set_print(data_set *D);



// data_set_reset_pointers
// Resets the data_pointers field of the given data_set to point at the data in sequence.
//
// Inputs:  (1) data_set *D, the data_set in which we would like to reset pointers
//
// Outputs: None
//
// Notes:
//

void data_set_reset_pointers(data_set *D);



// data_set_loadcsv
// Creates a data_set from data contained in a csv file.
//
// Inputs:
//  (1) csv_info info, the csv_info struct for the file to be loaded
//
// Output: data_set*, a pointer to the data_set containing the data stored in info.filename
//
// Notes:
//

data_set* data_set_loadcsv(csv_info info);

#endif // DATA_SET_H
