
/* Header file */
#include "kd_tree.h"

/* Standard Libraries */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom Libraries */
#include "data_set.h"
#include "common/median.h"
#include "common/spread.h"



kd_vertex* kd_vertex_create(int dimension, int disc_key) {
    kd_vertex *fresh_vertex = malloc(sizeof(kd_vertex));
    fresh_vertex->disc_key = disc_key;
    fresh_vertex->record = malloc(sizeof(NUMERICAL_TYPE)*dimension);
    fresh_vertex->extremes[0] = 0;
    fresh_vertex->extremes[1] = 0;
    fresh_vertex->children[0] = NULL;
    fresh_vertex->children[1] = NULL;

    return fresh_vertex;
}



void kd_vertex_destroy(kd_vertex *obsolete_vertex){
    free(obsolete_vertex->record);
    free(obsolete_vertex);
}



kd_tree* kd_tree_create(int dimension) {
    kd_tree *fresh_tree = malloc(sizeof(kd_tree));
    fresh_tree->dimension = dimension;
    fresh_tree->num_vertices = 0;
    fresh_tree->depth = -1;
    fresh_tree->root = NULL;

    return fresh_tree;
}



void kd_tree_insert(kd_tree *target_tree, NUMERICAL_TYPE *record) {
    int i;
    kd_vertex *fresh_vertex = kd_vertex_create(target_tree->dimension, 0);
    for (i = 0; i < target_tree->dimension; i++) {
        fresh_vertex->record[i] = record[i];
    }

    if (target_tree->root == NULL) {
        target_tree->root = fresh_vertex;
        target_tree->depth = 0;
    } else {
        kd_vertex *current_vertex, *previous_vertex;
        int branch;    // 0 for low, 1 for high
        int level;

        level = 0;
        current_vertex = target_tree->root;

        while (current_vertex != NULL) {
            /*
            branch = 0;
            if (record[current_vertex->disc_key] > current_vertex->record[current_vertex->disc_key]) {
                branch = 1;
            }
            */
            branch = (record[current_vertex->disc_key] > current_vertex->record[current_vertex->disc_key]);


            if ((current_vertex->extremes[branch] < record[current_vertex->disc_key]) == branch) {
                current_vertex->extremes[branch] = record[current_vertex->disc_key];
            }


            previous_vertex = current_vertex;
            current_vertex = current_vertex->children[branch];
            level++;
        }

        if (target_tree->depth < level) {
            target_tree->depth = level;
        }

        while (level < 0) {
            level += target_tree->dimension;
        }
        while (level >= target_tree->dimension) {
            level -= target_tree->dimension;
        }

        fresh_vertex->disc_key = level;
        fresh_vertex->extremes[0] = record[fresh_vertex->disc_key];
        fresh_vertex->extremes[1] = record[fresh_vertex->disc_key];
        previous_vertex->children[branch] = fresh_vertex;
    }

    target_tree->num_vertices++;
}



void kd_tree_insert_vertex(kd_tree *target_tree, kd_vertex* target_vertex) {
    if (target_tree->root == NULL) {
        target_tree->root = target_vertex;
        target_tree->depth = 0;
    } else {
        kd_vertex *current_vertex, *previous_vertex;
        int branch;    // 0 for low, 1 for high
        int level;

        level = 0;
        current_vertex = target_tree->root;

        while (current_vertex != NULL) {
            branch = (target_vertex->record[current_vertex->disc_key] > current_vertex->record[current_vertex->disc_key]);

            if ((current_vertex->extremes[branch] < target_vertex->record[current_vertex->disc_key]) == branch) {
                current_vertex->extremes[branch] = target_vertex->record[current_vertex->disc_key];
            }

            previous_vertex = current_vertex;
            current_vertex = current_vertex->children[branch];
            level++;
        }

        if (target_tree->depth < level) {
            target_tree->depth = level;
        }

        previous_vertex->children[branch] = target_vertex;
    }

    target_tree->num_vertices++;
}



kd_vertex** kd_tree_enumerate(kd_tree *target_tree){
    kd_vertex **vertices = malloc(sizeof(kd_vertex*)*target_tree->num_vertices);
    int current_position, insert_position;

    vertices[0] = target_tree->root;
    current_position = 0;
    insert_position = 1;

    while (insert_position < target_tree->num_vertices) {
        if (vertices[current_position]->children[0] != NULL) {
            vertices[insert_position] = vertices[current_position]->children[0];
            insert_position++;
        }

        if (vertices[current_position]->children[1] != NULL) {
            vertices[insert_position] = vertices[current_position]->children[1];
            insert_position++;
        }

        current_position++;
    }

    return vertices;
}



void kd_tree_destroy(kd_tree *obsolete_tree) {
    kd_vertex **vertices;
    int i;

    vertices = kd_tree_enumerate(obsolete_tree);
    for (i = 0; i < obsolete_tree->num_vertices; i++) {
        kd_vertex_destroy(vertices[i]);
    }
    free(obsolete_tree);
    free(vertices);
}



kd_tree* kd_tree_construct(data_set *source) {
    int i;
    kd_tree *fresh_tree;
    NUMERICAL_TYPE* record;

    fresh_tree = kd_tree_create(source->embedding_dimension);

    for (i = 0; i < source->num_data_points; i++) {
        record = extract_data(source, i);
        kd_tree_insert(fresh_tree, record);
    }

    return fresh_tree;
}



kd_tree* kd_tree_construct_fancy(data_set *source) {
    data_set* chunks[source->num_data_points];
    int examiner, saver, delimiter, current_axis, left, right;
    kd_tree *fresh_tree = kd_tree_create(source->embedding_dimension);

    chunks[0] = data_set_create_bare(source->embedding_dimension, source->num_data_points);
    chunks[0]->data = source->data;
    chunks[0]->data_pointers = source->data_pointers;
    examiner = -1;
    saver = 1;
    delimiter = 1;
    current_axis = 0;

    for(;;) {
        while (examiner < delimiter - 1) {
            examiner++;
            if (chunks[examiner]->num_data_points > 2) {
                if (chunks[examiner]->num_data_points > 4) {
                    median_of_medians(chunks[examiner], current_axis);
                }

                quick_select(chunks[examiner], current_axis);


                left = (chunks[examiner]->num_data_points + 1)/2 - 1;
                right = chunks[examiner]->num_data_points - left - 1;

                kd_tree_insert(fresh_tree, extract_data(chunks[examiner], left));

                // DEBUGGING
                //printf("\nInsert after select\n");

                if (left > 0) {
                    chunks[saver] = data_set_create_bare(source->embedding_dimension, left);
                    chunks[saver]->data_pointers = chunks[examiner]->data_pointers;
                    saver++;
                }
                if (right > 0) {
                    chunks[saver] = data_set_create_bare(source->embedding_dimension, right);
                    chunks[saver]->data_pointers = chunks[examiner]->data_pointers + left + 1;
                    saver++;
                }
            } else {
                // Just using left as a counter. Nothing special about it.
                for (left = 0; left < chunks[examiner]->num_data_points; left++) {
                    kd_tree_insert(fresh_tree, extract_data(chunks[examiner], left));

                    // DEBUGGING
                    //printf("\nInsert without select.\n");
                }
            }


            // DEBUGGING
            //printf("Examiner, Delimiter, Saver: %d, %d, %d\n", examiner, delimiter, saver);
        }

        if (delimiter == saver || saver > source->num_data_points) {
            break;
        } else {
            delimiter = saver;
            current_axis++;
            if (current_axis == source->embedding_dimension) {
                current_axis = 0;
            }
        }
    }

    for (left = 0; left < saver; left++) {
        free(chunks[left]);
    }

    return fresh_tree;
}



kd_tree* kd_tree_construct_adaptive(data_set *source) {
    data_set* chunks[source->num_data_points];
    int examiner, saver, delimiter, current_axis, left, right;
    kd_tree *fresh_tree = kd_tree_create(source->embedding_dimension);
    kd_vertex *inserter;

    chunks[0] = data_set_create_bare(source->embedding_dimension, source->num_data_points);
    chunks[0]->data = source->data;
    chunks[0]->data_pointers = source->data_pointers;
    examiner = -1;
    saver = 1;
    delimiter = 1;

    for(;;) {
        while (examiner < delimiter - 1) {
            examiner++;
            current_axis = max_axis(chunks[examiner], madmed);
            if (chunks[examiner]->num_data_points > 2) {
                median_of_medians(chunks[examiner], current_axis);
                quick_select(chunks[examiner], current_axis);

                left = (chunks[examiner]->num_data_points + 1)/2 - 1;
                right = chunks[examiner]->num_data_points - left - 1;

                inserter = kd_vertex_create(source->embedding_dimension, current_axis);
                // Just going to use current_axis as a counter, nothing special going on here.
                for (current_axis = 0; current_axis < source->embedding_dimension; current_axis++) {
                    inserter->record[current_axis] = extract_data(chunks[examiner], left)[current_axis];
                }

                //kd_tree_insert(fresh_tree, chunks[examiner]->data + left*source->embedding_dimension);
                kd_tree_insert_vertex(fresh_tree, inserter);

                if (left > 0) {
                    chunks[saver] = data_set_create_bare(source->embedding_dimension, left);
                    chunks[saver]->data_pointers = chunks[examiner]->data_pointers;
                    saver++;
                }
                if (right > 0) {
                    chunks[saver] = data_set_create(source->embedding_dimension, right);
                    chunks[saver]->data_pointers = chunks[examiner]->data_pointers + left + 1;
                    saver++;
                }
            } else {
                // Just using left as a counter. Nothing special about it.
                for (left = 0; left < chunks[examiner]->num_data_points; left++) {
                    kd_tree_insert(fresh_tree, extract_data(chunks[examiner], left));
                }
            }

        }

        if (delimiter == saver || saver > source->num_data_points) {
            break;
        } else {
            delimiter = saver;
        }
    }

    for (left = 0; left < saver; left++) {
        free(chunks[left]);
    }

    return fresh_tree;
}
