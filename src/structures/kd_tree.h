
#ifndef KD_TREE_H
#define KD_TREE_H

// An implementation of a Bentley's k-d tree data structure:
// Jon Louis Bentley, Multidimensional Binary Search Trees Used for Associative Searching, Communications of the ACM, Volume 18 (9), September 1975.

#include "../neighborly_types.h"



/*******************************************************
** The kd_vertex struct and its utility functions
********************************************************/

struct kd_vertex {
    int disc_key;
    NUMERICAL_TYPE* record;
    NUMERICAL_TYPE extremes[2];
    struct kd_vertex* children[2];  // 0th child is low, 1st child is high
};



// kd_vertex_create
// Creates a fresh kd_vertex
//
// Inputs:  (1) int dimension, the number of entries in the record which the vertex will hold
//          (2) int disc_key, the index along which to determine highness and lowness at the vertex
//
// Output:  kd_vertex *, a pointer to the fresh kd_vertex.
//
// Notes:
//  (1) The children of the fresh kd_vertex are set to be NULL.
//  (2) The extremes in the fresh kd_vertex are initialized as 0. This probably isn't very useful in development, but could be useful in testing.
//

kd_vertex* kd_vertex_create(int dimension, int disc_key);



// kd_vertex_destroy
// Frees up memory dynamically allocated by kd_vertex_create
//
// Inputs:  (1) kd_vertex *obsolete_vertex, a pointer to a kd_vertex which has outlived its utility
//
// Output: None
//
// Notes:
//

void kd_vertex_destroy(kd_vertex *obsolete_vertex);



/*******************************************************
** The kd_tree struct and its utility functions
********************************************************/

struct kd_tree {
    int dimension;
    int num_vertices;
    int depth;
    kd_vertex *root;
};



// kd_tree_create
// Creates a fresh kd_tree with no vertices designed to hold records of a given dimension.
//
// Inputs:  (1) int dimension, the number of entries in the records that the k-d tree is meant to hold
//
// Output:  kd_tree *, a pointer to the fresh kd_tree

kd_tree* kd_tree_create(int dimension);



// kd_tree_construct
// Imposes a k-d tree structure on a raw data_set
//
// Inputs:  (1) data_set *source, the raw data set that we would like to turn into a k-d tree
//
// Output:  kd_tree *, a pointer to the k-d tree version of the source data set.
//
// Notes:
//

kd_tree* kd_tree_construct(data_set *source);



// kd_tree_destroy
// Frees up memory assigned to dynamically allocated kd_trees constructed with the kd_tree_create and kd_tree_construct functions.
//
// Inputs:  (1) kd_tree *obsolete_tree, a k-d tree which has outlived its usefulness
//
// Output:  None
//
// Notes:
//

void kd_tree_destroy(kd_tree *obsolete_tree);



// kd_tree_insert
// Inserts a record into a k-d tree.
//
// Inputs:  (1) kd_tree *target_tree, the k-d tree into which the record is to be inserted
//          (2) NUMERICAL_TYPE *target_vector, the record which is to be inserted into target_tree
//
// Output:  None
//
// Notes:
//  (1) kd_tree_insert creates creates a new copy of target_vector in memory.
//  (2) TODO: Write a version of insert which simply uses a reference to the original record rather than doing a copy.

void kd_tree_insert(kd_tree *target_tree, NUMERICAL_TYPE *target_vector);



// kd_tree_insert_vertex
// Inserts a kd_vertex into a k-d tree.
//
// Inputs:  (1) kd_tree *target_tree, the k-d tree into which the record is to be inserted
//          (2) kd_vertex *target_vertex, the vertex to be inserted into target_tree
//
// Output:  None
//
// Notes:
//  (1) kd_tree_insert_vertex does NOT copy the contents of *target_vertex before insertion.
//  (2) It is ASSUMED that the disc_key field of *target_vertex is set prior to insertion.

void kd_tree_insert_vertex(kd_tree *target_tree, kd_vertex *target_vertex);



// kd_tree_enumerate
// Enumerates all the vertices in a k-d tree by doing a breadth-first traversal.
//
// Inputs:  (1) kd_tree *target_tree, the k-d tree all of whose vertices we would like to list
//
// Output:  kd_vertex**, an array of pointers to the vertices of target_tree
//
// Notes:
//  (1) The number of elements in the output array is target_tree->num_vertices. If you will be using the output independently of target_tree, make sure to store this number separately.
//

kd_vertex** kd_tree_enumerate(kd_tree *target_tree);



// kd_tree_construct_fancy
// Fancy construction of kd_tree. Inserts the admissible point with median coordinate at each insertion.
//
// Inputs:  (1) data_set *source, a pointer to the raw data
//
// Output:  kd_tree*, a pointer to the constructed kd_tree
//
// Notes:
//  (1) Changes the order of the data points in source. If you need to preserve the order, make a copy of your data_set before calling this function.
//

kd_tree* kd_tree_construct_fancy(data_set *source);



// kd_tree_construct_adaptive
// An implementation of the adaptive construction of kd_tree laid out in the following paper:
// Jerome H. Friedman, Jon Louis Bentley, Raphael Ari Finkel, "An Algorithm for Finding Best Matches in Logarithmic Expected Time", ACM Transactions on Mathematical Software, Volume 3 (3), September 1977, Pages 209-226.
//
// Inputs:  (1) data_set *source, a pointer to the raw data
//
// Output:  kd_tree*, a pointer to the constructed kd_tree
//
// Notes:
//  (1) Changes the order of the data points in source. If you need to preserve the order, make a copy of your data_set before calling this function.
//

kd_tree* kd_tree_construct_adaptive(data_set *source);


#endif // KD_TREE_H
