
/* Header file */
#include "results.h"

/* Standard libraries */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/* Custom libraries */




results_datum* results_datum_create() {
    results_datum *fresh_datum;
    fresh_datum = malloc(sizeof(results_datum));
    fresh_datum->data_point = NULL;
    fresh_datum->distance = -1;

    return fresh_datum;
}



void results_datum_destroy(results_datum *obsolete_datum) {
    if (obsolete_datum != NULL) {
        if (obsolete_datum->data_point != NULL) {
            free(obsolete_datum->data_point);
        }
        free(obsolete_datum);
    }
}



results_datum* results_datum_copy(results_datum *target_datum, int dimension) {
    results_datum *datum_copy;
    int i;

    datum_copy = malloc(sizeof(results_datum));
    datum_copy->distance = target_datum->distance;
    datum_copy->data_point = malloc(sizeof(NUMERICAL_TYPE)*dimension);
    for (i = 0; i < dimension; i++) {
        datum_copy->data_point[i] = target_datum->data_point[i];
    }

    return datum_copy;
}



results* results_create(int num_neighbors) {
    results *fresh_results;
    fresh_results = malloc(sizeof(results));
    fresh_results->num_neighbors = num_neighbors;
    fresh_results->data = malloc(sizeof(results_datum)*num_neighbors);
    assert(fresh_results->data != NULL);

    return fresh_results;
}



void results_destroy(results *obsolete_results) {
    if (obsolete_results != NULL) {
        if (obsolete_results->data != NULL) {
            free(obsolete_results->data);
        }

        free(obsolete_results);
    }
}



void results_print(int dimension, NUMERICAL_TYPE *target_point, results *target_results) {
    int i, j;
    printf("The %d nearest neighbors of (", target_results->num_neighbors);
    for (j = 0; j < dimension - 1; j++) {
        printf("%f, ", target_point[j]);
    }
    printf("%f):\n", target_point[j]);

    for (i = 0; i < target_results->num_neighbors; i++) {
        printf("   %d. (", target_results->num_neighbors - i);
        for (j = 0; j < dimension - 1; j++) {
            printf("%f, ", target_results->data[i].data_point[j]);
        }
        printf("%f) at distance %f\n", target_results->data[i].data_point[j], target_results->data[i].distance);
    }
}



int results_lt(results_datum* x, results_datum* y) {
    if (x->distance < y->distance) {
        return 1;
    } else {
        return 0;
    }
}



int results_gt(results_datum* x, results_datum* y) {
    if (x->distance > y->distance) {
        return 1;
    } else {
        return 0;
    }
}
