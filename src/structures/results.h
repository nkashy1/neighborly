
#ifndef RESULTS_H
#define RESULTS_H

/*********************************************************************************************
** Contains a data structure to hold the results of nearest neighbor queries, and utility functions to allow interaction with this structure.
**********************************************************************************************/

#include "../neighborly_types.h"



/**********************************************************
** results_datum struct definition and utilities
***********************************************************/

struct results_datum {
    NUMERICAL_TYPE *data_point;
    NUMERICAL_TYPE distance;
};



// results_datum_create
// Creates a fresh results_datum instance in memory and returns its address.
//
// Inputs:
//
// Output:  results_datum *, pointer to a fresh, empty results_datum struct instance
//
// Notes:
//  (1) data_point is initialized as NULL.
//  (2) distance is initialized as -1.

results_datum* results_datum_create();



// results_datum_destroy
// Deallocates memory allocated to a results_datum instantiation by results_datum_create.
//
// Inputs:  (1) results_datum *obsolete_datum, a results_datum instance which needs to be put on ice
//
// Output:  None
//
// Notes:
//

void results_datum_destroy(results_datum *obsolete_datum);



// results_datum_copy
// Copies a given results_datum struct.
//
// Inputs:  (1) results_datum *target_datum, a pointer to the results_datum to be copied
//          (2) int dimension, the number of slots in the data contained in target_datum
//
// Output:  results_datum *, a pointer to a copy of target_datum
//
// Notes:
//

results_datum* results_datum_copy(results_datum *target_datum, int dimension);



/**********************************************************
** results struct definition and utilities
***********************************************************/

struct results{
    int num_neighbors;
    results_datum *data;
};



// results_create
// Creates a fresh instance of results with a specified num_neighbors field.
//
// Inputs:  (1) int num_neighbors
//
// Output:  results *, a pointer to the fresh results structure
//
// Notes:
//

results* results_create(int num_neighbors);



// results_destroy
// Deallocates the memory used to store a results instance created by results_create.
//
// Inputs:  (1) results *obsolete_results, the results which need to sleep with the fishes
//
// Output:  None
//
// Notes:
//

void results_destroy(results *obsolete_results);



// results_print
// Prints the results of a nearest neighbor query.
//
// Inputs:  (1) results *target_results, the results to be printed to screen
//
// Output:  None
//
// Notes:
//

void results_print(int dimension, NUMERICAL_TYPE *target_point, results *target_results);



/****************************************
** results order functions
*****************************************/

int results_lt(results_datum* x, results_datum* y);

int results_gt(results_datum* x, results_datum* y);


#endif // RESULTS_H
