
#ifndef FLOAT_EPSILON
#define FLOAT_EPSILON 0.00001

/* Header file */
#include "test_distance.h"

/* Standard Libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../../src/structures/data_set.h"



void test_distance_functions(){

    int n;
    NUMERICAL_TYPE P[2], Q[2], p, output_distance;

    char* result;

    clock_t begin, elapsed;

    /*********************************
    ** distance_L_p Test 1:
    ** Calculate the L_2-distance between the points (0,0) and (1,1).
    *********************************/

    result = "Success";

    n = 2;
    P[0] = 0.0;
    P[1] = 0.0;
    Q[0] = 1.0;
    Q[1] = 1.0;
    p = 2;

    begin = clock();
    output_distance = distance_L_p(n, P, Q, p);
    elapsed = clock() - begin;

    //printf("\ndistance_L_p, Test 1:\nCalculated the L_2-distance between (%f, %f) and (%f, %f) to be %.10lf.\n", P[0], P[1], Q[0], Q[1], output_distance);
    //printf("The calculation took %d ticks.\n", (int) elapsed);

    if (fabs(output_distance - sqrt(2)) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\ndistance_L_p Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** distance_L_p Test 2:
    ** Calculate the L_1-distance between the points (0,0) and (1,1).
    *********************************/

    result = "Success";

    n = 2;
    P[0] = 0.0;
    P[1] = 0.0;
    Q[0] = 1.0;
    Q[1] = 1.0;
    p = 1;

    begin = clock();
    output_distance = distance_L_p(n, P, Q, p);
    elapsed = clock() - begin;

    //printf("\ndistance_L_p, Test 3:\nCalculated the L_1-distance between (%f, %f) and (%f, %f) to be %.10lf.\n", P[0], P[1], Q[0], Q[1], output_distance);
    //printf("The calculation took %d ticks.\n", (int) elapsed);

    if (fabs(output_distance - 2) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\ndistance_L_p Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** distance_L_p Test 3:
    ** Calculate the L_2-distance between the points (500.5, 500.5) and (500, 500).
    *********************************/

    result = "Success";

    n = 2;
    P[0] = 500.5;
    P[1] = 500.5;
    Q[0] = 500.0;
    Q[1] = 500.0;
    p = 2.0;

    begin = clock();
    output_distance = distance_L_p(n, P, Q, p);
    elapsed = clock() - begin;

    //printf("\ndistance_L_p, Test 4:\nCalculated the L_2-distance between (%f, %f) and (%f, %f) to be %.10lf.\n", P[0], P[1], Q[0], Q[1], output_distance);
    //printf("The calculation took %d ticks.\n", (int) elapsed);

    if (fabs(output_distance - (1/sqrt(2))) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\ndistance_L_p Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** distance_sup Test 1:
    ** Calculate the L_{\infty}-distance between the points (0,0) and (1,1).
    *********************************/

    result = "Success";

    n = 2;
    P[0] = 0.0;
    P[1] = 0.0;
    Q[0] = 1.0;
    Q[1] = 1.0;
    p = 0.0;

    begin = clock();
    output_distance = distance_sup(n, P, Q);
    elapsed = clock() - begin;

    //printf("\ndistance_L_p, Test 2:\nCalculated the L_{\\infty}-distance between (%f, %f) and (%f, %f) to be %.10lf.\n", P[0], P[1], Q[0], Q[1], output_distance);
    //printf("The calculation took %d ticks.\n", (int) elapsed);

    if (fabs(output_distance - 1) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\ndistance_sup Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

}



void test_distance_matrix_index_compression() {

    int compressed_index;

    char* result;

    clock_t begin, elapsed;

    /*********************************
    ** distance_matrix_index_compression Test 1:
    ** Check that, in a data set of size 2, (0, 1) maps to 0.
    *********************************/
    result = "Success";

    begin = clock();
    compressed_index = distance_matrix_index_compression(2, 0, 1);
    elapsed = clock() - begin;

    if (compressed_index != 0) {
        result = "Failure";
    }

    printf("\ndistance_matrix_index_compression Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** distance_matrix_index_compression Test 2:
    ** Check that, in a data set of size 2, (1, 0) maps to 0.
    *********************************/
    result = "Success";

    begin = clock();
    compressed_index = distance_matrix_index_compression(2, 1, 0);
    elapsed = clock() - begin;

    if (compressed_index != 0) {
        result = "Failure";
    }

    printf("\ndistance_matrix_index_compression Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** distance_matrix_index_compression Test 3:
    ** Check that, in a data set of size 5, (2, 3) maps to 7.
    *********************************/
    result = "Success";

    begin = clock();
    compressed_index = distance_matrix_index_compression(5, 2, 3);
    elapsed = clock() - begin;

    if (compressed_index != 7) {
        result = "Failure";
    }

    printf("\ndistance_matrix_index_compression Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** distance_matrix_index_compression Test 4:
    ** Check that, in a data set of size 5, (2, 4) maps to 8.
    *********************************/
    result = "Success";

    begin = clock();
    compressed_index = distance_matrix_index_compression(5, 2, 4);
    elapsed = clock() - begin;

    if (compressed_index != 8) {
        result = "Failure";
    }

    printf("\ndistance_matrix_index_compression Test 4: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** distance_matrix_index_compression Test 5:
    ** Check that, in a data set of size 10, (7, 9) maps to 43.
    *********************************/
    result = "Success";

    begin = clock();
    compressed_index = distance_matrix_index_compression(10, 7, 9);
    elapsed = clock() - begin;

    if (compressed_index != 43) {
        result = "Failure";
    }

    printf("\ndistance_matrix_index_compression Test 5: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** distance_matrix_index_compression Test 6:
    ** Check that, in a data set of size 10, (8, 7) maps to 42.
    *********************************/
    result = "Success";

    begin = clock();
    compressed_index = distance_matrix_index_compression(10, 8, 7);
    elapsed = clock() - begin;

    if (compressed_index != 42) {
        result = "Failure";
    }

    printf("\ndistance_matrix_index_compression Test 6: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

}



void test_distance_matrix_utilities() {

    char *result;

    /*********************************
    ** distance_matrix utilities Test 1:
    ** Distances between the points 0, 1, and 2 in R.
    *********************************/

    result = "Success";

    printf("\ndistance_matrix utilities Test 1:\n");

    int n = 1, N = 3;
    data_set *D = data_set_create(n, N);

    int i, j;

    for (i = 0; i < N; i++) {
        D->data[i] = i;
    }

    clock_t begin, elapsed;

    begin = clock();
    distance_matrix *pairwise_distances = distance_matrix_create(D);
    elapsed = clock() - begin;

    printf("distance_matrix_create --- %d ticks\n", (int) elapsed);

    NUMERICAL_TYPE extracted_distance;

    begin = clock();
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            if (j == i) {
                continue;
            } else {
                extracted_distance = extract_distance(pairwise_distances, i, j);
                if (fabs(extracted_distance - (float) abs(i - j)) > FLOAT_EPSILON) {
                    result = "Failure";
                }
            }
        }
    }
    elapsed = clock() - begin;

    printf("extract_distance --- %s in %d ticks.\n", result, (int) elapsed);

    // Deallocate all that memory
    data_set_destroy(D);
    distance_matrix_destroy(pairwise_distances);
}


#endif // FLOAT_EPSILON
