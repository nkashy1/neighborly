/* Header file */
#include "test_median.h"

/* Standard libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../../../src/structures/common/csv.h"



void test_csv() {
    csv_info info;

    char *filename;
    char delimiter;

    char *result;

    clock_t begin, elapsed;

    /*****************************************
    ** csv_info Test 1:
    ** Check that tests/csv_loadinfo1.csv, using delimiter ',' has 2 fields and 2 rows.
    ******************************************/

    result = "Success";

    filename = "tests/csv_loadinfo1.csv";
    delimiter = ',';

    begin = clock();
    info = csv_loadinfo(filename, delimiter);
    elapsed = clock() - begin;

    if (info.filename != filename || info.delimiter != delimiter || info.num_fields != 2 || info.num_rows != 2) {
        result = "Failure";
    }

    printf("\ncsv_info Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*****************************************
    ** csv_info Test 2:
    ** Check that tests/csv_loadinfo2.csv, using delimiter ',' has 4 fields and 3 rows.
    ******************************************/

    result = "Success";

    filename = "tests/csv_loadinfo2.csv";
    delimiter = ',';

    begin = clock();
    info = csv_loadinfo(filename, delimiter);
    elapsed = clock() - begin;

    if (info.filename != filename || info.delimiter != delimiter || info.num_fields != 4 || info.num_rows != 3) {
        result = "Failure";
    }

    printf("\ncsv_info Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);
}
