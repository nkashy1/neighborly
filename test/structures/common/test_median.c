
/* Header file */
#include "test_median.h"

/* Standard libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../../../src/structures/data_set.h"


void test_median() {
    char* result;
    data_set *test_data;
    NUMERICAL_TYPE *test_point;
    int axis, i;

    clock_t begin, elapsed;

    // Allocate a large chunk of memory to test_data
    test_data = data_set_create(1, 500);

    /*********************************
    ** quick_select Test 1:
    ** Test to see if the median of the data_set consisting of the vectors (2, 3), (4, 1), (1, 2) along axis 0 is (2, 3).
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 2;
    test_data->num_data_points = 3;

    test_data->data[0] = 2;
    test_data->data[1] = 3;
    test_data->data[2] = 4;
    test_data->data[3] = 1;
    test_data->data[4] = 1;
    test_data->data[5] = 2;

    test_data->data_pointers[0] = test_data->data;
    test_data->data_pointers[1] = test_data->data + 2;
    test_data->data_pointers[2] = test_data->data + 4;

    axis = 0;

    begin = clock();
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    /* Debugging:
    int i;
    for (i = 0; i < 3; i++) {
        test_point = extract_data(test_data, i);
        printf("(%f, %f)\n", test_point[0], test_point[1]);
    }
    */

    test_point = extract_data(test_data, median_position(test_data->num_data_points));
    if (test_point[0] != 2 || test_point[1] != 3) {
        result = "Failure";
    }

    printf("\nquick_select Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** quick_select Test 2:
    ** Test to see if the median of the data_set consisting of the vectors (2, 3), (4, 1), (1, 2) along axis 1 is (1, 2).
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 2;
    test_data->num_data_points = 3;

    test_data->data[0] = 2;
    test_data->data[1] = 3;
    test_data->data[2] = 4;
    test_data->data[3] = 1;
    test_data->data[4] = 1;
    test_data->data[5] = 2;

    test_data->data_pointers[0] = test_data->data;
    test_data->data_pointers[1] = test_data->data + 2;
    test_data->data_pointers[2] = test_data->data + 4;

    axis = 1;

    begin = clock();
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    /* Debugging:
    int i;
    for (i = 0; i < 3; i++) {
        test_point = extract_data(test_data, i);
        printf("(%f, %f)\n", test_point[0], test_point[1]);
    }
    */

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 1 || test_point[1] != 2) {
        result = "Failure";
    }

    printf("\nquick_select Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** quick_select Test 3:
    ** Test to see if the median of the data_set consisting of the vectors (2, 3), (4, 1), (1, 2), and (3, 4) along axis 0 is (2, 3).
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 2;
    test_data->num_data_points = 4;

    test_data->data[0] = 2;
    test_data->data[1] = 3;
    test_data->data[2] = 4;
    test_data->data[3] = 1;
    test_data->data[4] = 1;
    test_data->data[5] = 2;
    test_data->data[6] = 3;
    test_data->data[7] = 4;

    test_data->data_pointers[0] = test_data->data;
    test_data->data_pointers[1] = test_data->data + 2;
    test_data->data_pointers[2] = test_data->data + 4;
    test_data->data_pointers[3] = test_data->data + 6;

    axis = 0;

    begin = clock();
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 2 || test_point[1] != 3) {
        result = "Failure";
    }

    printf("\nquick_select Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** quick_select Test 4:
    ** Test to see if the median of the data_set consisting of the vectors (2, 3), (4, 1), (1, 2), and (3, 4) along axis 1 is (1, 2).
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 2;
    test_data->num_data_points = 4;

    test_data->data[0] = 2;
    test_data->data[1] = 3;
    test_data->data[2] = 4;
    test_data->data[3] = 1;
    test_data->data[4] = 1;
    test_data->data[5] = 2;
    test_data->data[6] = 3;
    test_data->data[7] = 4;

    test_data->data_pointers[0] = test_data->data;
    test_data->data_pointers[1] = test_data->data + 2;
    test_data->data_pointers[2] = test_data->data + 4;
    test_data->data_pointers[3] = test_data->data + 6;

    axis = 1;

    begin = clock();
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 1 || test_point[1] != 2) {
        result = "Failure";
    }

    printf("\nquick_select Test 4: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** quick_select Test 5:
    ** Test to see if the median of the data_set consisting of the vectors (2, 3), (4, 1), (4, 1), and (4, 1) along axis 1 is (4, 1).
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 2;
    test_data->num_data_points = 4;

    test_data->data[0] = 2;
    test_data->data[1] = 3;
    test_data->data[2] = 4;
    test_data->data[3] = 1;
    test_data->data[4] = 4;
    test_data->data[5] = 1;
    test_data->data[6] = 4;
    test_data->data[7] = 1;

    test_data->data_pointers[0] = test_data->data;
    test_data->data_pointers[1] = test_data->data + 2;
    test_data->data_pointers[2] = test_data->data + 4;
    test_data->data_pointers[3] = test_data->data + 6;

    axis = 1;

    begin = clock();
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 4 || test_point[1] != 1) {
        result = "Failure";
    }

    printf("\nquick_select Test 5: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** quick_select Test 6:
    ** Test to see if the median of the data_set consisting of the vectors (4, 1), (2, 3), (4, 1), and (4, 1) along axis 1 is (4, 1).
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 2;
    test_data->num_data_points = 4;

    test_data->data[0] = 4;
    test_data->data[1] = 1;
    test_data->data[2] = 2;
    test_data->data[3] = 3;
    test_data->data[4] = 4;
    test_data->data[5] = 1;
    test_data->data[6] = 4;
    test_data->data[7] = 1;

    test_data->data_pointers[0] = test_data->data;
    test_data->data_pointers[1] = test_data->data + 2;
    test_data->data_pointers[2] = test_data->data + 4;
    test_data->data_pointers[3] = test_data->data + 6;

    axis = 1;

    begin = clock();
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 4 || test_point[1] != 1) {
        result = "Failure";
    }

    printf("\nquick_select Test 6: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** quick_select Test 7:
    ** Test to see if the median of the data_set consisting of the vectors (500), (499), ..., (1) along axis 0 is 250.
    ** This is the worst case for quick_select!
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 1;
    test_data->num_data_points = 500;

    for (i = 0; i < test_data->num_data_points; i++) {
        test_data->data[i] = 500 - i;
    }

    for (i = 0; i < test_data->num_data_points; i++) {
        test_data->data_pointers[i] = test_data->data + i;
    }

    axis = 0;

    begin = clock();
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 250) {
        result = "Failure";
    }

    printf("\nquick_select Test 7: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** median_of_medians Test 1:
    ** Test on the data_set consisting of the vectors (500), (499), ..., (1).
    ** Check that median_of_medians along axis 0 makes 248 the last entry in the data set.
    **********************************/

    result = "Success";

    test_data->embedding_dimension = 1;
    test_data->num_data_points = 500;

    for (i = 0; i < test_data->num_data_points; i++) {
        test_data->data[i] = 500 - i;
    }

    for (i = 0; i < test_data->num_data_points; i++) {
        test_data->data_pointers[i] = test_data->data + i;
    }

    axis = 0;

    begin = clock();
    median_of_medians(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, test_data->num_data_points - 1);
    if (test_point[0] != 248) {
        result = "Failure";
    }

    printf("\nmedian_of_medians Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /************************************************
    ** quick_select + median_of_medians Test 1:
    ** Find the median of the data_set consisting of the vectors (500), (499), ..., (1) along axis 0.
    ** It should be 250.
    ** It should be faster than straight up quick_select.
    *************************************************/

    result = "Success";

    test_data->embedding_dimension = 1;
    test_data->num_data_points = 500;

    for (i = 0; i < test_data->num_data_points; i++) {
        test_data->data[i] = 500 - i;
    }

    for (i = 0; i < test_data->num_data_points; i++) {
        test_data->data_pointers[i] = test_data->data + i;
    }

    axis = 0;

    begin = clock();
    median_of_medians(test_data, axis);
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 250) {
        result = "Failure";
    }

    printf("\nquick_select + median_of_medians Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /************************************************
    ** quick_select + median_of_medians Test 2:
    ** Check what happens when we try to calculate the median of the vectors (1, 2) and (3, 4) along axis 0.
    ** Should give us (1, 2) as the quick_select algorithm does nothing in this case.
    *************************************************/

    result = "Success";

    test_data->embedding_dimension = 2;
    test_data->num_data_points = 2;

    test_data->data[0] = 1;
    test_data->data[1] = 2;
    test_data->data[2] = 3;
    test_data->data[3] = 4;

    for (i = 0; i < test_data->num_data_points; i++) {
        test_data->data_pointers[i] = test_data->data + i;
    }

    axis = 0;

    begin = clock();
    median_of_medians(test_data, axis);
    quick_select(test_data, axis);
    elapsed = clock() - begin;

    test_point = extract_data(test_data, (test_data->num_data_points + 1)/2 - 1);
    if (test_point[0] != 1 || test_point[1] != 2) {
        result = "Failure";
    }

    printf("\nquick_select + median_of_medians Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    // FREE EVERYTHING!
    data_set_destroy(test_data);
}
