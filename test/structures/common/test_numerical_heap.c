
/* Header file */
#include "test_numerical_heap.h"

/* Standard libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */


void test_numerical_heap_utilities() {

    char *result;
    int array_sizes = 20, i;

    numerical_heap *target_heap = malloc(sizeof(numerical_heap));
    target_heap->order = gt;
    NUMERICAL_TYPE *target_array = malloc(sizeof(NUMERICAL_TYPE)*array_sizes);
    NUMERICAL_TYPE *test_array = malloc(sizeof(NUMERICAL_TYPE)*array_sizes);

    assert(target_heap != NULL);
    assert(target_array != NULL);
    assert(test_array != NULL);

    clock_t begin, elapsed;


    /*********************************
    ** numerical_heap_down Test 1:
    ** Test to see if the array [0, 1, 2] becomes [2, 1, 0] after numerical_heap_down(0, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 3;
    target_heap->current_size = 3;

    for (i = 0; i < 3; i++) {
        target_array[i] = (float) i;
    }

    target_heap->vertices = target_array;


    begin = clock();
    numerical_heap_down(0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 3; i++) {
        if (target_heap->vertices[i] != 2 - i) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_down Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_down Test 2:
    ** Test to see if the array [0, 4, 5, 2, 1, 3] becomes [5, 4, 3, 2, 1, 0] after numerical_heap_down(0, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 6;

    target_array[0] = 0.0;
    target_array[1] = 4.0;
    target_array[2] = 5.0;
    target_array[3] = 2.0;
    target_array[4] = 1.0;
    target_array[5] = 3.0;

    target_heap->vertices = target_array;


    begin = clock();
    numerical_heap_down(0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->vertices[i] != 5 - i) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_down Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_down Test 3:
    ** Test to see if the array [0, 4, 5, 2, 1, 3] remains the same after numerical_heap_down(5, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 6;

    target_array[0] = 0.0;
    target_array[1] = 4.0;
    target_array[2] = 5.0;
    target_array[3] = 2.0;
    target_array[4] = 1.0;
    target_array[5] = 3.0;

    target_heap->vertices = target_array;

    for (i = 0; i < 6; i++) {
        test_array[i] = target_array[i];
    }

    begin = clock();
    numerical_heap_down(5, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_down Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_up Test 1:
    ** Test to see if the array [0, 1, 2] becomes [2, 1, 0] after numerical_heap_up(2, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 3;
    target_heap->current_size = 3;

    for (i = 0; i < 3; i++) {
        target_array[i] = (float) i;
    }

    target_heap->vertices = target_array;


    begin = clock();
    numerical_heap_up(2, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 3; i++) {
        if (target_heap->vertices[i] != 2 - i) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_up Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_up Test 2:
    ** Test to see if the array [0, 4, 5, 2, 1, 3] remains the same after numerical_heap_up(0, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 6;

    target_array[0] = 0.0;
    target_array[1] = 4.0;
    target_array[2] = 5.0;
    target_array[3] = 2.0;
    target_array[4] = 1.0;
    target_array[5] = 3.0;

    target_heap->vertices = target_array;

    for (i = 0; i < 6; i++) {
        test_array[i] = target_array[i];
    }

    begin = clock();
    numerical_heap_up(0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_up Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_up Test 3:
    ** Test to see if the array [0, 4, 5, 2, 1, 3] remains the same after numerical_heap_up(3, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 6;

    target_array[0] = 0.0;
    target_array[1] = 4.0;
    target_array[2] = 5.0;
    target_array[3] = 2.0;
    target_array[4] = 1.0;
    target_array[5] = 3.0;

    target_heap->vertices = target_array;

    for (i = 0; i < 6; i++) {
        test_array[i] = target_array[i];
    }

    begin = clock();
    numerical_heap_up(3, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_up Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_up Test 4:
    ** Test to see if the array [4, 1, 3, 2, 5, 0] becomes [5, 4, 3, 2, 1, 0]  after numerical_heap_up(4, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 6;

    target_array[0] = 4.0;
    target_array[1] = 1.0;
    target_array[2] = 3.0;
    target_array[3] = 2.0;
    target_array[4] = 5.0;
    target_array[5] = 0.0;

    target_heap->vertices = target_array;

    begin = clock();
    numerical_heap_up(4, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->vertices[i] != 5 - i) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_up Test 4: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_insert Test 1:
    ** Test to see if inserting the value 5 into the array [4, 3, 2, 1, 0] yields the array [5, 3, 4, 1, 0, 2].
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 5;

    target_array[0] = 4.0;
    target_array[1] = 3.0;
    target_array[2] = 2.0;
    target_array[3] = 1.0;
    target_array[4] = 0.0;

    target_heap->vertices = target_array;

    test_array[0] = 5.0;
    test_array[1] = 3.0;
    test_array[2] = 4.0;
    test_array[3] = 1.0;
    test_array[4] = 0.0;
    test_array[5] = 2.0;

    begin = clock();
    numerical_heap_insert(5.0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->current_size != 6) {
            result = "Failure";
            break;
        }
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_insert Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_insert Test 2:
    ** Test to see if inserting the value 4 into the array [5, 3, 2, 1, 0] yields the array [5, 3, 4, 1, 0, 2].
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 5;

    target_array[0] = 5.0;
    target_array[1] = 3.0;
    target_array[2] = 2.0;
    target_array[3] = 1.0;
    target_array[4] = 0.0;

    target_heap->vertices = target_array;

    test_array[0] = 5.0;
    test_array[1] = 3.0;
    test_array[2] = 4.0;
    test_array[3] = 1.0;
    test_array[4] = 0.0;
    test_array[5] = 2.0;

    begin = clock();
    numerical_heap_insert(4.0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->current_size != 6) {
            result = "Failure";
            break;
        }
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_insert Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_insert Test 3:
    ** Test to see if inserting the value 2 into the array [6, 3, 5, 1, 0, 4] yields the array [6, 3, 5, 1, 0, 4, 2].
    **********************************/

    result = "Success";

    target_heap->max_size = 7;
    target_heap->current_size = 6;

    target_array[0] = 6.0;
    target_array[1] = 3.0;
    target_array[2] = 5.0;
    target_array[3] = 1.0;
    target_array[4] = 0.0;
    target_array[5] = 4.0;

    target_heap->vertices = target_array;

    test_array[0] = 6.0;
    test_array[1] = 3.0;
    test_array[2] = 5.0;
    test_array[3] = 1.0;
    test_array[4] = 0.0;
    test_array[5] = 4.0;
    test_array[6] = 2.0;

    begin = clock();
    numerical_heap_insert(2.0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < target_heap->max_size; i++) {
        if (target_heap->current_size != target_heap->max_size) {
            result = "Failure";
            break;
        }
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_insert Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_delete Test 1:
    ** Test to see if the array [2, 1, 0] becomes [1, 0] after numerical_heap_delete(0, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 3;
    target_heap->current_size = 3;

    target_array[0] = 2.0;
    target_array[1] = 1.0;
    target_array[2] = 0.0;

    target_heap->vertices = target_array;

    test_array[0] = 1.0;
    test_array[1] = 0.0;

    begin = clock();
    numerical_heap_delete(0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 2; i++) {
        if (target_heap->current_size != 2) {
            result = "Failure";
            break;
        }
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_delete Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_delete Test 2:
    ** Test to see if the array [2, 1, 0] becomes [2, 0] after numerical_heap_delete(1, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 3;
    target_heap->current_size = 3;

    target_array[0] = 2.0;
    target_array[1] = 1.0;
    target_array[2] = 0.0;

    target_heap->vertices = target_array;

    test_array[0] = 2.0;
    test_array[1] = 0.0;

    begin = clock();
    numerical_heap_delete(1, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 2; i++) {
        if (target_heap->current_size != 2) {
            result = "Failure";
            break;
        }
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_delete Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_delete Test 3:
    ** Test to see if the array [5, 4, 3, 0, 1, 2] becomes [4, 2, 3, 0, 1] after numerical_heap_delete(0, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 6;
    target_heap->current_size = 6;

    target_array[0] = 5.0;
    target_array[1] = 4.0;
    target_array[2] = 3.0;
    target_array[3] = 0.0;
    target_array[4] = 1.0;
    target_array[5] = 2.0;

    target_heap->vertices = target_array;

    test_array[0] = 4.0;
    test_array[1] = 2.0;
    test_array[2] = 3.0;
    test_array[3] = 0.0;
    test_array[4] = 1.0;

    begin = clock();
    numerical_heap_delete(0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 2; i++) {
        if (target_heap->current_size != 5) {
            result = "Failure";
            break;
        }
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_delete Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_delete Test 4:
    ** Test to see if the array [9, 4, 8, 3, 2, 7, 5, 0, 1, -1, -2, 6] becomes [9, 6, 8, 4, 2, 7, 5, 0, 1, -1, -2] after numerical_heap_delete(3, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 12;
    target_heap->current_size = 12;

    target_array[0] = 9.0;
    target_array[1] = 4.0;
    target_array[2] = 8.0;
    target_array[3] = 3.0;
    target_array[4] = 2.0;
    target_array[5] = 7.0;
    target_array[6] = 5.0;
    target_array[7] = 0.0;
    target_array[8] = 1.0;
    target_array[9] = -1.0;
    target_array[10] = -2.0;
    target_array[11] = 6.0;

    target_heap->vertices = target_array;

    test_array[0] = 9.0;
    test_array[1] = 6.0;
    test_array[2] = 8.0;
    test_array[3] = 4.0;
    test_array[4] = 2.0;
    test_array[5] = 7.0;
    test_array[6] = 5.0;
    test_array[7] = 0.0;
    test_array[8] = 1.0;
    test_array[9] = -1.0;
    test_array[10] = -2.0;

    begin = clock();
    numerical_heap_delete(3, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 11; i++) {
        if (target_heap->current_size != 11) {
            result = "Failure";
            break;
        }
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_delete Test 4: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_heapify Test 1:
    ** Test to see if the array [0, 1, 2] becomes [2, 1, 0] after numerical_heap_heapify(0, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 3;
    target_heap->current_size = 3;

    target_array[0] = 0.0;
    target_array[1] = 1.0;
    target_array[2] = 2.0;

    target_heap->vertices = target_array;

    test_array[0] = 2.0;
    test_array[1] = 1.0;
    test_array[2] = 0.0;

    begin = clock();
    numerical_heap_heapify(0, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 3; i++) {
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_heapify Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_heapify Test 2:
    ** Test to see if the array [5, 0, 4, 1, 2] becomes [5, 2, 4, 1, 0] after numerical_heap_heapify(1, target_heap).
    **********************************/

    result = "Success";

    target_heap->max_size = 5;
    target_heap->current_size = 5;

    target_array[0] = 5.0;
    target_array[1] = 0.0;
    target_array[2] = 4.0;
    target_array[3] = 1.0;
    target_array[4] = 2.0;

    target_heap->vertices = target_array;

    test_array[0] = 5.0;
    test_array[1] = 2.0;
    test_array[2] = 4.0;
    test_array[3] = 1.0;
    test_array[4] = 0.0;

    begin = clock();
    numerical_heap_heapify(1, target_heap);
    elapsed = clock() - begin;

    for (i = 0; i < 5; i++) {
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_heapify Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_create Test 1:
    ** Test to see if the array [0, 1, 2] becomes the heap [2, 1, 0] after numerical_heap_create([0, 1, 2], 3, 3, gt).
    **********************************/

    result = "Success";

    target_array[0] = 0.0;
    target_array[1] = 1.0;
    target_array[2] = 2.0;

    test_array[0] = 2.0;
    test_array[1] = 1.0;
    test_array[2] = 0.0;

    begin = clock();
    target_heap = numerical_heap_create(target_array, 3, 3, gt);
    elapsed = clock() - begin;

    for (i = 0; i < 3; i++) {
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_create Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    /*********************************
    ** numerical_heap_create Test 2:
    ** Test to see if the array [4, 3, 1, 0, 5, 2] becomes the heap [5, 4, 2, 0, 3, 1] after numerical_heap_create([4, 3, 1, 0, 5, 2], 6, 10).
    **********************************/

    result = "Success";

    target_array[0] = 4.0;
    target_array[1] = 3.0;
    target_array[2] = 1.0;
    target_array[3] = 0.0;
    target_array[4] = 5.0;
    target_array[5] = 2.0;

    test_array[0] = 5.0;
    test_array[1] = 4.0;
    test_array[2] = 2.0;
    test_array[3] = 0.0;
    test_array[4] = 3.0;
    test_array[5] = 1.0;

    begin = clock();
    target_heap = numerical_heap_create(target_array, 6, 10, gt);
    elapsed = clock() - begin;

    for (i = 0; i < 6; i++) {
        if (target_heap->vertices[i] != test_array[i]) {
            result = "Failure";
            break;
        }
    }

    printf("\nnumerical_heap_create Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);



    // Free up all that allocated memory!

    free(test_array);
    free(target_array);
    free(target_heap);

}
