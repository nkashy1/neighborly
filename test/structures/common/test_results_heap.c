
/* Header file */
#include "test_results_heap.h"

/* Standard libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../../../src/metrics/distance.h"
#include "../../../src/structures/results.h"

void test_results_heap_utilities() {
    char* result;
    results_heap *test_heap, *test_heap_2;
    results_datum *test_data;
    int dimension, i;
    NUMERICAL_TYPE *reference_point;

    clock_t begin, elapsed;

    /*********************************
    ** results_heap utilities Test 1:
    ** Run the results_heaps utilities by storing the points (1,0), (0,1), and (0,0) in relation to the point (2,1).
    ** The order on the results heap should be (0,0), (0,1), (1,0).
    **********************************/

    result = "Success";

    dimension = 2;
    reference_point = malloc(sizeof(NUMERICAL_TYPE)*dimension);
    reference_point[0] = 2;
    reference_point[1] = 1;

    test_data = malloc(sizeof(results_datum)*3);
    for (i = 0; i < 3; i++) {
        test_data[i].data_point = malloc(sizeof(NUMERICAL_TYPE)*dimension);
    }

    test_data[0].data_point[0] = 1;
    test_data[0].data_point[1] = 0;
    test_data[1].data_point[0] = 0;
    test_data[1].data_point[1] = 1;
    test_data[2].data_point[0] = 0;
    test_data[2].data_point[1] = 0;

    for (i = 0; i < 3; i++) {
        test_data[i].distance = distance_L_p_power(2, test_data[i].data_point, reference_point, 2);
    }

    begin = clock();
    test_heap = results_heap_create(test_data, 3, 3, results_gt);
    elapsed = clock() - begin;

    if (test_heap->vertices[0].data_point[0] != 0 || test_heap->vertices[0].data_point[1] != 0) {
        result = "Failure";
    }
    if (test_heap->vertices[1].data_point[0] != 0 || test_heap->vertices[1].data_point[1] != 1) {
        result = "Failure";
    }
    if (test_heap->vertices[2].data_point[0] != 1 || test_heap->vertices[2].data_point[1] != 0) {
        result = "Failure";
    }

    printf("\nresults_heap_create Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    result = "Success";

    begin = clock();
    test_heap_2 = results_heap_copy(test_heap, 2);
    elapsed = clock() - begin;

    if (test_heap_2 == test_heap) {
        goto copy_1_failure;
    }

    if (test_heap_2->current_size != test_heap->current_size || test_heap_2->max_size != test_heap->max_size) {
        goto copy_1_failure;
    }

    if (test_heap_2->vertices == test_heap->vertices) {
        goto copy_1_failure;
    }

    for (i = 0; i < 3; i++) {
        if (test_heap_2->vertices[i].distance != test_heap->vertices[i].distance) {
            goto copy_1_failure;
        }

        if (test_heap_2->vertices[i].data_point[0] != test_heap->vertices[i].data_point[0]) {
            goto copy_1_failure;
        }

        if (test_heap_2->vertices[i].data_point[1] != test_heap->vertices[i].data_point[1]) {
            goto copy_1_failure;
        }
    }

    goto copy_1_success;

    copy_1_failure:
    result = "Failure";
    copy_1_success:
    printf("\nresults_heap_copy Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    for (i = 0; i < 3; i++) {
        free(test_heap->vertices[i].data_point);
        free(test_heap_2->vertices[i].data_point);
    }
    free(test_heap->vertices);
    free(test_heap);
    free(test_heap_2->vertices);
    free(test_heap_2);
    free(reference_point);
}
