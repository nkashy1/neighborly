
#ifndef FLOAT_EPSILON
#define FLOAT_EPSILON 0.00001

/* Header file */
#include "test_spread.h"

/* Standard libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../../../src/structures/data_set.h"
#include "../../../src/structures/common/median.h"



void test_spread() {
    char* result;
    data_set *test_data;
    NUMERICAL_TYPE test_value;
    int axis, i;

    clock_t begin, elapsed;

    // Allocate a large chunk of memory to test_data
    test_data = data_set_create(2, 100);


    /*************************************************
    ** madmed Test 1
    ** Consider the data_set {(i, 2i) : 1 <= i <= 100}.
    ** The mean absolute deviation from the median along axis 0 is 25.
    ** The mean absolute deviation from the median along axis 1 is 50.
    ** Let us see if this is what madmed thinks.
    **************************************************/
    result = "Success";

    for (i = 0; i < 100; i++) {
        test_data->data[2*i] = (NUMERICAL_TYPE) i + 1;
        test_data->data[2*i + 1] = (NUMERICAL_TYPE) 2*(i + 1);
    }

    axis = 0;
    begin = clock();
    test_value = madmed(test_data, axis);
    elapsed = clock() - begin;

    if (ABS(test_value - 25) > FLOAT_EPSILON) {
        printf("%f\n", test_value);
        goto madmed1_failure;
    }

    axis = 1;
    begin = clock();
    test_value = madmed(test_data, axis);
    elapsed += clock() - begin;

    if (ABS(test_value - 50) > FLOAT_EPSILON) {
        goto madmed1_failure;
    }

    goto madmed1_success;

    madmed1_failure:
    result = "Failure";

    madmed1_success:
    printf("\nmadmed Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*************************************************
    ** max_axis Test 1
    ** Check that max_axis returns 1 as the maximal axis for the previous data_set when using madmed as the spread function.
    **************************************************/
    result = "Success";

    begin = clock();
    axis = max_axis(test_data, madmed);
    elapsed = clock() - begin;

    if (axis != 1) {
        result = "Failure";
    }

    printf("\nmax_axis Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    // FREE STUFF
    data_set_destroy(test_data);
}

#endif // FLOAT_EPSILON
