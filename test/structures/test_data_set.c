
/* Header file */
#include "test_data_set.h"

/* Standard Libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../../src/structures/common/csv.h"



void test_data_set() {

    // Initialize a data set --- the 3 x 10 table of integers between 0 and 29 written sequentially, row-wise.
    data_set *D;
    D = data_set_create(3, 10);
    int total_indices = (D->embedding_dimension)*(D->num_data_points);

    char* result;
    NUMERICAL_TYPE* extracted_point;
    int i, j;

    clock_t begin, elapsed;

    for (i = 0; i < total_indices; i++){
        D->data[i] = i;
    }

    /*********************************
    ** extract_data Test 1:
    ** Extract first row and check if it is (0,1,2).
    *********************************/

    result = "Success";

    begin = clock();
    extracted_point = extract_data(D, 0);
    elapsed = clock() - begin;

    for (i = 0; i < 3; i++){
        if (extracted_point[i] != (float) i){
            result = "Failure";
            break;
        }
    }

    printf("\nextract_data Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** extract_data Test 2:
    ** Extract second row and check if it is (3,4,5).
    *********************************/

    result = "Success";

    begin = clock();
    extracted_point = extract_data(D, 1);
    elapsed = clock() - begin;

    for (i = 0; i < 3; i++){
        if (extracted_point[i] != (float)(3 + i)){
            result = "Failure";
            break;
        }
    }

    printf("\nextract_data Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** extract_data Test 3:
    ** Extract last row and check if it is (27,28,29).
    *********************************/

    result = "Success";

    begin = clock();
    extracted_point = extract_data(D, 9);
    elapsed = clock() - begin;

    for (i = 0; i < 3; i++){
        if (extracted_point[i] != (float) (27 + i)){
            result = "Failure";
            break;
        }
    }

    printf("\nextract_data Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** data_set_swap Test 1:
    ** Swap the first and last entries of the data_set D.
    *********************************/

    result = "Success";

    begin = clock();
    data_set_swap(D, 0, 9);
    elapsed = clock() - begin;

    extracted_point = extract_data(D, 0);
    if (extracted_point[0] != 27 || extracted_point[1] != 28 || extracted_point[2] != 29) {
        result = "Failure";
    }

    extracted_point = extract_data(D, 9);
    if (extracted_point[0] != 0 || extracted_point[1] != 1 || extracted_point[2] != 2) {
        result = "Failure";
    }

    for (i = 1; i < 9; i++) {
        extracted_point = extract_data(D, i);
        for (j = 0; j < 3; j++) {
            if (extracted_point[j] != 3*i + j) {
                goto swap_1_failure;
            }
        }
    }

    goto swap_1_success;

    swap_1_failure:
    result = "Failure";

    swap_1_success:
    printf("\ndata_set_swap Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** data_set_swap Test 2:
    ** Swap the first entry of the data_set D with itself.
    *********************************/

    result = "Success";

    begin = clock();
    data_set_swap(D, 0, 9);
    elapsed = clock() - begin;

    for (i = 0; i < 10; i++) {
        extracted_point = extract_data(D, i);
        for (j = 0; j < 3; j++) {
            if (extracted_point[j] != 3*i + j) {
                goto swap_2_failure;
            }
        }
    }

    goto swap_2_success;

    swap_2_failure:
    result = "Failure";

    swap_2_success:
    printf("\ndata_set_swap Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    // Deallocation of dynamically allocated D->data.
    data_set_destroy(D);


    /*********************************
    ** data_set_loadcsv Test 1:
    ** Check that the data from tests/data_set_loadcsv1.csv is correctly loaded.
    *********************************/

    result = "Success";

    csv_info info;
    info = csv_loadinfo("tests/data_set_loadcsv1.csv", ',');

    begin = clock();
    D = data_set_loadcsv(info);
    elapsed = clock() - begin;

    if (D->embedding_dimension != 2 || D->num_data_points != 2) {
        result = "Failure";
    }

    if (extract_data(D, 0)[0] != 1 || extract_data(D, 0)[1] != 2 || extract_data(D, 1)[0] != 3 || extract_data(D, 1)[1] != 4) {
        result = "Failure";
    }

    printf("\ndata_set_loadcsv Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    data_set_destroy(D);


    /*********************************
    ** data_set_loadcsv Test 2:
    ** Check that the data from tests/data_set_loadcsv2.csv is correctly loaded.
    *********************************/

    result = "Success";

    info = csv_loadinfo("tests/data_set_loadcsv2.csv", ',');

    begin = clock();
    D = data_set_loadcsv(info);
    elapsed = clock() - begin;

    if (D->embedding_dimension != 2 || D->num_data_points != 2) {
        result = "Failure";
    }

    if (extract_data(D, 0)[0] != 1 || extract_data(D, 0)[1] != 2 || extract_data(D, 1)[0] != 3 || extract_data(D, 1)[1] != 4) {
        result = "Failure";
    }

    printf("\ndata_set_loadcsv Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    data_set_destroy(D);


    /*********************************
    ** data_set_loadcsv Test 3:
    ** Check that the data from tests/data_set_loadcsv3.csv is correctly loaded.
    *********************************/

    result = "Success";

    info = csv_loadinfo("tests/data_set_loadcsv3.csv", ',');

    begin = clock();
    D = data_set_loadcsv(info);
    elapsed = clock() - begin;

    if (D->embedding_dimension != 5 || D->num_data_points != 300) {
        goto data_set_loadcsv3_failure;
    }

    for (i = D->num_data_points*D->embedding_dimension - 1; i >= 0; i--) {
        if (D->data[i] != i) {
            goto data_set_loadcsv3_failure;
        }
    }

    goto data_set_loadcsv3_success;

    data_set_loadcsv3_failure:
    result = "Failure";

    data_set_loadcsv3_success:
    printf("\ndata_set_loadcsv Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    data_set_destroy(D);
}
