
/* Header file */
#include "test_kd_tree.h"

/* Standard Libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../../src/structures/data_set.h"


void test_kd_tree() {
    char *result;
    clock_t begin, elapsed;

    kd_vertex *test_vertex, *temp_vertex;
    kd_tree* test_tree;
    kd_vertex** test_enumeration;
    int i;

    data_set *test_data;


    /**************************************
    ** kd_vertex utilities Test 1:
    ** Check to see if we can create and destroy kd_vertex structs without any problems.
    ***************************************/

    result = "Success";

    begin = clock();
    test_vertex = kd_vertex_create(2, 0);
    i = test_vertex->disc_key;
    temp_vertex = test_vertex->children[0];
    kd_vertex_destroy(test_vertex);
    elapsed = clock() - begin;

    if (i != 0 || temp_vertex != NULL) {
        result = "Failure";
    }

    printf("\nkd_vertex utilities Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_create Test 1:
    ** Check to see if creating an empty kd_tree of dimension 2 goes off without any hitches.
    ***************************************/

    result = "Success";

    begin = clock();
    test_tree = kd_tree_create(2);
    elapsed = clock() - begin;

    if (test_tree->dimension != 2 || test_tree->root != NULL) {
        result = "Failure";
    }

    printf("\nkd_tree_create Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_insert Test 1:
    ** Insert the vector (0,0) into test_tree from the previous test.
    ***************************************/

    result = "Success";

    NUMERICAL_TYPE v[2];
    v[0] = 0.0;
    v[1] = 0.0;

    begin = clock();
    kd_tree_insert(test_tree, v);
    elapsed = clock() - begin;

    if (test_tree->dimension != 2 || test_tree->root->record[0] != 0.0 || test_tree->root->record[1] != 0.0 || test_tree->root->disc_key != 0 || test_tree->root->children[0] != NULL || test_tree->root->children[1] != NULL) {
        result = "Failure";
    }

    printf("\nkd_tree_insert Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_insert Test 2:
    ** Insert the vector (1,0) into test_tree from the previous test.
    ***************************************/

    result = "Success";

    v[0] = 1.0;
    v[1] = 0.0;

    begin = clock();
    kd_tree_insert(test_tree, v);
    elapsed = clock() - begin;

    temp_vertex = test_tree->root->children[0];
    test_vertex = test_tree->root->children[1];

    if (temp_vertex != NULL || test_vertex == NULL) {
        result = "Failure";
    }

    if (test_vertex->record[0] != 1.0 || test_vertex->record[1] != 0.0) {
        result = "Failure";
    }

    printf("\nkd_tree_insert Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_insert Test 3:
    ** Insert the vector (-1,1) into test_tree from the previous test.
    ***************************************/

    result = "Success";

    v[0] = -1.0;
    v[1] = 1.0;

    begin = clock();
    kd_tree_insert(test_tree, v);
    elapsed = clock() - begin;

    test_vertex = test_tree->root->children[0];

    if (test_vertex == NULL) {
        result = "Failure";
    }

    if (test_vertex->record[0] != -1.0 || test_vertex->record[1] != 1.0) {
        result = "Failure";
    }

    if (test_tree->depth != 1) {
        result = "Failure";
    }

    printf("\nkd_tree_insert Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_insert Test 4:
    ** Insert the vector (-1,-1) into test_tree from the previous test.
    ***************************************/

    result = "Success";

    v[0] = -1.0;
    v[1] = -1.0;

    begin = clock();
    kd_tree_insert(test_tree, v);
    elapsed = clock() - begin;

    test_vertex = test_vertex->children[0];

    if (test_vertex == NULL) {
        result = "Failure";
    }

    if (test_vertex->record[0] != -1.0 || test_vertex->record[1] != -1.0) {
        result = "Failure";
    }

    printf("\nkd_tree_insert Test 4: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_insert Test 5:
    ** Insert the vector (2,-1) into test_tree from the previous test and check if the structure of the entire tree is as we expect.
    ***************************************/

    result = "Success";

    v[0] = 2.0;
    v[1] = -1.0;

    begin = clock();
    kd_tree_insert(test_tree, v);
    elapsed = clock() - begin;

    test_vertex = test_tree->root;

    if (test_vertex->record[0] != 0.0 || test_vertex->record[1] != 0.0) {
        result = "Failure";
    }

    if (test_vertex->extremes[0] != -1.0 || test_vertex->extremes[1] != 2.0) {
        result = "Failure";
    }

    temp_vertex = test_vertex->children[0];
    test_vertex = test_vertex->children[1];

    if (temp_vertex->record[0] != -1.0 || temp_vertex->record[1] != 1.0 || test_vertex->record[0] != 1.0 || test_vertex->record[1] != 0.0) {
        result = "Failure";
    }

    if (temp_vertex->extremes[0] != -1.0 || test_vertex->extremes[0] != -1.0) {
        result = "Failure";
    }

    temp_vertex = temp_vertex->children[0];
    test_vertex = test_vertex->children[0];

    if (temp_vertex->record[0] != -1.0 || temp_vertex->record[1] != -1.0 || test_vertex->record[0] != 2.0 || test_vertex->record[1] != -1.0) {
        result = "Failure";
    }

    if (temp_vertex->extremes[0] != -1.0 || test_vertex->extremes[1] != 2.0) {
        result = "Failure";
    }

    if (test_tree->num_vertices != 5) {
        result = "Failure";
    }

    if (test_tree->depth != 2) {
        result = "Failure";
    }

    printf("\nkd_tree_insert Test 5: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_destroy Test 1:
    ** Check that the destruction doesn't segfault.
    ***************************************/

    result = "Success";

    begin = clock();
    kd_tree_destroy(test_tree);
    elapsed = clock() - begin;

    printf("\nkd_tree_destroy Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_construct Test 1:
    ** Check that a k-d tree is correctly constructed from the data set corresponding to the previous example. The points are:
    ** 1. (0, 0)
    ** 2. (1, 0)
    ** 3. (-1, 1)
    ** 4. (-1, -1)
    ** 5. (2, -1)
    ***************************************/

    result = "Success";

    test_data = data_set_create(2, 5);

    test_data->data[0] = 0.0;
    test_data->data[1] = 0.0;
    test_data->data[2] = 1.0;
    test_data->data[3] = 0.0;
    test_data->data[4] = -1.0;
    test_data->data[5] = 1.0;
    test_data->data[6] = -1.0;
    test_data->data[7] = -1.0;
    test_data->data[8] = 2.0;
    test_data->data[9] = -1.0;

    begin = clock();
    test_tree = kd_tree_construct(test_data);
    elapsed = clock() - begin;

    test_vertex = test_tree->root;

    if (test_vertex->record[0] != 0.0 || test_vertex->record[1] != 0.0) {
        result = "Failure";
    }

    if (test_vertex->extremes[0] != -1.0 || test_vertex->extremes[1] != 2.0) {
        result = "Failure";
    }

    temp_vertex = test_vertex->children[0];
    test_vertex = test_vertex->children[1];

    if (temp_vertex->record[0] != -1.0 || temp_vertex->record[1] != 1.0 || test_vertex->record[0] != 1.0 || test_vertex->record[1] != 0.0) {
        result = "Failure";
    }

    if (temp_vertex->extremes[0] != -1.0 || test_vertex->extremes[0] != -1.0) {
        result = "Failure";
    }

    temp_vertex = temp_vertex->children[0];
    test_vertex = test_vertex->children[0];

    if (temp_vertex->record[0] != -1.0 || temp_vertex->record[1] != -1.0 || test_vertex->record[0] != 2.0 || test_vertex->record[1] != -1.0) {
        result = "Failure";
    }

    if (temp_vertex->extremes[0] != -1.0 || test_vertex->extremes[1] != 2.0) {
        result = "Failure";
    }

    if (test_tree->num_vertices != 5) {
        result = "Failure";
    }

    if (test_tree->depth != 2) {
        result = "Failure";
    }

    printf("\nkd_tree_construct Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_enumerate Test 1:
    ** Check that the enumeration of the vertices in the existing test_tree is as expected.
    ***************************************/

    result = "Success";

    begin = clock();
    test_enumeration = kd_tree_enumerate(test_tree);
    elapsed = clock() - begin;

    /*
    for (i = 0; i < test_tree->num_vertices; i++) {
        printf("\n%d: %f, %f\n", i, test_enumeration[i]->record[0], test_enumeration[i]->record[1]);
    }
    */

    if (test_enumeration[0]->record[0] != 0.0 || test_enumeration[0]->record[1] != 0.0) {
        result = "Failure";
    }

    if (test_enumeration[1]->record[0] != -1.0 || test_enumeration[1]->record[1] != 1.0) {
        result = "Failure";
    }

    if (test_enumeration[2]->record[0] != 1.0 || test_enumeration[2]->record[1] != 0.0) {
        result = "Failure";
    }

    if (test_enumeration[3]->record[0] != -1.0 || test_enumeration[3]->record[1] != -1.0) {
        result = "Failure";
    }

    if (test_enumeration[4]->record[0] != 2.0 || test_enumeration[4]->record[1] != -1.0) {
        result = "Failure";
    }

    printf("\nkd_tree_enumerate Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    free(test_enumeration);


    /**************************************
    ** kd_tree_destroy Test 2:
    ** Check that the destruction doesn't segfault.
    ***************************************/

    result = "Success";

    begin = clock();
    kd_tree_destroy(test_tree);
    elapsed = clock() - begin;

    printf("\nkd_tree_destroy Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    data_set_destroy(test_data);


    /**************************************
    ** kd_tree_construct_fancy Test 1:
    ** Check that a k-d tree is correctly constructed from the data set corresponding to the previous example. The points are:
    ** 1. (0, 0)
    ** 2. (1, 0)
    ** 3. (-1, 1)
    ** 4. (-1, -1)
    ** 5. (2, -1)
    ***************************************/

    result = "Success";

    test_data = data_set_create(2, 5);

    test_data->data[0] = 0.0;
    test_data->data[1] = 0.0;
    test_data->data[2] = 1.0;
    test_data->data[3] = 0.0;
    test_data->data[4] = -1.0;
    test_data->data[5] = 1.0;
    test_data->data[6] = -1.0;
    test_data->data[7] = -1.0;
    test_data->data[8] = 2.0;
    test_data->data[9] = -1.0;

    begin = clock();
    test_tree = kd_tree_construct_fancy(test_data);
    elapsed = clock() - begin;

    test_enumeration = kd_tree_enumerate(test_tree);

    if (test_enumeration[0]->record[0] != 0 || test_enumeration[0]->record[1] != 0) {
        result = "Failure";
    }

    if (test_enumeration[1]->record[0] != -1 || test_enumeration[1]->record[1] != 1) {
        result = "Failure";
    }

    if (test_enumeration[2]->record[0] != 1 || test_enumeration[2]->record[1] != 0) {
        result = "Failure";
    }

    if (test_enumeration[3]->record[0] != -1 || test_enumeration[3]->record[1] != -1) {
        result = "Failure";
    }

    if (test_enumeration[4]->record[0] != 2 || test_enumeration[4]->record[1] != -1) {
        result = "Failure";
    }

    printf("\nkd_tree_construct_fancy Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    free(test_enumeration);


    /**************************************
    ** kd_tree_destroy Test 3:
    ** Check that the destruction doesn't segfault.
    ***************************************/

    result = "Success";

    begin = clock();
    kd_tree_destroy(test_tree);
    elapsed = clock() - begin;

    printf("\nkd_tree_destroy Test 3: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    data_set_destroy(test_data);


    /**************************************
    ** kd_tree_construct_fancy Test 2:
    ** Check that a k-d tree is correctly constructed from the data set consisting of the following points:
    ** 1. (0, 0)
    ** 2. (1, 0)
    ** 3. (-1, 1)
    ** 4. (-1, -1)
    ** 5. (2, -1)
    ** 6. (3, 5)
    ** 7. (2, 2)
    ***************************************/

    result = "Success";

    test_data = data_set_create(2,7);

    test_data->data[0] = 0.0;
    test_data->data[1] = 0.0;
    test_data->data[2] = 1.0;
    test_data->data[3] = 0.0;
    test_data->data[4] = -1.0;
    test_data->data[5] = 1.0;
    test_data->data[6] = -1.0;
    test_data->data[7] = -1.0;
    test_data->data[8] = 2.0;
    test_data->data[9] = -1.0;
    test_data->data[10] = 3.0;
    test_data->data[11] = 5.0;
    test_data->data[12] = 2.0;
    test_data->data[13] = 2.0;

    begin = clock();
    test_tree = kd_tree_construct_fancy(test_data);
    elapsed = clock() - begin;

    test_enumeration = kd_tree_enumerate(test_tree);

    if (test_enumeration[0]->record[0] != 1 || test_enumeration[0]->record[1] != 0) {
        result = "Failure 0";
    }

    if (test_enumeration[1]->record[0] != 0 || test_enumeration[1]->record[1] != 0) {
        result = "Failure 1";
    }

    if (test_enumeration[2]->record[0] != 2 || test_enumeration[2]->record[1] != 2) {
        result = "Failure 2";
    }

    if (test_enumeration[3]->record[0] != -1 || test_enumeration[3]->record[1] != -1) {
        result = "Failure 3";
    }

    if (test_enumeration[4]->record[0] != -1 || test_enumeration[4]->record[1] != 1) {
        result = "Failure 4";
    }

    if (test_enumeration[5]->record[0] != 2 || test_enumeration[5]->record[1] != -1) {
        result = "Failure 5";
    }

    if (test_enumeration[6]->record[0] != 3 || test_enumeration[6]->record[1] != 5) {
        result = "Failure 6";
    }

    printf("\nkd_tree_construct_fancy Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    free(test_enumeration);


    /**************************************
    ** kd_tree_destroy Test 4:
    ** Check that the destruction doesn't segfault.
    ***************************************/

    result = "Success";

    begin = clock();
    kd_tree_destroy(test_tree);
    elapsed = clock() - begin;

    printf("\nkd_tree_destroy Test 4: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    data_set_destroy(test_data);


    /**************************************
    ** kd_tree_insert_vertex Test 1:
    ** Insert the following (record, disc_key) pairs as vertices into an empty kd_tree and check that this yields the right structure:
    ** 1. ((0, 1), 1)
    ** 2. ((1, 0), 1)
    ** 3. ((1, 2), 0)
    ** 4. ((0, 4), 0)
    ** 5. ((2, -1), 1)
    ***************************************/

    result = "Success";

    test_tree = kd_tree_create(2);

    temp_vertex = kd_vertex_create(2, 1);
    temp_vertex->record[0] = 0;
    temp_vertex->record[1] = 1;

    begin = clock();
    kd_tree_insert_vertex(test_tree, temp_vertex);
    elapsed = clock() - begin;

    temp_vertex = kd_vertex_create(2, 1);
    temp_vertex->record[0] = 1;
    temp_vertex->record[1] = 0;

    begin = clock();
    kd_tree_insert_vertex(test_tree, temp_vertex);
    elapsed += clock() - begin;

    temp_vertex = kd_vertex_create(2, 0);
    temp_vertex->record[0] = 1;
    temp_vertex->record[1] = 2;

    begin = clock();
    kd_tree_insert_vertex(test_tree, temp_vertex);
    elapsed += clock() - begin;

    temp_vertex = kd_vertex_create(2, 0);
    temp_vertex->record[0] = 0;
    temp_vertex->record[1] = 4;

    begin = clock();
    kd_tree_insert_vertex(test_tree, temp_vertex);
    elapsed += clock() - begin;

    temp_vertex = kd_vertex_create(2, 1);
    temp_vertex->record[0] = 2;
    temp_vertex->record[1] = -1;

    begin = clock();
    kd_tree_insert_vertex(test_tree, temp_vertex);
    elapsed += clock() - begin;

    if (test_tree->num_vertices != 5) {
        goto insert_vertex_1_failure;
    }

    test_vertex = test_tree->root;
    if (test_vertex->record[0] != 0 || test_vertex->record[1] != 1 || test_vertex->disc_key != 1) {
        goto insert_vertex_1_failure;
    }

    temp_vertex = test_vertex->children[0];
    if (temp_vertex->record[0] != 1 || temp_vertex->record[1] != 0 || temp_vertex->disc_key != 1) {
        goto insert_vertex_1_failure;
    }

    test_vertex = test_vertex->children[1];
    if (test_vertex->record[0] != 1 || test_vertex->record[1] != 2 || test_vertex->disc_key != 0) {
        goto insert_vertex_1_failure;
    }

    temp_vertex = temp_vertex->children[0];
    if (temp_vertex->record[0] != 2 || temp_vertex->record[1] != -1 || temp_vertex->disc_key != 1) {
        goto insert_vertex_1_failure;
    }

    test_vertex = test_vertex->children[0];
    if (test_vertex->record[0] != 0 || test_vertex->record[1] != 4 || test_vertex->disc_key != 0) {
        goto insert_vertex_1_failure;
    }

    goto insert_vertex_1_success;

    insert_vertex_1_failure:
    result = "Failure";

    insert_vertex_1_success:
    printf("\nkd_tree_insert_vertex Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /**************************************
    ** kd_tree_construct_adaptive Test 1:
    ** Check that a k-d tree is correctly constructed from the data set consisting of the following points:
    ** 1. (0, 0)
    ** 2. (1, 0)
    ** 3. (-1, 1)
    ** 4. (-1, -1)
    ** 5. (2, -1)
    ** 6. (3, 5)
    ** 7. (2, 2)
    ***************************************/

    result = "Success";

    test_data = data_set_create(2,7);

    test_data->data[0] = 0.0;
    test_data->data[1] = 0.0;
    test_data->data[2] = 1.0;
    test_data->data[3] = 0.0;
    test_data->data[4] = -1.0;
    test_data->data[5] = 1.0;
    test_data->data[6] = -1.0;
    test_data->data[7] = -1.0;
    test_data->data[8] = 2.0;
    test_data->data[9] = -1.0;
    test_data->data[10] = 3.0;
    test_data->data[11] = 5.0;
    test_data->data[12] = 2.0;
    test_data->data[13] = 2.0;

    begin = clock();
    test_tree = kd_tree_construct_adaptive(test_data);
    elapsed = clock() - begin;

    test_enumeration = kd_tree_enumerate(test_tree);

    if (test_enumeration[0]->record[0] != 1 || test_enumeration[0]->record[1] != 0 || test_enumeration[0]->disc_key != 1) {
        printf("(%f, %f), %d\n", test_enumeration[0]->record[0], test_enumeration[0]->record[1], test_enumeration[0]->disc_key);
        result = "Failure 0";
        goto adaptive1_result;
    }

    if (test_enumeration[1]->record[0] != 0 || test_enumeration[1]->record[1] != 0 || test_enumeration[1]->disc_key != 0) {
        result = "Failure 1";
        goto adaptive1_result;
    }

    if (test_enumeration[2]->record[0] != 2 || test_enumeration[2]->record[1] != 2 || test_enumeration[2]->disc_key != 1) {
        result = "Failure 2";
        goto adaptive1_result;
    }

    if (test_enumeration[3]->record[0] != -1 || test_enumeration[3]->record[1] != -1 || test_enumeration[3]->disc_key != 0) {
        result = "Failure 3";
        goto adaptive1_result;
    }

    if (test_enumeration[4]->record[0] != 2 || test_enumeration[4]->record[1] != -1 || test_enumeration[4]->disc_key != 0) {
        result = "Failure 4";
        goto adaptive1_result;
    }

    if (test_enumeration[5]->record[0] != -1 || test_enumeration[5]->record[1] != 1 || test_enumeration[5]->disc_key != 0) {
        result = "Failure 5";
        goto adaptive1_result;
    }

    if (test_enumeration[6]->record[0] != 3 || test_enumeration[6]->record[1] != 5 || test_enumeration[6]->disc_key != 0) {
        result = "Failure 6";
        goto adaptive1_result;
    }

    adaptive1_result:
    printf("\nkd_tree_construct_adaptive Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    free(test_enumeration);



    // FREE STUFF
    data_set_destroy(test_data);

}

