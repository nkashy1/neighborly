
/* Header file */
#include "test_results.h"

/* Standard libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */

void test_results() {

    char *result;
    results_datum *datum_test, *datum_test_2;
    results *results_test;
    int truth, dimension, i;

    clock_t begin, elapsed;

    /********************************************
    ** results_datum_create Test 1:
    ** Does results_datum_create work without any problems?
    *********************************************/
    result = "Success";

    begin = clock();
    datum_test = results_datum_create();
    elapsed = clock() - begin;

    printf("\nresults_datum_create Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /********************************************
    ** results_datum_destroy Test 1:
    ** Are we able to obliterate the previously instantiated results_datum without a hitch?
    *********************************************/

    begin = clock();
    results_datum_destroy(datum_test);
    elapsed = clock() - begin;

    printf("\nresults_datum_destroy Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /********************************************
    ** results_datum_copy Test 1:
    ** Does results_datum_create work without any problems?
    *********************************************/
    result = "Success";

    dimension = 2;

    datum_test = results_datum_create();
    datum_test->distance = 1;
    datum_test->data_point = malloc(sizeof(NUMERICAL_TYPE)*dimension);
    for (i = 0; i < dimension; i++) {
        datum_test->data_point[i] = i;
    }

    begin = clock();
    datum_test_2 = results_datum_copy(datum_test, dimension);
    elapsed = clock() - begin;

    if (datum_test_2->distance != datum_test->distance) {
        goto copy_1_failure;
    }

    if (datum_test_2->data_point == datum_test->data_point) {
        goto copy_1_failure;
    }

    for (i = 0; i < dimension; i++) {
        if (datum_test_2->data_point[i] != datum_test->data_point[i]) {
            goto copy_1_failure;
        }
    }

    goto copy_1_success;

    copy_1_failure:
    result = "Failure";

    copy_1_success:
    printf("\nresults_datum_copy Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    // Free stuff
    results_datum_destroy(datum_test);
    results_datum_destroy(datum_test_2);


    /********************************************
    ** results_create Test 1:
    ** Make a results struct with no problems?
    *********************************************/

    begin = clock();
    results_test = results_create(5);
    elapsed = clock() - begin;

    printf("\nresults_create Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************************
    ** results_destroy Test 1:
    ** Destroy the same results struct without any issues?
    **********************************************/

    begin = clock();
    results_destroy(results_test);
    elapsed = clock() - begin;

    printf("\nresults_destroy Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************************
    ** results_lt Test 1:
    ** Check that our results version of "less than" works.
    **********************************************/

    datum_test = results_datum_create();
    datum_test_2 = results_datum_create();

    datum_test->distance = 1;
    datum_test_2->distance = 2;

    begin = clock();
    truth = results_lt(datum_test, datum_test_2);
    elapsed = clock() - begin;

    if (truth != 1) {
        result = "Failure";
    }

    printf("\nresults_lt Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************************
    ** results_lt Test 2:
    ** Check that our results version of "less than" works.
    **********************************************/

    result = "Success";

    begin = clock();
    truth = results_lt(datum_test_2, datum_test);
    elapsed = clock() - begin;

    if (truth != 0) {
        result = "Failure";
    }

    printf("\nresults_lt Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    results_datum_destroy(datum_test);
    results_datum_destroy(datum_test_2);

}

#ifndef HEAP_DATA_TYPE
#define HEAP_DATA_TYPE
typedef results heap_data_type;

#undef HEAP_DATA_TYPE
#endif // HEAP_DATA_TYPE
