
#ifndef FLOAT_EPSILON
#define FLOAT_EPSILON 0.00001

/* Header file */
#include "test_naive.h"

/* Standard Libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../src/metrics/distance.h"
#include "../src/structures/data_set.h"

void test_naive_nearest_neighbor_distance() {
    data_set *D;
    D = data_set_create(2, 1000);

    NUMERICAL_TYPE P[2];
    NUMERICAL_TYPE estimated_nearest_neighbor_distance;
    char* result;
    int i, j;

    clock_t begin, elapsed;

    // Initialize D to contain in its i^th row the vector (i, i).
    for (i = 0; i < D->num_data_points; i++) {
        for (j = 0; j < D->embedding_dimension; j++) {
            D->data[D->embedding_dimension*i + j] = i;
            //continue;
        }
    }

    /*********************************
    ** naive_nearest_neighbor_distance Test 1:
    ** Check that the nearest-neighbor distance of 'D' to (-1, -1) is sqrt(2).
    *********************************/
    result = "Success";
    P[0] = -1.0;
    P[1] = -1.0;

    begin = clock();
    estimated_nearest_neighbor_distance = naive_nearest_neighbor_distance(D, P);
    elapsed = clock() - begin;

    if (fabs(estimated_nearest_neighbor_distance - sqrt(2)) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\nnaive_nearest_neighbor_distance Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);


    /*********************************
    ** naive_nearest_neighbor_distance Test 2:
    ** Check that the nearest-neighbor distance of 'D' to (500.5, 500.5) is 1/(2*sqrt(2)).
    *********************************/

    result = "Success";

    P[0] = 500.5;
    P[1] = 500.5;

    begin = clock();
    estimated_nearest_neighbor_distance = naive_nearest_neighbor_distance(D, P);
    elapsed = clock() - begin;

    if (fabs(estimated_nearest_neighbor_distance - (1/sqrt(2))) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\nnaive_nearest_neighbor_distance Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    // Deallocate the array of data points in D.
    data_set_destroy(D);
}



void test_naive_all_nearest_neighbor_distances() {

    char *result;

    clock_t begin, elapsed;

    /*********************************
    ** naive_all_nearest_neighbor_distances Test 1:
    ** Check that the nearest neighbor distances within the data set consisting of the vectors (i, i) for 0 <= i <= 999 are all
    ** sqrt(2).
    *********************************/

    result = "Success";

    data_set *D;
    D = data_set_create(2, 1000);

    int i, j;



    // Initialize D to contain in its i^th row the vector (i, i).
    for (i = 0; i < D->num_data_points; i++) {
        for (j = 0; j < D->embedding_dimension; j++) {
            D->data[D->embedding_dimension*i + j] = i;
            //continue;
        }
    }


    distance_matrix *pairwise_distances = distance_matrix_create(D);

    begin = clock();
    NUMERICAL_TYPE *nearest_neighbor_distances = naive_all_nearest_neighbor_distances(pairwise_distances);
    elapsed = clock() - begin;

    for (i = 0; i < D->num_data_points; i++) {
        if (fabs(nearest_neighbor_distances[i] - sqrt(2)) > FLOAT_EPSILON) {
            printf("%d, %f", i, nearest_neighbor_distances[i]);
            result = "Failure";
            break;
        }
    }

    printf("\nnaive_all_nearest_neighbor_distances Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    // Deallocate!
    data_set_destroy(D);
    distance_matrix_destroy(pairwise_distances);

}

#endif // FLOAT_EPSILON
