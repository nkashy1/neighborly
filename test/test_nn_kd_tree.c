
#ifndef FLOAT_EPSILON
#define FLOAT_EPSILON 0.00001

/* Header file */
#include "test_nn_kd_tree.h"

/* Test libraries */
#include "../src/nn_kd_tree.h"

/* Standard Libraries */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Custom libraries */
#include "../src/metrics/distance.h"
#include "../src/structures/data_set.h"
#include "../src/structures/kd_tree.h"
#include "../src/structures/results.h"
#include "../src/structures/common/results_heap.h"


void test_nn_kd_tree() {
    data_set *D;
    D = data_set_create(2, 100000);

    kd_tree *frame_tree;
    results_heap *potential_neighbors;

    NUMERICAL_TYPE P[2];
    NUMERICAL_TYPE p;
    NUMERICAL_TYPE estimated_nearest_neighbor_distance;
    results* neighbors;
    results** all_neighbors;
    char* result;
    int i, j;

    clock_t begin, elapsed;

    // Initialize D to contain in its i^th row the vector (i, i).
    for (i = 0; i < D->num_data_points; i++) {
        for (j = 0; j < D->embedding_dimension; j++) {
            D->data[D->embedding_dimension*i + j] = i;
            //continue;
        }
    }

    begin = clock();
    frame_tree = kd_tree_construct_fancy(D);
    elapsed = clock() - begin;
    printf("\nkd_tree construction: %d ticks\n", (int) elapsed);


    // Debugging
    //printf("HELLO, %d, %d\n", frame_tree->num_vertices, frame_tree->depth);

    /*********************************
    ** nn_streaming_kd_tree Test 1:
    ** Check that the nearest-neighbor distance of 'D' to (-1, -1) is sqrt(2).
    *********************************/

    result = "Success";
    P[0] = -1.0;
    P[1] = -1.0;
    p = 2;

    begin = clock();
    neighbors = nn_streaming_kd_tree(frame_tree, P, 1, p);
    elapsed = clock() - begin;

    estimated_nearest_neighbor_distance = neighbors->data[0].distance;

    if (fabs(estimated_nearest_neighbor_distance - sqrt(2)) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\nnn_streaming_kd_tree Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    results_destroy(neighbors);


    /*********************************
    ** nn_streaming_kd_tree Test 2:
    ** Check that the nearest-neighbor distance of 'D' to (500.5, 500.5) is 1/(2*sqrt(2)).
    *********************************/

    result = "Success";

    P[0] = 500.5;
    P[1] = 500.5;
    p = 2;

    begin = clock();
    neighbors = nn_streaming_kd_tree(frame_tree, P, 1, p);
    elapsed = clock() - begin;
    estimated_nearest_neighbor_distance = neighbors->data[0].distance;

    if (fabs(estimated_nearest_neighbor_distance - (1/sqrt(2))) > FLOAT_EPSILON) {
        result = "Failure";
    }

    printf("\nnn_streaming_kd_tree Test 2: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    results_destroy(neighbors);


    /*********************************
    ** nn_streaming_kd_tree_with_prep Test 1:
    ** Check that the nearest-neighbor distance of 'D' to (500.5, 500.5) is 1/(2*sqrt(2)).
    *********************************/

    result = "Success";

    P[0] = 500.5;
    P[1] = 500.5;
    p = 2;

    potential_neighbors = results_heap_create(NULL, 0, 1, results_gt);
    potential_neighbors->vertices = malloc(sizeof(results_datum)*1);

    begin = clock();
    neighbors = nn_streaming_kd_tree_with_prep(frame_tree, P, potential_neighbors, p);
    elapsed = clock() - begin;
    estimated_nearest_neighbor_distance = neighbors->data[0].distance;

    if (fabs(estimated_nearest_neighbor_distance - (1/sqrt(2))) > FLOAT_EPSILON) {
        result = "Failure";
    }

    if (potential_neighbors->max_size != 1) {
        result = "Failure";
    }

    if (potential_neighbors->current_size != 1) {
        result = "Failure";
    }

    printf("\nnn_streaming_kd_tree_with_prep Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    results_destroy(neighbors);
    free(potential_neighbors->vertices);
    free(potential_neighbors);


    /*********************************
    ** nn_all_kd_tree Test 1:
    ** Check that the nearest-neighbor distances within the data set {(i,i) : 0 <= i < 1000} are all sqrt(2).
    *********************************/

    result = "Success";

    p = 2;

    begin = clock();
    all_neighbors = nn_all_kd_tree(frame_tree, 2, p);
    elapsed = clock() - begin;

    for (i = 0; i < frame_tree->num_vertices; i++) {
        if (fabs(all_neighbors[i]->data[0].distance - sqrt(2)) > FLOAT_EPSILON) {
            result = "Failure";
            break;
        }
    }

    printf("\nnn_all_kd_tree Test 1: %s\n", result);
    printf("The computation took %d ticks.\n", (int) elapsed);

    // Free up all that space allocated to results.
    for (i = 0; i < frame_tree->num_vertices; i++) {
        results_destroy(all_neighbors[i]);
    }
    free(all_neighbors);


    // Deallocate the array of data points in D.
    data_set_destroy(D);

    // Better destroy frame_tree, as well.
    kd_tree_destroy(frame_tree);

}

#endif // FLOAT_EPSILON
