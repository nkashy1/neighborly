
/* Test Library */
#include "metrics/test_distance.h"
#include "structures/test_data_set.h"
#include "test_naive.h"
#include "structures/common/test_numerical_heap.h"
#include "structures/common/test_results_heap.h"
#include "structures/test_kd_tree.h"
#include "structures/test_results.h"
#include "test_nn_kd_tree.h"
#include "structures/common/test_median.h"
#include "structures/common/test_spread.h"
#include "structures/common/test_csv.h"

/* Standard libraries */
#include <stdio.h>
#include <string.h>

/* Tolerance in floating point calculations */
#define FLOAT_EPSILON 0.00001

int main(int argc, char* argv[]){
    int i;

    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "all")) {
            test_distance_functions();
            test_distance_matrix_index_compression();
            test_distance_matrix_utilities();
            test_data_set();
            test_naive_nearest_neighbor_distance();
            test_naive_all_nearest_neighbor_distances();
            test_numerical_heap_utilities();
            test_results_heap_utilities();
            test_kd_tree();
            test_results();
            test_median();
            test_spread();
            test_nn_kd_tree();
            test_csv();
        } else if (!strcmp(argv[i], "distance")) {
            test_distance_functions();
            test_distance_matrix_index_compression();
            test_distance_matrix_utilities();
        } else if (!strcmp(argv[i], "naive")){
            test_naive_nearest_neighbor_distance();
            test_naive_all_nearest_neighbor_distances();
        } else if (!strcmp(argv[i], "data_set")) {
            test_data_set();
        } else if (!strcmp(argv[i], "numerical_heap")) {
            test_numerical_heap_utilities();
        } else if (!strcmp(argv[i], "results_heap")) {
            test_results_heap_utilities();
        } else if (!strcmp(argv[i], "kd_tree")) {
            test_kd_tree();
        } else if (!strcmp(argv[i], "results")) {
            test_results();
        } else if (!strcmp(argv[i], "median")) {
            test_median();
        } else if (!strcmp(argv[i], "spread")) {
            test_spread();
        } else if (!strcmp(argv[i], "nn_kd_tree")) {
            test_nn_kd_tree();
        } else if (!strcmp(argv[i], "csv")) {
            test_csv();
        }
    }

    return 0;
}

#undef HEAP_DATA_TYPE
#undef NUMERICAL_TYPE
